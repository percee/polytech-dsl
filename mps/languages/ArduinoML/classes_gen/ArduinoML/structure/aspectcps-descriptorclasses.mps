<?xml version="1.0" encoding="UTF-8"?>
<model ref="00000000-0000-4000-5f02-5beb5f025beb/i:f045075(checkpoints/ArduinoML.structure@descriptorclasses)">
  <persistence version="9" />
  <attribute name="checkpoint" value="DescriptorClasses" />
  <attribute name="generation-plan" value="AspectCPS" />
  <languages />
  <imports>
    <import index="m31e" ref="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)" />
    <import index="ksn4" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.lang.smodel(MPS.Core/)" />
    <import index="c17a" ref="8865b7a8-5271-43d3-884c-6fd1d9cfdd34/java:org.jetbrains.mps.openapi.language(MPS.OpenAPI/)" />
    <import index="ze1i" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime(MPS.Core/)" />
    <import index="e8bb" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.adapter.ids(MPS.Core/)" />
    <import index="33ny" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.util(JDK/)" />
    <import index="vndm" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.language(MPS.Core/)" />
    <import index="bzg8" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel.runtime.impl(MPS.Core/)" />
    <import index="mhfm" ref="3f233e7f-b8a6-46d2-a57f-795d56775243/java:org.jetbrains.annotations(Annotations/)" />
    <import index="w1kc" ref="6ed54515-acc8-4d1e-a16c-9fd6cfe951ea/java:jetbrains.mps.smodel(MPS.Core/)" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1215693861676" name="jetbrains.mps.baseLanguage.structure.BaseAssignmentExpression" flags="nn" index="d038R">
        <child id="1068498886297" name="rValue" index="37vLTx" />
        <child id="1068498886295" name="lValue" index="37vLTJ" />
      </concept>
      <concept id="1202948039474" name="jetbrains.mps.baseLanguage.structure.InstanceMethodCallOperation" flags="nn" index="liA8E" />
      <concept id="1465982738277781862" name="jetbrains.mps.baseLanguage.structure.PlaceholderMember" flags="ng" index="2tJIrI" />
      <concept id="1076505808687" name="jetbrains.mps.baseLanguage.structure.WhileStatement" flags="nn" index="2$JKZl">
        <child id="1076505808688" name="condition" index="2$JKZa" />
      </concept>
      <concept id="1188207840427" name="jetbrains.mps.baseLanguage.structure.AnnotationInstance" flags="nn" index="2AHcQZ">
        <reference id="1188208074048" name="annotation" index="2AI5Lk" />
      </concept>
      <concept id="1188208481402" name="jetbrains.mps.baseLanguage.structure.HasAnnotation" flags="ng" index="2AJDlI">
        <child id="1188208488637" name="annotation" index="2AJF6D" />
      </concept>
      <concept id="1224848483129" name="jetbrains.mps.baseLanguage.structure.IBLDeprecatable" flags="ng" index="IEa8$">
        <property id="1224848525476" name="isDeprecated" index="IEkAT" />
      </concept>
      <concept id="1154032098014" name="jetbrains.mps.baseLanguage.structure.AbstractLoopStatement" flags="nn" index="2LF5Ji">
        <child id="1154032183016" name="body" index="2LFqv$" />
      </concept>
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1083245097125" name="jetbrains.mps.baseLanguage.structure.EnumClass" flags="ig" index="Qs71p">
        <child id="1083245396908" name="enumConstant" index="Qtgdg" />
      </concept>
      <concept id="1083245299891" name="jetbrains.mps.baseLanguage.structure.EnumConstantDeclaration" flags="ig" index="QsSxf" />
      <concept id="1083260308424" name="jetbrains.mps.baseLanguage.structure.EnumConstantReference" flags="nn" index="Rm8GO">
        <reference id="1083260308426" name="enumConstantDeclaration" index="Rm8GQ" />
        <reference id="1144432896254" name="enumClass" index="1Px2BO" />
      </concept>
      <concept id="1145552977093" name="jetbrains.mps.baseLanguage.structure.GenericNewExpression" flags="nn" index="2ShNRf">
        <child id="1145553007750" name="creator" index="2ShVmc" />
      </concept>
      <concept id="1070462154015" name="jetbrains.mps.baseLanguage.structure.StaticFieldDeclaration" flags="ig" index="Wx3nA">
        <property id="6468716278899126575" name="isVolatile" index="2dlcS1" />
        <property id="6468716278899125786" name="isTransient" index="2dld4O" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700938" name="jetbrains.mps.baseLanguage.structure.StaticMethodDeclaration" flags="ig" index="2YIFZL" />
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1070533707846" name="jetbrains.mps.baseLanguage.structure.StaticFieldReference" flags="nn" index="10M0yZ">
        <reference id="1144433057691" name="classifier" index="1PxDUh" />
      </concept>
      <concept id="1070534058343" name="jetbrains.mps.baseLanguage.structure.NullLiteral" flags="nn" index="10Nm6u" />
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1070534934090" name="jetbrains.mps.baseLanguage.structure.CastExpression" flags="nn" index="10QFUN">
        <child id="1070534934091" name="type" index="10QFUM" />
        <child id="1070534934092" name="expression" index="10QFUP" />
      </concept>
      <concept id="1068390468200" name="jetbrains.mps.baseLanguage.structure.FieldDeclaration" flags="ig" index="312cEg">
        <property id="8606350594693632173" name="isTransient" index="eg7rD" />
        <property id="1240249534625" name="isVolatile" index="34CwA1" />
      </concept>
      <concept id="1068390468198" name="jetbrains.mps.baseLanguage.structure.ClassConcept" flags="ig" index="312cEu">
        <property id="1221565133444" name="isFinal" index="1EXbeo" />
        <child id="1165602531693" name="superclass" index="1zkMxy" />
      </concept>
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <property id="1176718929932" name="isFinal" index="3TUv4t" />
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1068498886292" name="jetbrains.mps.baseLanguage.structure.ParameterDeclaration" flags="ir" index="37vLTG" />
      <concept id="1068498886294" name="jetbrains.mps.baseLanguage.structure.AssignmentExpression" flags="nn" index="37vLTI" />
      <concept id="1225271177708" name="jetbrains.mps.baseLanguage.structure.StringType" flags="in" index="17QB3L" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="4269842503726207156" name="jetbrains.mps.baseLanguage.structure.LongLiteral" flags="nn" index="1adDum">
        <property id="4269842503726207157" name="value" index="1adDun" />
      </concept>
      <concept id="1068580123132" name="jetbrains.mps.baseLanguage.structure.BaseMethodDeclaration" flags="ng" index="3clF44">
        <property id="4276006055363816570" name="isSynchronized" index="od$2w" />
        <property id="1181808852946" name="isFinal" index="DiZV1" />
        <child id="1068580123133" name="returnType" index="3clF45" />
        <child id="1068580123134" name="parameter" index="3clF46" />
        <child id="1068580123135" name="body" index="3clF47" />
      </concept>
      <concept id="1068580123165" name="jetbrains.mps.baseLanguage.structure.InstanceMethodDeclaration" flags="ig" index="3clFb_">
        <property id="1178608670077" name="isAbstract" index="1EzhhJ" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580123137" name="jetbrains.mps.baseLanguage.structure.BooleanConstant" flags="nn" index="3clFbT">
        <property id="1068580123138" name="value" index="3clFbU" />
      </concept>
      <concept id="1068580123140" name="jetbrains.mps.baseLanguage.structure.ConstructorDeclaration" flags="ig" index="3clFbW" />
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1068581517677" name="jetbrains.mps.baseLanguage.structure.VoidType" flags="in" index="3cqZAl" />
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
      <concept id="1212685548494" name="jetbrains.mps.baseLanguage.structure.ClassCreator" flags="nn" index="1pGfFk" />
      <concept id="1107461130800" name="jetbrains.mps.baseLanguage.structure.Classifier" flags="ng" index="3pOWGL">
        <child id="5375687026011219971" name="member" index="jymVt" unordered="true" />
      </concept>
      <concept id="7812454656619025416" name="jetbrains.mps.baseLanguage.structure.MethodDeclaration" flags="ng" index="1rXfSm">
        <property id="8355037393041754995" name="isNative" index="2aFKle" />
      </concept>
      <concept id="7812454656619025412" name="jetbrains.mps.baseLanguage.structure.LocalMethodCall" flags="nn" index="1rXfSq" />
      <concept id="1107535904670" name="jetbrains.mps.baseLanguage.structure.ClassifierType" flags="in" index="3uibUv">
        <reference id="1107535924139" name="classifier" index="3uigEE" />
        <child id="1109201940907" name="parameter" index="11_B2D" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1073239437375" name="jetbrains.mps.baseLanguage.structure.NotEqualsExpression" flags="nn" index="3y3z36" />
      <concept id="1178549954367" name="jetbrains.mps.baseLanguage.structure.IVisible" flags="ng" index="1B3ioH">
        <child id="1178549979242" name="visibility" index="1B3o_S" />
      </concept>
      <concept id="1163670490218" name="jetbrains.mps.baseLanguage.structure.SwitchStatement" flags="nn" index="3KaCP$">
        <child id="1163670592366" name="defaultBlock" index="3Kb1Dw" />
        <child id="1163670766145" name="expression" index="3KbGdf" />
        <child id="1163670772911" name="case" index="3KbHQx" />
      </concept>
      <concept id="1163670641947" name="jetbrains.mps.baseLanguage.structure.SwitchCase" flags="ng" index="3KbdKl">
        <child id="1163670677455" name="expression" index="3Kbmr1" />
        <child id="1163670683720" name="body" index="3Kbo56" />
      </concept>
      <concept id="1146644602865" name="jetbrains.mps.baseLanguage.structure.PublicVisibility" flags="nn" index="3Tm1VV" />
      <concept id="1146644623116" name="jetbrains.mps.baseLanguage.structure.PrivateVisibility" flags="nn" index="3Tm6S6" />
      <concept id="1116615150612" name="jetbrains.mps.baseLanguage.structure.ClassifierClassExpression" flags="nn" index="3VsKOn">
        <reference id="1116615189566" name="classifier" index="3VsUkX" />
      </concept>
    </language>
    <language id="b401a680-8325-4110-8fd3-84331ff25bef" name="jetbrains.mps.lang.generator">
      <concept id="9032177546941580387" name="jetbrains.mps.lang.generator.structure.TrivialNodeId" flags="nn" index="2$VJBW">
        <property id="9032177546941580392" name="nodeId" index="2$VJBR" />
        <child id="8557539026538618631" name="cncpt" index="3iCydw" />
      </concept>
      <concept id="5808518347809715508" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_InputNode" flags="nn" index="385nmt">
        <property id="5808518347809748738" name="presentation" index="385vuF" />
        <child id="5808518347809747118" name="node" index="385v07" />
      </concept>
      <concept id="3864140621129707969" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_Mappings" flags="nn" index="39dXUE">
        <child id="3864140621129713349" name="labels" index="39e2AI" />
      </concept>
      <concept id="3864140621129713351" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_NodeMapEntry" flags="nn" index="39e2AG">
        <property id="5843998055530255671" name="isNewRoot" index="2mV_xN" />
        <reference id="3864140621129713371" name="inputOrigin" index="39e2AK" />
        <child id="5808518347809748862" name="inputNode" index="385vvn" />
        <child id="3864140621129713365" name="outputNode" index="39e2AY" />
      </concept>
      <concept id="3864140621129713348" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_LabelEntry" flags="nn" index="39e2AJ">
        <property id="3864140621129715945" name="label" index="39e3Y2" />
        <child id="3864140621129715947" name="entries" index="39e3Y0" />
      </concept>
      <concept id="3864140621129713362" name="jetbrains.mps.lang.generator.structure.GeneratorDebug_NodeRef" flags="nn" index="39e2AT">
        <reference id="3864140621129713363" name="node" index="39e2AS" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="6911370362349121511" name="jetbrains.mps.lang.smodel.structure.ConceptId" flags="nn" index="2x4n5u">
        <property id="6911370362349122519" name="conceptName" index="2x4mPI" />
        <property id="6911370362349121516" name="conceptId" index="2x4n5l" />
        <property id="6911370362349133804" name="isInterface" index="2x4o5l" />
        <child id="6911370362349121514" name="languageIdentity" index="2x4n5j" />
      </concept>
      <concept id="3542851458883438784" name="jetbrains.mps.lang.smodel.structure.LanguageId" flags="nn" index="2V$Bhx">
        <property id="3542851458883439831" name="namespace" index="2V$B1Q" />
        <property id="3542851458883439832" name="languageId" index="2V$B1T" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="540871147943773365" name="jetbrains.mps.baseLanguage.collections.structure.SingleArgumentSequenceOperation" flags="nn" index="25WWJ4">
        <child id="540871147943773366" name="argument" index="25WWJ7" />
      </concept>
      <concept id="1237467461002" name="jetbrains.mps.baseLanguage.collections.structure.GetIteratorOperation" flags="nn" index="uNJiE" />
      <concept id="1237467705688" name="jetbrains.mps.baseLanguage.collections.structure.IteratorType" flags="in" index="uOF1S">
        <child id="1237467730343" name="elementType" index="uOL27" />
      </concept>
      <concept id="1237470895604" name="jetbrains.mps.baseLanguage.collections.structure.HasNextOperation" flags="nn" index="v0PNk" />
      <concept id="1237471031357" name="jetbrains.mps.baseLanguage.collections.structure.GetNextOperation" flags="nn" index="v1n4t" />
      <concept id="1151688443754" name="jetbrains.mps.baseLanguage.collections.structure.ListType" flags="in" index="_YKpA">
        <child id="1151688676805" name="elementType" index="_ZDj9" />
      </concept>
      <concept id="1237721394592" name="jetbrains.mps.baseLanguage.collections.structure.AbstractContainerCreator" flags="nn" index="HWqM0">
        <child id="1237721435807" name="elementType" index="HW$YZ" />
      </concept>
      <concept id="1227008614712" name="jetbrains.mps.baseLanguage.collections.structure.LinkedListCreator" flags="nn" index="2Jqq0_" />
      <concept id="1160612413312" name="jetbrains.mps.baseLanguage.collections.structure.AddElementOperation" flags="nn" index="TSZUe" />
    </language>
  </registry>
  <node concept="Qs71p" id="0">
    <property role="TrG5h" value="COMP" />
    <node concept="QsSxf" id="1" role="Qtgdg">
      <property role="TrG5h" value="lt" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="h" role="37wK5m">
        <property role="Xl_RC" value="lt" />
      </node>
      <node concept="Xl_RD" id="i" role="37wK5m">
        <property role="Xl_RC" value="&lt;" />
      </node>
    </node>
    <node concept="QsSxf" id="2" role="Qtgdg">
      <property role="TrG5h" value="gt" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="j" role="37wK5m">
        <property role="Xl_RC" value="gt" />
      </node>
      <node concept="Xl_RD" id="k" role="37wK5m">
        <property role="Xl_RC" value="&gt;" />
      </node>
    </node>
    <node concept="QsSxf" id="3" role="Qtgdg">
      <property role="TrG5h" value="le" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="l" role="37wK5m">
        <property role="Xl_RC" value="le" />
      </node>
      <node concept="Xl_RD" id="m" role="37wK5m">
        <property role="Xl_RC" value="&lt;=" />
      </node>
    </node>
    <node concept="QsSxf" id="4" role="Qtgdg">
      <property role="TrG5h" value="ge" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="n" role="37wK5m">
        <property role="Xl_RC" value="ge" />
      </node>
      <node concept="Xl_RD" id="o" role="37wK5m">
        <property role="Xl_RC" value="&gt;=" />
      </node>
    </node>
    <node concept="QsSxf" id="5" role="Qtgdg">
      <property role="TrG5h" value="eq" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="p" role="37wK5m">
        <property role="Xl_RC" value="eq" />
      </node>
      <node concept="Xl_RD" id="q" role="37wK5m">
        <property role="Xl_RC" value="==" />
      </node>
    </node>
    <node concept="QsSxf" id="6" role="Qtgdg">
      <property role="TrG5h" value="ne" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="r" role="37wK5m">
        <property role="Xl_RC" value="ne" />
      </node>
      <node concept="Xl_RD" id="s" role="37wK5m">
        <property role="Xl_RC" value="!=" />
      </node>
    </node>
    <node concept="3Tm1VV" id="7" role="1B3o_S" />
    <node concept="312cEg" id="8" role="jymVt">
      <property role="TrG5h" value="myName" />
      <property role="3TUv4t" value="true" />
      <node concept="17QB3L" id="t" role="1tU5fm" />
      <node concept="3Tm6S6" id="u" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="9" role="jymVt">
      <property role="TrG5h" value="getName" />
      <node concept="3clFbS" id="v" role="3clF47">
        <node concept="3cpWs6" id="y" role="3cqZAp">
          <node concept="37vLTw" id="z" role="3cqZAk">
            <ref role="3cqZAo" node="8" resolve="myName" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="w" role="3clF45" />
      <node concept="3Tm1VV" id="x" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="a" role="jymVt">
      <property role="TrG5h" value="myValue" />
      <property role="3TUv4t" value="true" />
      <node concept="17QB3L" id="$" role="1tU5fm" />
      <node concept="3Tm6S6" id="_" role="1B3o_S" />
    </node>
    <node concept="3clFbW" id="b" role="jymVt">
      <node concept="3clFbS" id="A" role="3clF47">
        <node concept="3clFbF" id="E" role="3cqZAp">
          <node concept="37vLTI" id="G" role="3clFbG">
            <node concept="37vLTw" id="H" role="37vLTJ">
              <ref role="3cqZAo" node="8" resolve="myName" />
            </node>
            <node concept="37vLTw" id="I" role="37vLTx">
              <ref role="3cqZAo" node="B" resolve="name" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="F" role="3cqZAp">
          <node concept="37vLTI" id="J" role="3clFbG">
            <node concept="37vLTw" id="K" role="37vLTJ">
              <ref role="3cqZAo" node="a" resolve="myValue" />
            </node>
            <node concept="37vLTw" id="L" role="37vLTx">
              <ref role="3cqZAo" node="C" resolve="value" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="B" role="3clF46">
        <property role="TrG5h" value="name" />
        <node concept="17QB3L" id="M" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="C" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="N" role="1tU5fm" />
      </node>
      <node concept="3Tm6S6" id="D" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="c" role="jymVt">
      <property role="TrG5h" value="getValue" />
      <node concept="3clFbS" id="O" role="3clF47">
        <node concept="3cpWs6" id="R" role="3cqZAp">
          <node concept="37vLTw" id="S" role="3cqZAk">
            <ref role="3cqZAo" node="a" resolve="myValue" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="P" role="3clF45" />
      <node concept="3Tm1VV" id="Q" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="d" role="jymVt">
      <property role="TrG5h" value="getValueAsString" />
      <node concept="3clFbS" id="T" role="3clF47">
        <node concept="3cpWs6" id="W" role="3cqZAp">
          <node concept="37vLTw" id="X" role="3cqZAk">
            <ref role="3cqZAo" node="a" resolve="myValue" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="U" role="3clF45" />
      <node concept="3Tm1VV" id="V" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="e" role="jymVt">
      <property role="TrG5h" value="getConstants" />
      <node concept="3clFbS" id="Y" role="3clF47">
        <node concept="3cpWs8" id="11" role="3cqZAp">
          <node concept="3cpWsn" id="19" role="3cpWs9">
            <property role="TrG5h" value="list" />
            <node concept="_YKpA" id="1a" role="1tU5fm">
              <node concept="3uibUv" id="1c" role="_ZDj9">
                <ref role="3uigEE" node="0" resolve="COMP" />
              </node>
            </node>
            <node concept="2ShNRf" id="1b" role="33vP2m">
              <node concept="2Jqq0_" id="1d" role="2ShVmc">
                <node concept="3uibUv" id="1e" role="HW$YZ">
                  <ref role="3uigEE" node="0" resolve="COMP" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="12" role="3cqZAp">
          <node concept="2OqwBi" id="1f" role="3clFbG">
            <node concept="37vLTw" id="1g" role="2Oq$k0">
              <ref role="3cqZAo" node="19" resolve="list" />
            </node>
            <node concept="TSZUe" id="1h" role="2OqNvi">
              <node concept="Rm8GO" id="1i" role="25WWJ7">
                <ref role="Rm8GQ" node="1" resolve="lt" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="13" role="3cqZAp">
          <node concept="2OqwBi" id="1j" role="3clFbG">
            <node concept="37vLTw" id="1k" role="2Oq$k0">
              <ref role="3cqZAo" node="19" resolve="list" />
            </node>
            <node concept="TSZUe" id="1l" role="2OqNvi">
              <node concept="Rm8GO" id="1m" role="25WWJ7">
                <ref role="Rm8GQ" node="2" resolve="gt" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="14" role="3cqZAp">
          <node concept="2OqwBi" id="1n" role="3clFbG">
            <node concept="37vLTw" id="1o" role="2Oq$k0">
              <ref role="3cqZAo" node="19" resolve="list" />
            </node>
            <node concept="TSZUe" id="1p" role="2OqNvi">
              <node concept="Rm8GO" id="1q" role="25WWJ7">
                <ref role="Rm8GQ" node="3" resolve="le" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="15" role="3cqZAp">
          <node concept="2OqwBi" id="1r" role="3clFbG">
            <node concept="37vLTw" id="1s" role="2Oq$k0">
              <ref role="3cqZAo" node="19" resolve="list" />
            </node>
            <node concept="TSZUe" id="1t" role="2OqNvi">
              <node concept="Rm8GO" id="1u" role="25WWJ7">
                <ref role="Rm8GQ" node="4" resolve="ge" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="16" role="3cqZAp">
          <node concept="2OqwBi" id="1v" role="3clFbG">
            <node concept="37vLTw" id="1w" role="2Oq$k0">
              <ref role="3cqZAo" node="19" resolve="list" />
            </node>
            <node concept="TSZUe" id="1x" role="2OqNvi">
              <node concept="Rm8GO" id="1y" role="25WWJ7">
                <ref role="Rm8GQ" node="5" resolve="eq" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="17" role="3cqZAp">
          <node concept="2OqwBi" id="1z" role="3clFbG">
            <node concept="37vLTw" id="1$" role="2Oq$k0">
              <ref role="3cqZAo" node="19" resolve="list" />
            </node>
            <node concept="TSZUe" id="1_" role="2OqNvi">
              <node concept="Rm8GO" id="1A" role="25WWJ7">
                <ref role="Rm8GQ" node="6" resolve="ne" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="18" role="3cqZAp">
          <node concept="37vLTw" id="1B" role="3cqZAk">
            <ref role="3cqZAo" node="19" resolve="list" />
          </node>
        </node>
      </node>
      <node concept="_YKpA" id="Z" role="3clF45">
        <node concept="3uibUv" id="1C" role="_ZDj9">
          <ref role="3uigEE" node="0" resolve="COMP" />
        </node>
      </node>
      <node concept="3Tm1VV" id="10" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="f" role="jymVt">
      <property role="TrG5h" value="getDefault" />
      <node concept="3clFbS" id="1D" role="3clF47">
        <node concept="3cpWs6" id="1G" role="3cqZAp">
          <node concept="10Nm6u" id="1H" role="3cqZAk" />
        </node>
      </node>
      <node concept="3uibUv" id="1E" role="3clF45">
        <ref role="3uigEE" node="0" resolve="COMP" />
      </node>
      <node concept="3Tm1VV" id="1F" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="g" role="jymVt">
      <property role="TrG5h" value="parseValue" />
      <node concept="3clFbS" id="1I" role="3clF47">
        <node concept="3clFbJ" id="1M" role="3cqZAp">
          <node concept="3clFbS" id="1U" role="3clFbx">
            <node concept="3cpWs6" id="1W" role="3cqZAp">
              <node concept="2YIFZM" id="1X" role="3cqZAk">
                <ref role="37wK5l" node="f" resolve="getDefault" />
                <ref role="1Pybhc" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="1V" role="3clFbw">
            <node concept="10Nm6u" id="1Y" role="3uHU7w" />
            <node concept="37vLTw" id="1Z" role="3uHU7B">
              <ref role="3cqZAo" node="1K" resolve="value" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1N" role="3cqZAp">
          <node concept="3clFbS" id="20" role="3clFbx">
            <node concept="3cpWs6" id="22" role="3cqZAp">
              <node concept="Rm8GO" id="23" role="3cqZAk">
                <ref role="Rm8GQ" node="1" resolve="lt" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="21" role="3clFbw">
            <node concept="37vLTw" id="24" role="2Oq$k0">
              <ref role="3cqZAo" node="1K" resolve="value" />
            </node>
            <node concept="liA8E" id="25" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="26" role="37wK5m">
                <node concept="Rm8GO" id="27" role="2Oq$k0">
                  <ref role="Rm8GQ" node="1" resolve="lt" />
                  <ref role="1Px2BO" node="0" resolve="COMP" />
                </node>
                <node concept="liA8E" id="28" role="2OqNvi">
                  <ref role="37wK5l" node="d" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1O" role="3cqZAp">
          <node concept="3clFbS" id="29" role="3clFbx">
            <node concept="3cpWs6" id="2b" role="3cqZAp">
              <node concept="Rm8GO" id="2c" role="3cqZAk">
                <ref role="Rm8GQ" node="2" resolve="gt" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2a" role="3clFbw">
            <node concept="37vLTw" id="2d" role="2Oq$k0">
              <ref role="3cqZAo" node="1K" resolve="value" />
            </node>
            <node concept="liA8E" id="2e" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="2f" role="37wK5m">
                <node concept="Rm8GO" id="2g" role="2Oq$k0">
                  <ref role="Rm8GQ" node="2" resolve="gt" />
                  <ref role="1Px2BO" node="0" resolve="COMP" />
                </node>
                <node concept="liA8E" id="2h" role="2OqNvi">
                  <ref role="37wK5l" node="d" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1P" role="3cqZAp">
          <node concept="3clFbS" id="2i" role="3clFbx">
            <node concept="3cpWs6" id="2k" role="3cqZAp">
              <node concept="Rm8GO" id="2l" role="3cqZAk">
                <ref role="Rm8GQ" node="3" resolve="le" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2j" role="3clFbw">
            <node concept="37vLTw" id="2m" role="2Oq$k0">
              <ref role="3cqZAo" node="1K" resolve="value" />
            </node>
            <node concept="liA8E" id="2n" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="2o" role="37wK5m">
                <node concept="Rm8GO" id="2p" role="2Oq$k0">
                  <ref role="Rm8GQ" node="3" resolve="le" />
                  <ref role="1Px2BO" node="0" resolve="COMP" />
                </node>
                <node concept="liA8E" id="2q" role="2OqNvi">
                  <ref role="37wK5l" node="d" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1Q" role="3cqZAp">
          <node concept="3clFbS" id="2r" role="3clFbx">
            <node concept="3cpWs6" id="2t" role="3cqZAp">
              <node concept="Rm8GO" id="2u" role="3cqZAk">
                <ref role="Rm8GQ" node="4" resolve="ge" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2s" role="3clFbw">
            <node concept="37vLTw" id="2v" role="2Oq$k0">
              <ref role="3cqZAo" node="1K" resolve="value" />
            </node>
            <node concept="liA8E" id="2w" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="2x" role="37wK5m">
                <node concept="Rm8GO" id="2y" role="2Oq$k0">
                  <ref role="Rm8GQ" node="4" resolve="ge" />
                  <ref role="1Px2BO" node="0" resolve="COMP" />
                </node>
                <node concept="liA8E" id="2z" role="2OqNvi">
                  <ref role="37wK5l" node="d" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1R" role="3cqZAp">
          <node concept="3clFbS" id="2$" role="3clFbx">
            <node concept="3cpWs6" id="2A" role="3cqZAp">
              <node concept="Rm8GO" id="2B" role="3cqZAk">
                <ref role="Rm8GQ" node="5" resolve="eq" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2_" role="3clFbw">
            <node concept="37vLTw" id="2C" role="2Oq$k0">
              <ref role="3cqZAo" node="1K" resolve="value" />
            </node>
            <node concept="liA8E" id="2D" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="2E" role="37wK5m">
                <node concept="Rm8GO" id="2F" role="2Oq$k0">
                  <ref role="Rm8GQ" node="5" resolve="eq" />
                  <ref role="1Px2BO" node="0" resolve="COMP" />
                </node>
                <node concept="liA8E" id="2G" role="2OqNvi">
                  <ref role="37wK5l" node="d" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="1S" role="3cqZAp">
          <node concept="3clFbS" id="2H" role="3clFbx">
            <node concept="3cpWs6" id="2J" role="3cqZAp">
              <node concept="Rm8GO" id="2K" role="3cqZAk">
                <ref role="Rm8GQ" node="6" resolve="ne" />
                <ref role="1Px2BO" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2I" role="3clFbw">
            <node concept="37vLTw" id="2L" role="2Oq$k0">
              <ref role="3cqZAo" node="1K" resolve="value" />
            </node>
            <node concept="liA8E" id="2M" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="2N" role="37wK5m">
                <node concept="Rm8GO" id="2O" role="2Oq$k0">
                  <ref role="Rm8GQ" node="6" resolve="ne" />
                  <ref role="1Px2BO" node="0" resolve="COMP" />
                </node>
                <node concept="liA8E" id="2P" role="2OqNvi">
                  <ref role="37wK5l" node="d" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="1T" role="3cqZAp">
          <node concept="2YIFZM" id="2Q" role="3cqZAk">
            <ref role="37wK5l" node="f" resolve="getDefault" />
            <ref role="1Pybhc" node="0" resolve="COMP" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="1J" role="3clF45">
        <ref role="3uigEE" node="0" resolve="COMP" />
      </node>
      <node concept="37vLTG" id="1K" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="2R" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="1L" role="1B3o_S" />
    </node>
  </node>
  <node concept="312cEu" id="2S">
    <property role="TrG5h" value="COMP_PropertySupport" />
    <node concept="3uibUv" id="2T" role="1zkMxy">
      <ref role="3uigEE" to="w1kc:~PropertySupport" resolve="PropertySupport" />
    </node>
    <node concept="3Tm1VV" id="2U" role="1B3o_S" />
    <node concept="3clFb_" id="2V" role="jymVt">
      <property role="TrG5h" value="canSetValue" />
      <node concept="3clFbS" id="2Y" role="3clF47">
        <node concept="3clFbJ" id="32" role="3cqZAp">
          <node concept="3clFbS" id="36" role="3clFbx">
            <node concept="3cpWs6" id="38" role="3cqZAp">
              <node concept="3clFbT" id="39" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="37" role="3clFbw">
            <node concept="37vLTw" id="3a" role="3uHU7B">
              <ref role="3cqZAo" node="30" resolve="value" />
            </node>
            <node concept="10Nm6u" id="3b" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs8" id="33" role="3cqZAp">
          <node concept="3cpWsn" id="3c" role="3cpWs9">
            <property role="TrG5h" value="constants" />
            <node concept="uOF1S" id="3d" role="1tU5fm">
              <node concept="3uibUv" id="3f" role="uOL27">
                <ref role="3uigEE" node="0" resolve="COMP" />
              </node>
            </node>
            <node concept="2OqwBi" id="3e" role="33vP2m">
              <node concept="2YIFZM" id="3g" role="2Oq$k0">
                <ref role="37wK5l" node="e" resolve="getConstants" />
                <ref role="1Pybhc" node="0" resolve="COMP" />
              </node>
              <node concept="uNJiE" id="3h" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="2$JKZl" id="34" role="3cqZAp">
          <node concept="3clFbS" id="3i" role="2LFqv$">
            <node concept="3cpWs8" id="3k" role="3cqZAp">
              <node concept="3cpWsn" id="3m" role="3cpWs9">
                <property role="TrG5h" value="constant" />
                <node concept="3uibUv" id="3n" role="1tU5fm">
                  <ref role="3uigEE" node="0" resolve="COMP" />
                </node>
                <node concept="2OqwBi" id="3o" role="33vP2m">
                  <node concept="37vLTw" id="3p" role="2Oq$k0">
                    <ref role="3cqZAo" node="3c" resolve="constants" />
                  </node>
                  <node concept="v1n4t" id="3q" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3l" role="3cqZAp">
              <node concept="3clFbS" id="3r" role="3clFbx">
                <node concept="3cpWs6" id="3t" role="3cqZAp">
                  <node concept="3clFbT" id="3u" role="3cqZAk">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="3s" role="3clFbw">
                <node concept="37vLTw" id="3v" role="2Oq$k0">
                  <ref role="3cqZAo" node="30" resolve="value" />
                </node>
                <node concept="liA8E" id="3w" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                  <node concept="2OqwBi" id="3x" role="37wK5m">
                    <node concept="37vLTw" id="3y" role="2Oq$k0">
                      <ref role="3cqZAo" node="3m" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="3z" role="2OqNvi">
                      <ref role="37wK5l" node="9" resolve="getName" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="3j" role="2$JKZa">
            <node concept="37vLTw" id="3$" role="2Oq$k0">
              <ref role="3cqZAo" node="3c" resolve="constants" />
            </node>
            <node concept="v0PNk" id="3_" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="35" role="3cqZAp">
          <node concept="3clFbT" id="3A" role="3cqZAk">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="2Z" role="3clF45" />
      <node concept="37vLTG" id="30" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="3B" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="31" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="2W" role="jymVt">
      <property role="TrG5h" value="toInternalValue" />
      <node concept="3clFbS" id="3C" role="3clF47">
        <node concept="3clFbJ" id="3G" role="3cqZAp">
          <node concept="3clFbS" id="3K" role="3clFbx">
            <node concept="3cpWs6" id="3M" role="3cqZAp">
              <node concept="10Nm6u" id="3N" role="3cqZAk" />
            </node>
          </node>
          <node concept="3clFbC" id="3L" role="3clFbw">
            <node concept="37vLTw" id="3O" role="3uHU7B">
              <ref role="3cqZAo" node="3E" resolve="value" />
            </node>
            <node concept="10Nm6u" id="3P" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs8" id="3H" role="3cqZAp">
          <node concept="3cpWsn" id="3Q" role="3cpWs9">
            <property role="TrG5h" value="constants" />
            <node concept="2OqwBi" id="3R" role="33vP2m">
              <node concept="2YIFZM" id="3T" role="2Oq$k0">
                <ref role="37wK5l" node="e" resolve="getConstants" />
                <ref role="1Pybhc" node="0" resolve="COMP" />
              </node>
              <node concept="uNJiE" id="3U" role="2OqNvi" />
            </node>
            <node concept="uOF1S" id="3S" role="1tU5fm">
              <node concept="3uibUv" id="3V" role="uOL27">
                <ref role="3uigEE" node="0" resolve="COMP" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2$JKZl" id="3I" role="3cqZAp">
          <node concept="3clFbS" id="3W" role="2LFqv$">
            <node concept="3cpWs8" id="3Y" role="3cqZAp">
              <node concept="3cpWsn" id="40" role="3cpWs9">
                <property role="TrG5h" value="constant" />
                <node concept="3uibUv" id="41" role="1tU5fm">
                  <ref role="3uigEE" node="0" resolve="COMP" />
                </node>
                <node concept="2OqwBi" id="42" role="33vP2m">
                  <node concept="37vLTw" id="43" role="2Oq$k0">
                    <ref role="3cqZAo" node="3Q" resolve="constants" />
                  </node>
                  <node concept="v1n4t" id="44" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="3Z" role="3cqZAp">
              <node concept="3clFbS" id="45" role="3clFbx">
                <node concept="3cpWs6" id="47" role="3cqZAp">
                  <node concept="2OqwBi" id="48" role="3cqZAk">
                    <node concept="37vLTw" id="49" role="2Oq$k0">
                      <ref role="3cqZAo" node="40" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="4a" role="2OqNvi">
                      <ref role="37wK5l" node="d" resolve="getValueAsString" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="46" role="3clFbw">
                <node concept="37vLTw" id="4b" role="2Oq$k0">
                  <ref role="3cqZAo" node="3E" resolve="value" />
                </node>
                <node concept="liA8E" id="4c" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                  <node concept="2OqwBi" id="4d" role="37wK5m">
                    <node concept="37vLTw" id="4e" role="2Oq$k0">
                      <ref role="3cqZAo" node="40" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="4f" role="2OqNvi">
                      <ref role="37wK5l" node="9" resolve="getName" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="3X" role="2$JKZa">
            <node concept="37vLTw" id="4g" role="2Oq$k0">
              <ref role="3cqZAo" node="3Q" resolve="constants" />
            </node>
            <node concept="v0PNk" id="4h" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="3J" role="3cqZAp">
          <node concept="10Nm6u" id="4i" role="3cqZAk" />
        </node>
      </node>
      <node concept="17QB3L" id="3D" role="3clF45" />
      <node concept="37vLTG" id="3E" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="4j" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="3F" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="2X" role="jymVt">
      <property role="TrG5h" value="fromInternalValue" />
      <node concept="3clFbS" id="4k" role="3clF47">
        <node concept="3cpWs8" id="4o" role="3cqZAp">
          <node concept="3cpWsn" id="4r" role="3cpWs9">
            <property role="TrG5h" value="constant" />
            <node concept="3uibUv" id="4s" role="1tU5fm">
              <ref role="3uigEE" node="0" resolve="COMP" />
            </node>
            <node concept="2YIFZM" id="4t" role="33vP2m">
              <ref role="37wK5l" node="g" resolve="parseValue" />
              <ref role="1Pybhc" node="0" resolve="COMP" />
              <node concept="37vLTw" id="4u" role="37wK5m">
                <ref role="3cqZAo" node="4m" resolve="value" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="4p" role="3cqZAp">
          <node concept="3clFbS" id="4v" role="3clFbx">
            <node concept="3cpWs6" id="4x" role="3cqZAp">
              <node concept="2OqwBi" id="4y" role="3cqZAk">
                <node concept="37vLTw" id="4z" role="2Oq$k0">
                  <ref role="3cqZAo" node="4r" resolve="constant" />
                </node>
                <node concept="liA8E" id="4$" role="2OqNvi">
                  <ref role="37wK5l" node="9" resolve="getName" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="4w" role="3clFbw">
            <node concept="37vLTw" id="4_" role="3uHU7B">
              <ref role="3cqZAo" node="4r" resolve="constant" />
            </node>
            <node concept="10Nm6u" id="4A" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs6" id="4q" role="3cqZAp">
          <node concept="Xl_RD" id="4B" role="3cqZAk">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4l" role="3clF45" />
      <node concept="37vLTG" id="4m" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="4C" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="4n" role="1B3o_S" />
    </node>
  </node>
  <node concept="Qs71p" id="4D">
    <property role="TrG5h" value="COND" />
    <node concept="QsSxf" id="4E" role="Qtgdg">
      <property role="TrG5h" value="and" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="4Q" role="37wK5m">
        <property role="Xl_RC" value="and" />
      </node>
      <node concept="Xl_RD" id="4R" role="37wK5m">
        <property role="Xl_RC" value="&amp;&amp;" />
      </node>
    </node>
    <node concept="QsSxf" id="4F" role="Qtgdg">
      <property role="TrG5h" value="or" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="4S" role="37wK5m">
        <property role="Xl_RC" value="or" />
      </node>
      <node concept="Xl_RD" id="4T" role="37wK5m">
        <property role="Xl_RC" value="||" />
      </node>
    </node>
    <node concept="3Tm1VV" id="4G" role="1B3o_S" />
    <node concept="312cEg" id="4H" role="jymVt">
      <property role="TrG5h" value="myName" />
      <property role="3TUv4t" value="true" />
      <node concept="17QB3L" id="4U" role="1tU5fm" />
      <node concept="3Tm6S6" id="4V" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="4I" role="jymVt">
      <property role="TrG5h" value="getName" />
      <node concept="3clFbS" id="4W" role="3clF47">
        <node concept="3cpWs6" id="4Z" role="3cqZAp">
          <node concept="37vLTw" id="50" role="3cqZAk">
            <ref role="3cqZAo" node="4H" resolve="myName" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="4X" role="3clF45" />
      <node concept="3Tm1VV" id="4Y" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="4J" role="jymVt">
      <property role="TrG5h" value="myValue" />
      <property role="3TUv4t" value="true" />
      <node concept="17QB3L" id="51" role="1tU5fm" />
      <node concept="3Tm6S6" id="52" role="1B3o_S" />
    </node>
    <node concept="3clFbW" id="4K" role="jymVt">
      <node concept="3clFbS" id="53" role="3clF47">
        <node concept="3clFbF" id="57" role="3cqZAp">
          <node concept="37vLTI" id="59" role="3clFbG">
            <node concept="37vLTw" id="5a" role="37vLTJ">
              <ref role="3cqZAo" node="4H" resolve="myName" />
            </node>
            <node concept="37vLTw" id="5b" role="37vLTx">
              <ref role="3cqZAo" node="54" resolve="name" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="58" role="3cqZAp">
          <node concept="37vLTI" id="5c" role="3clFbG">
            <node concept="37vLTw" id="5d" role="37vLTJ">
              <ref role="3cqZAo" node="4J" resolve="myValue" />
            </node>
            <node concept="37vLTw" id="5e" role="37vLTx">
              <ref role="3cqZAo" node="55" resolve="value" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="54" role="3clF46">
        <property role="TrG5h" value="name" />
        <node concept="17QB3L" id="5f" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="55" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="5g" role="1tU5fm" />
      </node>
      <node concept="3Tm6S6" id="56" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="4L" role="jymVt">
      <property role="TrG5h" value="getValue" />
      <node concept="3clFbS" id="5h" role="3clF47">
        <node concept="3cpWs6" id="5k" role="3cqZAp">
          <node concept="37vLTw" id="5l" role="3cqZAk">
            <ref role="3cqZAo" node="4J" resolve="myValue" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="5i" role="3clF45" />
      <node concept="3Tm1VV" id="5j" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="4M" role="jymVt">
      <property role="TrG5h" value="getValueAsString" />
      <node concept="3clFbS" id="5m" role="3clF47">
        <node concept="3cpWs6" id="5p" role="3cqZAp">
          <node concept="37vLTw" id="5q" role="3cqZAk">
            <ref role="3cqZAo" node="4J" resolve="myValue" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="5n" role="3clF45" />
      <node concept="3Tm1VV" id="5o" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="4N" role="jymVt">
      <property role="TrG5h" value="getConstants" />
      <node concept="3clFbS" id="5r" role="3clF47">
        <node concept="3cpWs8" id="5u" role="3cqZAp">
          <node concept="3cpWsn" id="5y" role="3cpWs9">
            <property role="TrG5h" value="list" />
            <node concept="_YKpA" id="5z" role="1tU5fm">
              <node concept="3uibUv" id="5_" role="_ZDj9">
                <ref role="3uigEE" node="4D" resolve="COND" />
              </node>
            </node>
            <node concept="2ShNRf" id="5$" role="33vP2m">
              <node concept="2Jqq0_" id="5A" role="2ShVmc">
                <node concept="3uibUv" id="5B" role="HW$YZ">
                  <ref role="3uigEE" node="4D" resolve="COND" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5v" role="3cqZAp">
          <node concept="2OqwBi" id="5C" role="3clFbG">
            <node concept="37vLTw" id="5D" role="2Oq$k0">
              <ref role="3cqZAo" node="5y" resolve="list" />
            </node>
            <node concept="TSZUe" id="5E" role="2OqNvi">
              <node concept="Rm8GO" id="5F" role="25WWJ7">
                <ref role="Rm8GQ" node="4E" resolve="and" />
                <ref role="1Px2BO" node="4D" resolve="COND" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="5w" role="3cqZAp">
          <node concept="2OqwBi" id="5G" role="3clFbG">
            <node concept="37vLTw" id="5H" role="2Oq$k0">
              <ref role="3cqZAo" node="5y" resolve="list" />
            </node>
            <node concept="TSZUe" id="5I" role="2OqNvi">
              <node concept="Rm8GO" id="5J" role="25WWJ7">
                <ref role="Rm8GQ" node="4F" resolve="or" />
                <ref role="1Px2BO" node="4D" resolve="COND" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5x" role="3cqZAp">
          <node concept="37vLTw" id="5K" role="3cqZAk">
            <ref role="3cqZAo" node="5y" resolve="list" />
          </node>
        </node>
      </node>
      <node concept="_YKpA" id="5s" role="3clF45">
        <node concept="3uibUv" id="5L" role="_ZDj9">
          <ref role="3uigEE" node="4D" resolve="COND" />
        </node>
      </node>
      <node concept="3Tm1VV" id="5t" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="4O" role="jymVt">
      <property role="TrG5h" value="getDefault" />
      <node concept="3clFbS" id="5M" role="3clF47">
        <node concept="3cpWs6" id="5P" role="3cqZAp">
          <node concept="10Nm6u" id="5Q" role="3cqZAk" />
        </node>
      </node>
      <node concept="3uibUv" id="5N" role="3clF45">
        <ref role="3uigEE" node="4D" resolve="COND" />
      </node>
      <node concept="3Tm1VV" id="5O" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="4P" role="jymVt">
      <property role="TrG5h" value="parseValue" />
      <node concept="3clFbS" id="5R" role="3clF47">
        <node concept="3clFbJ" id="5V" role="3cqZAp">
          <node concept="3clFbS" id="5Z" role="3clFbx">
            <node concept="3cpWs6" id="61" role="3cqZAp">
              <node concept="2YIFZM" id="62" role="3cqZAk">
                <ref role="37wK5l" node="4O" resolve="getDefault" />
                <ref role="1Pybhc" node="4D" resolve="COND" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="60" role="3clFbw">
            <node concept="10Nm6u" id="63" role="3uHU7w" />
            <node concept="37vLTw" id="64" role="3uHU7B">
              <ref role="3cqZAo" node="5T" resolve="value" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5W" role="3cqZAp">
          <node concept="3clFbS" id="65" role="3clFbx">
            <node concept="3cpWs6" id="67" role="3cqZAp">
              <node concept="Rm8GO" id="68" role="3cqZAk">
                <ref role="Rm8GQ" node="4E" resolve="and" />
                <ref role="1Px2BO" node="4D" resolve="COND" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="66" role="3clFbw">
            <node concept="37vLTw" id="69" role="2Oq$k0">
              <ref role="3cqZAo" node="5T" resolve="value" />
            </node>
            <node concept="liA8E" id="6a" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="6b" role="37wK5m">
                <node concept="Rm8GO" id="6c" role="2Oq$k0">
                  <ref role="Rm8GQ" node="4E" resolve="and" />
                  <ref role="1Px2BO" node="4D" resolve="COND" />
                </node>
                <node concept="liA8E" id="6d" role="2OqNvi">
                  <ref role="37wK5l" node="4M" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="5X" role="3cqZAp">
          <node concept="3clFbS" id="6e" role="3clFbx">
            <node concept="3cpWs6" id="6g" role="3cqZAp">
              <node concept="Rm8GO" id="6h" role="3cqZAk">
                <ref role="Rm8GQ" node="4F" resolve="or" />
                <ref role="1Px2BO" node="4D" resolve="COND" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6f" role="3clFbw">
            <node concept="37vLTw" id="6i" role="2Oq$k0">
              <ref role="3cqZAo" node="5T" resolve="value" />
            </node>
            <node concept="liA8E" id="6j" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="6k" role="37wK5m">
                <node concept="Rm8GO" id="6l" role="2Oq$k0">
                  <ref role="Rm8GQ" node="4F" resolve="or" />
                  <ref role="1Px2BO" node="4D" resolve="COND" />
                </node>
                <node concept="liA8E" id="6m" role="2OqNvi">
                  <ref role="37wK5l" node="4M" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="5Y" role="3cqZAp">
          <node concept="2YIFZM" id="6n" role="3cqZAk">
            <ref role="37wK5l" node="4O" resolve="getDefault" />
            <ref role="1Pybhc" node="4D" resolve="COND" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="5S" role="3clF45">
        <ref role="3uigEE" node="4D" resolve="COND" />
      </node>
      <node concept="37vLTG" id="5T" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="6o" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="5U" role="1B3o_S" />
    </node>
  </node>
  <node concept="312cEu" id="6p">
    <property role="TrG5h" value="COND_PropertySupport" />
    <node concept="3uibUv" id="6q" role="1zkMxy">
      <ref role="3uigEE" to="w1kc:~PropertySupport" resolve="PropertySupport" />
    </node>
    <node concept="3Tm1VV" id="6r" role="1B3o_S" />
    <node concept="3clFb_" id="6s" role="jymVt">
      <property role="TrG5h" value="canSetValue" />
      <node concept="3clFbS" id="6v" role="3clF47">
        <node concept="3clFbJ" id="6z" role="3cqZAp">
          <node concept="3clFbS" id="6B" role="3clFbx">
            <node concept="3cpWs6" id="6D" role="3cqZAp">
              <node concept="3clFbT" id="6E" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="6C" role="3clFbw">
            <node concept="37vLTw" id="6F" role="3uHU7B">
              <ref role="3cqZAo" node="6x" resolve="value" />
            </node>
            <node concept="10Nm6u" id="6G" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs8" id="6$" role="3cqZAp">
          <node concept="3cpWsn" id="6H" role="3cpWs9">
            <property role="TrG5h" value="constants" />
            <node concept="uOF1S" id="6I" role="1tU5fm">
              <node concept="3uibUv" id="6K" role="uOL27">
                <ref role="3uigEE" node="4D" resolve="COND" />
              </node>
            </node>
            <node concept="2OqwBi" id="6J" role="33vP2m">
              <node concept="2YIFZM" id="6L" role="2Oq$k0">
                <ref role="37wK5l" node="4N" resolve="getConstants" />
                <ref role="1Pybhc" node="4D" resolve="COND" />
              </node>
              <node concept="uNJiE" id="6M" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="2$JKZl" id="6_" role="3cqZAp">
          <node concept="3clFbS" id="6N" role="2LFqv$">
            <node concept="3cpWs8" id="6P" role="3cqZAp">
              <node concept="3cpWsn" id="6R" role="3cpWs9">
                <property role="TrG5h" value="constant" />
                <node concept="3uibUv" id="6S" role="1tU5fm">
                  <ref role="3uigEE" node="4D" resolve="COND" />
                </node>
                <node concept="2OqwBi" id="6T" role="33vP2m">
                  <node concept="37vLTw" id="6U" role="2Oq$k0">
                    <ref role="3cqZAo" node="6H" resolve="constants" />
                  </node>
                  <node concept="v1n4t" id="6V" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="6Q" role="3cqZAp">
              <node concept="3clFbS" id="6W" role="3clFbx">
                <node concept="3cpWs6" id="6Y" role="3cqZAp">
                  <node concept="3clFbT" id="6Z" role="3cqZAk">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="6X" role="3clFbw">
                <node concept="37vLTw" id="70" role="2Oq$k0">
                  <ref role="3cqZAo" node="6x" resolve="value" />
                </node>
                <node concept="liA8E" id="71" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                  <node concept="2OqwBi" id="72" role="37wK5m">
                    <node concept="37vLTw" id="73" role="2Oq$k0">
                      <ref role="3cqZAo" node="6R" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="74" role="2OqNvi">
                      <ref role="37wK5l" node="4I" resolve="getName" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="6O" role="2$JKZa">
            <node concept="37vLTw" id="75" role="2Oq$k0">
              <ref role="3cqZAo" node="6H" resolve="constants" />
            </node>
            <node concept="v0PNk" id="76" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="6A" role="3cqZAp">
          <node concept="3clFbT" id="77" role="3cqZAk">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="6w" role="3clF45" />
      <node concept="37vLTG" id="6x" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="78" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="6y" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="6t" role="jymVt">
      <property role="TrG5h" value="toInternalValue" />
      <node concept="3clFbS" id="79" role="3clF47">
        <node concept="3clFbJ" id="7d" role="3cqZAp">
          <node concept="3clFbS" id="7h" role="3clFbx">
            <node concept="3cpWs6" id="7j" role="3cqZAp">
              <node concept="10Nm6u" id="7k" role="3cqZAk" />
            </node>
          </node>
          <node concept="3clFbC" id="7i" role="3clFbw">
            <node concept="37vLTw" id="7l" role="3uHU7B">
              <ref role="3cqZAo" node="7b" resolve="value" />
            </node>
            <node concept="10Nm6u" id="7m" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs8" id="7e" role="3cqZAp">
          <node concept="3cpWsn" id="7n" role="3cpWs9">
            <property role="TrG5h" value="constants" />
            <node concept="2OqwBi" id="7o" role="33vP2m">
              <node concept="2YIFZM" id="7q" role="2Oq$k0">
                <ref role="37wK5l" node="4N" resolve="getConstants" />
                <ref role="1Pybhc" node="4D" resolve="COND" />
              </node>
              <node concept="uNJiE" id="7r" role="2OqNvi" />
            </node>
            <node concept="uOF1S" id="7p" role="1tU5fm">
              <node concept="3uibUv" id="7s" role="uOL27">
                <ref role="3uigEE" node="4D" resolve="COND" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2$JKZl" id="7f" role="3cqZAp">
          <node concept="3clFbS" id="7t" role="2LFqv$">
            <node concept="3cpWs8" id="7v" role="3cqZAp">
              <node concept="3cpWsn" id="7x" role="3cpWs9">
                <property role="TrG5h" value="constant" />
                <node concept="3uibUv" id="7y" role="1tU5fm">
                  <ref role="3uigEE" node="4D" resolve="COND" />
                </node>
                <node concept="2OqwBi" id="7z" role="33vP2m">
                  <node concept="37vLTw" id="7$" role="2Oq$k0">
                    <ref role="3cqZAo" node="7n" resolve="constants" />
                  </node>
                  <node concept="v1n4t" id="7_" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="7w" role="3cqZAp">
              <node concept="3clFbS" id="7A" role="3clFbx">
                <node concept="3cpWs6" id="7C" role="3cqZAp">
                  <node concept="2OqwBi" id="7D" role="3cqZAk">
                    <node concept="37vLTw" id="7E" role="2Oq$k0">
                      <ref role="3cqZAo" node="7x" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="7F" role="2OqNvi">
                      <ref role="37wK5l" node="4M" resolve="getValueAsString" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="7B" role="3clFbw">
                <node concept="37vLTw" id="7G" role="2Oq$k0">
                  <ref role="3cqZAo" node="7b" resolve="value" />
                </node>
                <node concept="liA8E" id="7H" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                  <node concept="2OqwBi" id="7I" role="37wK5m">
                    <node concept="37vLTw" id="7J" role="2Oq$k0">
                      <ref role="3cqZAo" node="7x" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="7K" role="2OqNvi">
                      <ref role="37wK5l" node="4I" resolve="getName" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="7u" role="2$JKZa">
            <node concept="37vLTw" id="7L" role="2Oq$k0">
              <ref role="3cqZAo" node="7n" resolve="constants" />
            </node>
            <node concept="v0PNk" id="7M" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="7g" role="3cqZAp">
          <node concept="10Nm6u" id="7N" role="3cqZAk" />
        </node>
      </node>
      <node concept="17QB3L" id="7a" role="3clF45" />
      <node concept="37vLTG" id="7b" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="7O" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="7c" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="6u" role="jymVt">
      <property role="TrG5h" value="fromInternalValue" />
      <node concept="3clFbS" id="7P" role="3clF47">
        <node concept="3cpWs8" id="7T" role="3cqZAp">
          <node concept="3cpWsn" id="7W" role="3cpWs9">
            <property role="TrG5h" value="constant" />
            <node concept="3uibUv" id="7X" role="1tU5fm">
              <ref role="3uigEE" node="4D" resolve="COND" />
            </node>
            <node concept="2YIFZM" id="7Y" role="33vP2m">
              <ref role="37wK5l" node="4P" resolve="parseValue" />
              <ref role="1Pybhc" node="4D" resolve="COND" />
              <node concept="37vLTw" id="7Z" role="37wK5m">
                <ref role="3cqZAo" node="7R" resolve="value" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="7U" role="3cqZAp">
          <node concept="3clFbS" id="80" role="3clFbx">
            <node concept="3cpWs6" id="82" role="3cqZAp">
              <node concept="2OqwBi" id="83" role="3cqZAk">
                <node concept="37vLTw" id="84" role="2Oq$k0">
                  <ref role="3cqZAo" node="7W" resolve="constant" />
                </node>
                <node concept="liA8E" id="85" role="2OqNvi">
                  <ref role="37wK5l" node="4I" resolve="getName" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="81" role="3clFbw">
            <node concept="37vLTw" id="86" role="3uHU7B">
              <ref role="3cqZAo" node="7W" resolve="constant" />
            </node>
            <node concept="10Nm6u" id="87" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs6" id="7V" role="3cqZAp">
          <node concept="Xl_RD" id="88" role="3cqZAk">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="7Q" role="3clF45" />
      <node concept="37vLTG" id="7R" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="89" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="7S" role="1B3o_S" />
    </node>
  </node>
  <node concept="312cEu" id="8a">
    <property role="TrG5h" value="ConceptPresentationAspectImpl" />
    <node concept="3uibUv" id="8b" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~ConceptPresentationAspectBase" resolve="ConceptPresentationAspectBase" />
    </node>
    <node concept="312cEg" id="8c" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Action" />
      <node concept="3uibUv" id="8q" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8r" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8d" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Actuator" />
      <node concept="3uibUv" id="8s" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8t" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8e" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_App" />
      <node concept="3uibUv" id="8u" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8v" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8f" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Brick" />
      <node concept="3uibUv" id="8w" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8x" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8g" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Condition" />
      <node concept="3uibUv" id="8y" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8z" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8h" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_ConditionAnalogic" />
      <node concept="3uibUv" id="8$" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8_" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8i" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_ICondition" />
      <node concept="3uibUv" id="8A" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8B" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8j" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_LogicOp" />
      <node concept="3uibUv" id="8C" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8D" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8k" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Sensor" />
      <node concept="3uibUv" id="8E" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8F" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8l" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_State" />
      <node concept="3uibUv" id="8G" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8H" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="8m" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="false" />
      <property role="TrG5h" value="props_Transition" />
      <node concept="3uibUv" id="8I" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="3Tm6S6" id="8J" role="1B3o_S" />
    </node>
    <node concept="3Tm1VV" id="8n" role="1B3o_S" />
    <node concept="2tJIrI" id="8o" role="jymVt" />
    <node concept="3clFb_" id="8p" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDescriptor" />
      <property role="DiZV1" value="false" />
      <node concept="3Tm1VV" id="8K" role="1B3o_S" />
      <node concept="37vLTG" id="8L" role="3clF46">
        <property role="TrG5h" value="c" />
        <node concept="3uibUv" id="8Q" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
        </node>
      </node>
      <node concept="3clFbS" id="8M" role="3clF47">
        <node concept="3cpWs8" id="8R" role="3cqZAp">
          <node concept="3cpWsn" id="8U" role="3cpWs9">
            <property role="TrG5h" value="structureDescriptor" />
            <node concept="3uibUv" id="8V" role="1tU5fm">
              <ref role="3uigEE" node="lk" resolve="StructureAspectDescriptor" />
            </node>
            <node concept="10QFUN" id="8W" role="33vP2m">
              <node concept="3uibUv" id="8X" role="10QFUM">
                <ref role="3uigEE" node="lk" resolve="StructureAspectDescriptor" />
              </node>
              <node concept="2OqwBi" id="8Y" role="10QFUP">
                <node concept="37vLTw" id="8Z" role="2Oq$k0">
                  <ref role="3cqZAo" to="ze1i:~ConceptPresentationAspectBase.myLanguageRuntime" resolve="myLanguageRuntime" />
                </node>
                <node concept="liA8E" id="90" role="2OqNvi">
                  <ref role="37wK5l" to="vndm:~LanguageRuntime.getAspect(java.lang.Class):jetbrains.mps.smodel.runtime.ILanguageAspect" resolve="getAspect" />
                  <node concept="3VsKOn" id="91" role="37wK5m">
                    <ref role="3VsUkX" to="ze1i:~StructureAspectDescriptor" resolve="StructureAspectDescriptor" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3KaCP$" id="8S" role="3cqZAp">
          <node concept="2OqwBi" id="92" role="3KbGdf">
            <node concept="37vLTw" id="9e" role="2Oq$k0">
              <ref role="3cqZAo" node="8U" resolve="structureDescriptor" />
            </node>
            <node concept="liA8E" id="9f" role="2OqNvi">
              <ref role="37wK5l" node="lE" resolve="internalIndex" />
              <node concept="37vLTw" id="9g" role="37wK5m">
                <ref role="3cqZAo" node="8L" resolve="c" />
              </node>
            </node>
          </node>
          <node concept="3KbdKl" id="93" role="3KbHQx">
            <node concept="3clFbS" id="9h" role="3Kbo56">
              <node concept="3clFbJ" id="9j" role="3cqZAp">
                <node concept="3clFbS" id="9l" role="3clFbx">
                  <node concept="3cpWs8" id="9n" role="3cqZAp">
                    <node concept="3cpWsn" id="9q" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="9r" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="9s" role="33vP2m">
                        <node concept="1pGfFk" id="9t" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9o" role="3cqZAp">
                    <node concept="2OqwBi" id="9u" role="3clFbG">
                      <node concept="37vLTw" id="9v" role="2Oq$k0">
                        <ref role="3cqZAo" node="9q" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="9w" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByReference(long,long,long,long,java.lang.String,java.lang.String,java.lang.String):jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByReference" />
                        <node concept="1adDum" id="9x" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="9y" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="9z" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a57389a0L" />
                        </node>
                        <node concept="1adDum" id="9$" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a57427daL" />
                        </node>
                        <node concept="Xl_RD" id="9_" role="37wK5m">
                          <property role="Xl_RC" value="target" />
                        </node>
                        <node concept="Xl_RD" id="9A" role="37wK5m">
                          <property role="Xl_RC" value="" />
                        </node>
                        <node concept="Xl_RD" id="9B" role="37wK5m">
                          <property role="Xl_RC" value="" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9p" role="3cqZAp">
                    <node concept="37vLTI" id="9C" role="3clFbG">
                      <node concept="2OqwBi" id="9D" role="37vLTx">
                        <node concept="37vLTw" id="9F" role="2Oq$k0">
                          <ref role="3cqZAo" node="9q" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="9G" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="9E" role="37vLTJ">
                        <ref role="3cqZAo" node="8c" resolve="props_Action" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="9m" role="3clFbw">
                  <node concept="10Nm6u" id="9H" role="3uHU7w" />
                  <node concept="37vLTw" id="9I" role="3uHU7B">
                    <ref role="3cqZAo" node="8c" resolve="props_Action" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="9k" role="3cqZAp">
                <node concept="37vLTw" id="9J" role="3cqZAk">
                  <ref role="3cqZAo" node="8c" resolve="props_Action" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="9i" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fp" resolve="Action" />
            </node>
          </node>
          <node concept="3KbdKl" id="94" role="3KbHQx">
            <node concept="3clFbS" id="9K" role="3Kbo56">
              <node concept="3clFbJ" id="9M" role="3cqZAp">
                <node concept="3clFbS" id="9O" role="3clFbx">
                  <node concept="3cpWs8" id="9Q" role="3cqZAp">
                    <node concept="3cpWsn" id="9T" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="9U" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="9V" role="33vP2m">
                        <node concept="1pGfFk" id="9W" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9R" role="3cqZAp">
                    <node concept="2OqwBi" id="9X" role="3clFbG">
                      <node concept="37vLTw" id="9Y" role="2Oq$k0">
                        <ref role="3cqZAo" node="9T" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="9Z" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByName():jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByName" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="9S" role="3cqZAp">
                    <node concept="37vLTI" id="a0" role="3clFbG">
                      <node concept="2OqwBi" id="a1" role="37vLTx">
                        <node concept="37vLTw" id="a3" role="2Oq$k0">
                          <ref role="3cqZAo" node="9T" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="a4" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="a2" role="37vLTJ">
                        <ref role="3cqZAo" node="8d" resolve="props_Actuator" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="9P" role="3clFbw">
                  <node concept="10Nm6u" id="a5" role="3uHU7w" />
                  <node concept="37vLTw" id="a6" role="3uHU7B">
                    <ref role="3cqZAo" node="8d" resolve="props_Actuator" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="9N" role="3cqZAp">
                <node concept="37vLTw" id="a7" role="3cqZAk">
                  <ref role="3cqZAo" node="8d" resolve="props_Actuator" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="9L" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fq" resolve="Actuator" />
            </node>
          </node>
          <node concept="3KbdKl" id="95" role="3KbHQx">
            <node concept="3clFbS" id="a8" role="3Kbo56">
              <node concept="3clFbJ" id="aa" role="3cqZAp">
                <node concept="3clFbS" id="ac" role="3clFbx">
                  <node concept="3cpWs8" id="ae" role="3cqZAp">
                    <node concept="3cpWsn" id="ah" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="ai" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="aj" role="33vP2m">
                        <node concept="1pGfFk" id="ak" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="af" role="3cqZAp">
                    <node concept="2OqwBi" id="al" role="3clFbG">
                      <node concept="37vLTw" id="am" role="2Oq$k0">
                        <ref role="3cqZAo" node="ah" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="an" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByName():jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByName" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="ag" role="3cqZAp">
                    <node concept="37vLTI" id="ao" role="3clFbG">
                      <node concept="2OqwBi" id="ap" role="37vLTx">
                        <node concept="37vLTw" id="ar" role="2Oq$k0">
                          <ref role="3cqZAo" node="ah" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="as" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="aq" role="37vLTJ">
                        <ref role="3cqZAo" node="8e" resolve="props_App" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="ad" role="3clFbw">
                  <node concept="10Nm6u" id="at" role="3uHU7w" />
                  <node concept="37vLTw" id="au" role="3uHU7B">
                    <ref role="3cqZAo" node="8e" resolve="props_App" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="ab" role="3cqZAp">
                <node concept="37vLTw" id="av" role="3cqZAk">
                  <ref role="3cqZAo" node="8e" resolve="props_App" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="a9" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fr" resolve="App" />
            </node>
          </node>
          <node concept="3KbdKl" id="96" role="3KbHQx">
            <node concept="3clFbS" id="aw" role="3Kbo56">
              <node concept="3clFbJ" id="ay" role="3cqZAp">
                <node concept="3clFbS" id="a$" role="3clFbx">
                  <node concept="3cpWs8" id="aA" role="3cqZAp">
                    <node concept="3cpWsn" id="aD" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="aE" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="aF" role="33vP2m">
                        <node concept="1pGfFk" id="aG" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="aB" role="3cqZAp">
                    <node concept="2OqwBi" id="aH" role="3clFbG">
                      <node concept="37vLTw" id="aI" role="2Oq$k0">
                        <ref role="3cqZAo" node="aD" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="aJ" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByName():jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByName" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="aC" role="3cqZAp">
                    <node concept="37vLTI" id="aK" role="3clFbG">
                      <node concept="2OqwBi" id="aL" role="37vLTx">
                        <node concept="37vLTw" id="aN" role="2Oq$k0">
                          <ref role="3cqZAo" node="aD" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="aO" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="aM" role="37vLTJ">
                        <ref role="3cqZAo" node="8f" resolve="props_Brick" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="a_" role="3clFbw">
                  <node concept="10Nm6u" id="aP" role="3uHU7w" />
                  <node concept="37vLTw" id="aQ" role="3uHU7B">
                    <ref role="3cqZAo" node="8f" resolve="props_Brick" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="az" role="3cqZAp">
                <node concept="37vLTw" id="aR" role="3cqZAk">
                  <ref role="3cqZAo" node="8f" resolve="props_Brick" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="ax" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fs" resolve="Brick" />
            </node>
          </node>
          <node concept="3KbdKl" id="97" role="3KbHQx">
            <node concept="3clFbS" id="aS" role="3Kbo56">
              <node concept="3clFbJ" id="aU" role="3cqZAp">
                <node concept="3clFbS" id="aW" role="3clFbx">
                  <node concept="3cpWs8" id="aY" role="3cqZAp">
                    <node concept="3cpWsn" id="b1" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="b2" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="b3" role="33vP2m">
                        <node concept="1pGfFk" id="b4" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="aZ" role="3cqZAp">
                    <node concept="2OqwBi" id="b5" role="3clFbG">
                      <node concept="37vLTw" id="b6" role="2Oq$k0">
                        <ref role="3cqZAo" node="b1" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="b7" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByReference(long,long,long,long,java.lang.String,java.lang.String,java.lang.String):jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByReference" />
                        <node concept="1adDum" id="b8" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="b9" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="ba" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a574326fL" />
                        </node>
                        <node concept="1adDum" id="bb" role="37wK5m">
                          <property role="1adDun" value="0x6c655ac4f1eb8d19L" />
                        </node>
                        <node concept="Xl_RD" id="bc" role="37wK5m">
                          <property role="Xl_RC" value="sensor" />
                        </node>
                        <node concept="Xl_RD" id="bd" role="37wK5m">
                          <property role="Xl_RC" value="" />
                        </node>
                        <node concept="Xl_RD" id="be" role="37wK5m">
                          <property role="Xl_RC" value="" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="b0" role="3cqZAp">
                    <node concept="37vLTI" id="bf" role="3clFbG">
                      <node concept="2OqwBi" id="bg" role="37vLTx">
                        <node concept="37vLTw" id="bi" role="2Oq$k0">
                          <ref role="3cqZAo" node="b1" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="bj" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="bh" role="37vLTJ">
                        <ref role="3cqZAo" node="8g" resolve="props_Condition" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="aX" role="3clFbw">
                  <node concept="10Nm6u" id="bk" role="3uHU7w" />
                  <node concept="37vLTw" id="bl" role="3uHU7B">
                    <ref role="3cqZAo" node="8g" resolve="props_Condition" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="aV" role="3cqZAp">
                <node concept="37vLTw" id="bm" role="3cqZAk">
                  <ref role="3cqZAo" node="8g" resolve="props_Condition" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="aT" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="ft" resolve="Condition" />
            </node>
          </node>
          <node concept="3KbdKl" id="98" role="3KbHQx">
            <node concept="3clFbS" id="bn" role="3Kbo56">
              <node concept="3clFbJ" id="bp" role="3cqZAp">
                <node concept="3clFbS" id="br" role="3clFbx">
                  <node concept="3cpWs8" id="bt" role="3cqZAp">
                    <node concept="3cpWsn" id="bw" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="bx" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="by" role="33vP2m">
                        <node concept="1pGfFk" id="bz" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="bu" role="3cqZAp">
                    <node concept="2OqwBi" id="b$" role="3clFbG">
                      <node concept="37vLTw" id="b_" role="2Oq$k0">
                        <ref role="3cqZAo" node="bw" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="bA" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String):jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="rawPresentation" />
                        <node concept="Xl_RD" id="bB" role="37wK5m">
                          <property role="Xl_RC" value="ConditionAnalogic" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="bv" role="3cqZAp">
                    <node concept="37vLTI" id="bC" role="3clFbG">
                      <node concept="2OqwBi" id="bD" role="37vLTx">
                        <node concept="37vLTw" id="bF" role="2Oq$k0">
                          <ref role="3cqZAo" node="bw" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="bG" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="bE" role="37vLTJ">
                        <ref role="3cqZAo" node="8h" resolve="props_ConditionAnalogic" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="bs" role="3clFbw">
                  <node concept="10Nm6u" id="bH" role="3uHU7w" />
                  <node concept="37vLTw" id="bI" role="3uHU7B">
                    <ref role="3cqZAo" node="8h" resolve="props_ConditionAnalogic" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="bq" role="3cqZAp">
                <node concept="37vLTw" id="bJ" role="3cqZAk">
                  <ref role="3cqZAo" node="8h" resolve="props_ConditionAnalogic" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="bo" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fu" resolve="ConditionAnalogic" />
            </node>
          </node>
          <node concept="3KbdKl" id="99" role="3KbHQx">
            <node concept="3clFbS" id="bK" role="3Kbo56">
              <node concept="3clFbJ" id="bM" role="3cqZAp">
                <node concept="3clFbS" id="bO" role="3clFbx">
                  <node concept="3cpWs8" id="bQ" role="3cqZAp">
                    <node concept="3cpWsn" id="bS" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="bT" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="bU" role="33vP2m">
                        <node concept="1pGfFk" id="bV" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="bR" role="3cqZAp">
                    <node concept="37vLTI" id="bW" role="3clFbG">
                      <node concept="2OqwBi" id="bX" role="37vLTx">
                        <node concept="37vLTw" id="bZ" role="2Oq$k0">
                          <ref role="3cqZAo" node="bS" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="c0" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="bY" role="37vLTJ">
                        <ref role="3cqZAo" node="8i" resolve="props_ICondition" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="bP" role="3clFbw">
                  <node concept="10Nm6u" id="c1" role="3uHU7w" />
                  <node concept="37vLTw" id="c2" role="3uHU7B">
                    <ref role="3cqZAo" node="8i" resolve="props_ICondition" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="bN" role="3cqZAp">
                <node concept="37vLTw" id="c3" role="3cqZAk">
                  <ref role="3cqZAo" node="8i" resolve="props_ICondition" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="bL" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fv" resolve="ICondition" />
            </node>
          </node>
          <node concept="3KbdKl" id="9a" role="3KbHQx">
            <node concept="3clFbS" id="c4" role="3Kbo56">
              <node concept="3clFbJ" id="c6" role="3cqZAp">
                <node concept="3clFbS" id="c8" role="3clFbx">
                  <node concept="3cpWs8" id="ca" role="3cqZAp">
                    <node concept="3cpWsn" id="cd" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="ce" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="cf" role="33vP2m">
                        <node concept="1pGfFk" id="cg" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="cb" role="3cqZAp">
                    <node concept="2OqwBi" id="ch" role="3clFbG">
                      <node concept="37vLTw" id="ci" role="2Oq$k0">
                        <ref role="3cqZAo" node="cd" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="cj" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.rawPresentation(java.lang.String):jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="rawPresentation" />
                        <node concept="Xl_RD" id="ck" role="37wK5m">
                          <property role="Xl_RC" value="LogicOp" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="cc" role="3cqZAp">
                    <node concept="37vLTI" id="cl" role="3clFbG">
                      <node concept="2OqwBi" id="cm" role="37vLTx">
                        <node concept="37vLTw" id="co" role="2Oq$k0">
                          <ref role="3cqZAo" node="cd" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="cp" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="cn" role="37vLTJ">
                        <ref role="3cqZAo" node="8j" resolve="props_LogicOp" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="c9" role="3clFbw">
                  <node concept="10Nm6u" id="cq" role="3uHU7w" />
                  <node concept="37vLTw" id="cr" role="3uHU7B">
                    <ref role="3cqZAo" node="8j" resolve="props_LogicOp" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="c7" role="3cqZAp">
                <node concept="37vLTw" id="cs" role="3cqZAk">
                  <ref role="3cqZAo" node="8j" resolve="props_LogicOp" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="c5" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fw" resolve="LogicOp" />
            </node>
          </node>
          <node concept="3KbdKl" id="9b" role="3KbHQx">
            <node concept="3clFbS" id="ct" role="3Kbo56">
              <node concept="3clFbJ" id="cv" role="3cqZAp">
                <node concept="3clFbS" id="cx" role="3clFbx">
                  <node concept="3cpWs8" id="cz" role="3cqZAp">
                    <node concept="3cpWsn" id="cA" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="cB" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="cC" role="33vP2m">
                        <node concept="1pGfFk" id="cD" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="c$" role="3cqZAp">
                    <node concept="2OqwBi" id="cE" role="3clFbG">
                      <node concept="37vLTw" id="cF" role="2Oq$k0">
                        <ref role="3cqZAo" node="cA" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="cG" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByName():jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByName" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="c_" role="3cqZAp">
                    <node concept="37vLTI" id="cH" role="3clFbG">
                      <node concept="2OqwBi" id="cI" role="37vLTx">
                        <node concept="37vLTw" id="cK" role="2Oq$k0">
                          <ref role="3cqZAo" node="cA" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="cL" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="cJ" role="37vLTJ">
                        <ref role="3cqZAo" node="8k" resolve="props_Sensor" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="cy" role="3clFbw">
                  <node concept="10Nm6u" id="cM" role="3uHU7w" />
                  <node concept="37vLTw" id="cN" role="3uHU7B">
                    <ref role="3cqZAo" node="8k" resolve="props_Sensor" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="cw" role="3cqZAp">
                <node concept="37vLTw" id="cO" role="3cqZAk">
                  <ref role="3cqZAo" node="8k" resolve="props_Sensor" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="cu" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fx" resolve="Sensor" />
            </node>
          </node>
          <node concept="3KbdKl" id="9c" role="3KbHQx">
            <node concept="3clFbS" id="cP" role="3Kbo56">
              <node concept="3clFbJ" id="cR" role="3cqZAp">
                <node concept="3clFbS" id="cT" role="3clFbx">
                  <node concept="3cpWs8" id="cV" role="3cqZAp">
                    <node concept="3cpWsn" id="cY" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="cZ" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="d0" role="33vP2m">
                        <node concept="1pGfFk" id="d1" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="cW" role="3cqZAp">
                    <node concept="2OqwBi" id="d2" role="3clFbG">
                      <node concept="37vLTw" id="d3" role="2Oq$k0">
                        <ref role="3cqZAo" node="cY" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="d4" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByName():jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByName" />
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="cX" role="3cqZAp">
                    <node concept="37vLTI" id="d5" role="3clFbG">
                      <node concept="2OqwBi" id="d6" role="37vLTx">
                        <node concept="37vLTw" id="d8" role="2Oq$k0">
                          <ref role="3cqZAo" node="cY" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="d9" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="d7" role="37vLTJ">
                        <ref role="3cqZAo" node="8l" resolve="props_State" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="cU" role="3clFbw">
                  <node concept="10Nm6u" id="da" role="3uHU7w" />
                  <node concept="37vLTw" id="db" role="3uHU7B">
                    <ref role="3cqZAo" node="8l" resolve="props_State" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="cS" role="3cqZAp">
                <node concept="37vLTw" id="dc" role="3cqZAk">
                  <ref role="3cqZAo" node="8l" resolve="props_State" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="cQ" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fy" resolve="State" />
            </node>
          </node>
          <node concept="3KbdKl" id="9d" role="3KbHQx">
            <node concept="3clFbS" id="dd" role="3Kbo56">
              <node concept="3clFbJ" id="df" role="3cqZAp">
                <node concept="3clFbS" id="dh" role="3clFbx">
                  <node concept="3cpWs8" id="dj" role="3cqZAp">
                    <node concept="3cpWsn" id="dm" role="3cpWs9">
                      <property role="TrG5h" value="cpb" />
                      <node concept="3uibUv" id="dn" role="1tU5fm">
                        <ref role="3uigEE" to="ze1i:~ConceptPresentationBuilder" resolve="ConceptPresentationBuilder" />
                      </node>
                      <node concept="2ShNRf" id="do" role="33vP2m">
                        <node concept="1pGfFk" id="dp" role="2ShVmc">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.&lt;init&gt;()" resolve="ConceptPresentationBuilder" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="dk" role="3cqZAp">
                    <node concept="2OqwBi" id="dq" role="3clFbG">
                      <node concept="37vLTw" id="dr" role="2Oq$k0">
                        <ref role="3cqZAo" node="dm" resolve="cpb" />
                      </node>
                      <node concept="liA8E" id="ds" role="2OqNvi">
                        <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.presentationByReference(long,long,long,long,java.lang.String,java.lang.String,java.lang.String):jetbrains.mps.smodel.runtime.ConceptPresentationBuilder" resolve="presentationByReference" />
                        <node concept="1adDum" id="dt" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="du" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="dv" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a574322dL" />
                        </node>
                        <node concept="1adDum" id="dw" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a5743269L" />
                        </node>
                        <node concept="Xl_RD" id="dx" role="37wK5m">
                          <property role="Xl_RC" value="to" />
                        </node>
                        <node concept="Xl_RD" id="dy" role="37wK5m">
                          <property role="Xl_RC" value="" />
                        </node>
                        <node concept="Xl_RD" id="dz" role="37wK5m">
                          <property role="Xl_RC" value="" />
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="3clFbF" id="dl" role="3cqZAp">
                    <node concept="37vLTI" id="d$" role="3clFbG">
                      <node concept="2OqwBi" id="d_" role="37vLTx">
                        <node concept="37vLTw" id="dB" role="2Oq$k0">
                          <ref role="3cqZAo" node="dm" resolve="cpb" />
                        </node>
                        <node concept="liA8E" id="dC" role="2OqNvi">
                          <ref role="37wK5l" to="ze1i:~ConceptPresentationBuilder.create():jetbrains.mps.smodel.runtime.ConceptPresentation" resolve="create" />
                        </node>
                      </node>
                      <node concept="37vLTw" id="dA" role="37vLTJ">
                        <ref role="3cqZAo" node="8m" resolve="props_Transition" />
                      </node>
                    </node>
                  </node>
                </node>
                <node concept="3clFbC" id="di" role="3clFbw">
                  <node concept="10Nm6u" id="dD" role="3uHU7w" />
                  <node concept="37vLTw" id="dE" role="3uHU7B">
                    <ref role="3cqZAo" node="8m" resolve="props_Transition" />
                  </node>
                </node>
              </node>
              <node concept="3cpWs6" id="dg" role="3cqZAp">
                <node concept="37vLTw" id="dF" role="3cqZAk">
                  <ref role="3cqZAo" node="8m" resolve="props_Transition" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="de" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fz" resolve="Transition" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="8T" role="3cqZAp">
          <node concept="10Nm6u" id="dG" role="3cqZAk" />
        </node>
      </node>
      <node concept="3uibUv" id="8N" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptPresentation" resolve="ConceptPresentation" />
      </node>
      <node concept="2AHcQZ" id="8O" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="2AHcQZ" id="8P" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
      </node>
    </node>
  </node>
  <node concept="39dXUE" id="dH">
    <node concept="39e2AJ" id="dI" role="39e2AI">
      <property role="39e3Y2" value="javaClass" />
      <node concept="39e2AG" id="dN" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWEtNz" resolve="COMP" />
        <node concept="385nmt" id="dQ" role="385vvn">
          <property role="385vuF" value="COMP" />
          <node concept="2$VJBW" id="dS" role="385v07">
            <property role="2$VJBR" value="3199544644537539811" />
            <node concept="2x4n5u" id="dT" role="3iCydw">
              <property role="2x4mPI" value="EnumerationDataTypeDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtih8jjv" />
              <node concept="2V$Bhx" id="dU" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="dR" role="39e2AY">
          <ref role="39e2AS" node="0" resolve="COMP" />
        </node>
      </node>
      <node concept="39e2AG" id="dO" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWB9QV" resolve="COND" />
        <node concept="385nmt" id="dV" role="385vvn">
          <property role="385vuF" value="COND" />
          <node concept="2$VJBW" id="dX" role="385v07">
            <property role="2$VJBR" value="3199544644536671675" />
            <node concept="2x4n5u" id="dY" role="3iCydw">
              <property role="2x4mPI" value="EnumerationDataTypeDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtih8jjv" />
              <node concept="2V$Bhx" id="dZ" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="dW" role="39e2AY">
          <ref role="39e2AS" node="4D" resolve="COND" />
        </node>
      </node>
      <node concept="39e2AG" id="dP" role="39e3Y0">
        <ref role="39e2AK" to="m31e:6moOPi_sS_V" resolve="SIGNAL" />
        <node concept="385nmt" id="e0" role="385vvn">
          <property role="385vuF" value="SIGNAL" />
          <node concept="2$VJBW" id="e2" role="385v07">
            <property role="2$VJBR" value="7320833554797267323" />
            <node concept="2x4n5u" id="e3" role="3iCydw">
              <property role="2x4mPI" value="EnumerationDataTypeDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtih8jjv" />
              <node concept="2V$Bhx" id="e4" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="e1" role="39e2AY">
          <ref role="39e2AS" node="hN" resolve="SIGNAL" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="dJ" role="39e2AI">
      <property role="39e3Y2" value="outputEnumConstant" />
      <node concept="39e2AG" id="e5" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWB9QW" />
        <node concept="385nmt" id="ef" role="385vvn">
          <property role="385vuF" value="and" />
          <node concept="2$VJBW" id="eh" role="385v07">
            <property role="2$VJBR" value="3199544644536671676" />
            <node concept="2x4n5u" id="ei" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="ej" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="eg" role="39e2AY">
          <ref role="39e2AS" node="4E" resolve="and" />
        </node>
      </node>
      <node concept="39e2AG" id="e6" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWEtNL" />
        <node concept="385nmt" id="ek" role="385vvn">
          <property role="385vuF" value="eq" />
          <node concept="2$VJBW" id="em" role="385v07">
            <property role="2$VJBR" value="3199544644537539825" />
            <node concept="2x4n5u" id="en" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="eo" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="el" role="39e2AY">
          <ref role="39e2AS" node="5" resolve="eq" />
        </node>
      </node>
      <node concept="39e2AG" id="e7" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWEtNG" />
        <node concept="385nmt" id="ep" role="385vvn">
          <property role="385vuF" value="ge" />
          <node concept="2$VJBW" id="er" role="385v07">
            <property role="2$VJBR" value="3199544644537539820" />
            <node concept="2x4n5u" id="es" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="et" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="eq" role="39e2AY">
          <ref role="39e2AS" node="4" resolve="ge" />
        </node>
      </node>
      <node concept="39e2AG" id="e8" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWEtN_" />
        <node concept="385nmt" id="eu" role="385vvn">
          <property role="385vuF" value="gt" />
          <node concept="2$VJBW" id="ew" role="385v07">
            <property role="2$VJBR" value="3199544644537539813" />
            <node concept="2x4n5u" id="ex" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="ey" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="ev" role="39e2AY">
          <ref role="39e2AS" node="2" resolve="gt" />
        </node>
      </node>
      <node concept="39e2AG" id="e9" role="39e3Y0">
        <ref role="39e2AK" to="m31e:6moOPi_sS_W" />
        <node concept="385nmt" id="ez" role="385vvn">
          <property role="385vuF" value="high" />
          <node concept="2$VJBW" id="e_" role="385v07">
            <property role="2$VJBR" value="7320833554797267324" />
            <node concept="2x4n5u" id="eA" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="eB" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="e$" role="39e2AY">
          <ref role="39e2AS" node="hO" resolve="high" />
        </node>
      </node>
      <node concept="39e2AG" id="ea" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWEtNC" />
        <node concept="385nmt" id="eC" role="385vvn">
          <property role="385vuF" value="le" />
          <node concept="2$VJBW" id="eE" role="385v07">
            <property role="2$VJBR" value="3199544644537539816" />
            <node concept="2x4n5u" id="eF" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="eG" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="eD" role="39e2AY">
          <ref role="39e2AS" node="3" resolve="le" />
        </node>
      </node>
      <node concept="39e2AG" id="eb" role="39e3Y0">
        <ref role="39e2AK" to="m31e:6moOPi_sSAo" />
        <node concept="385nmt" id="eH" role="385vvn">
          <property role="385vuF" value="low" />
          <node concept="2$VJBW" id="eJ" role="385v07">
            <property role="2$VJBR" value="7320833554797267352" />
            <node concept="2x4n5u" id="eK" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="eL" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="eI" role="39e2AY">
          <ref role="39e2AS" node="hP" resolve="low" />
        </node>
      </node>
      <node concept="39e2AG" id="ec" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWEtN$" />
        <node concept="385nmt" id="eM" role="385vvn">
          <property role="385vuF" value="lt" />
          <node concept="2$VJBW" id="eO" role="385v07">
            <property role="2$VJBR" value="3199544644537539812" />
            <node concept="2x4n5u" id="eP" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="eQ" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="eN" role="39e2AY">
          <ref role="39e2AS" node="1" resolve="lt" />
        </node>
      </node>
      <node concept="39e2AG" id="ed" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWFiG0" />
        <node concept="385nmt" id="eR" role="385vvn">
          <property role="385vuF" value="ne" />
          <node concept="2$VJBW" id="eT" role="385v07">
            <property role="2$VJBR" value="3199544644537756416" />
            <node concept="2x4n5u" id="eU" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="eV" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="eS" role="39e2AY">
          <ref role="39e2AS" node="6" resolve="ne" />
        </node>
      </node>
      <node concept="39e2AG" id="ee" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWB9QX" />
        <node concept="385nmt" id="eW" role="385vvn">
          <property role="385vuF" value="or" />
          <node concept="2$VJBW" id="eY" role="385v07">
            <property role="2$VJBR" value="3199544644536671677" />
            <node concept="2x4n5u" id="eZ" role="3iCydw">
              <property role="2x4mPI" value="EnumerationMemberDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtlokhiq" />
              <node concept="2V$Bhx" id="f0" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="eX" role="39e2AY">
          <ref role="39e2AS" node="4F" resolve="or" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="dK" role="39e2AI">
      <property role="39e3Y2" value="propertySupportClass" />
      <node concept="39e2AG" id="f1" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWEtNz" resolve="COMP" />
        <node concept="385nmt" id="f4" role="385vvn">
          <property role="385vuF" value="COMP" />
          <node concept="2$VJBW" id="f6" role="385v07">
            <property role="2$VJBR" value="3199544644537539811" />
            <node concept="2x4n5u" id="f7" role="3iCydw">
              <property role="2x4mPI" value="EnumerationDataTypeDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtih8jjv" />
              <node concept="2V$Bhx" id="f8" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="f5" role="39e2AY">
          <ref role="39e2AS" node="2S" resolve="COMP_PropertySupport" />
        </node>
      </node>
      <node concept="39e2AG" id="f2" role="39e3Y0">
        <ref role="39e2AK" to="m31e:2LB4erWB9QV" resolve="COND" />
        <node concept="385nmt" id="f9" role="385vvn">
          <property role="385vuF" value="COND" />
          <node concept="2$VJBW" id="fb" role="385v07">
            <property role="2$VJBR" value="3199544644536671675" />
            <node concept="2x4n5u" id="fc" role="3iCydw">
              <property role="2x4mPI" value="EnumerationDataTypeDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtih8jjv" />
              <node concept="2V$Bhx" id="fd" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="fa" role="39e2AY">
          <ref role="39e2AS" node="6p" resolve="COND_PropertySupport" />
        </node>
      </node>
      <node concept="39e2AG" id="f3" role="39e3Y0">
        <ref role="39e2AK" to="m31e:6moOPi_sS_V" resolve="SIGNAL" />
        <node concept="385nmt" id="fe" role="385vvn">
          <property role="385vuF" value="SIGNAL" />
          <node concept="2$VJBW" id="fg" role="385v07">
            <property role="2$VJBR" value="7320833554797267323" />
            <node concept="2x4n5u" id="fh" role="3iCydw">
              <property role="2x4mPI" value="EnumerationDataTypeDeclaration" />
              <property role="2x4o5l" value="false" />
              <property role="2x4n5l" value="dtih8jjv" />
              <node concept="2V$Bhx" id="fi" role="2x4n5j">
                <property role="2V$B1T" value="c72da2b9-7cce-4447-8389-f407dc1158b7" />
                <property role="2V$B1Q" value="jetbrains.mps.lang.structure" />
              </node>
            </node>
          </node>
        </node>
        <node concept="39e2AT" id="ff" role="39e2AY">
          <ref role="39e2AS" node="jz" resolve="SIGNAL_PropertySupport" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="dL" role="39e2AI">
      <property role="39e3Y2" value="ConceptPresentationAspectClass" />
      <node concept="39e2AG" id="fj" role="39e3Y0">
        <property role="2mV_xN" value="true" />
        <node concept="39e2AT" id="fk" role="39e2AY">
          <ref role="39e2AS" node="8a" resolve="ConceptPresentationAspectImpl" />
        </node>
      </node>
    </node>
    <node concept="39e2AJ" id="dM" role="39e2AI">
      <property role="39e3Y2" value="StructureAspectDescriptorCons" />
      <node concept="39e2AG" id="fl" role="39e3Y0">
        <property role="2mV_xN" value="true" />
        <node concept="39e2AT" id="fm" role="39e2AY">
          <ref role="39e2AS" node="l$" resolve="StructureAspectDescriptor" />
        </node>
      </node>
    </node>
  </node>
  <node concept="312cEu" id="fn">
    <property role="TrG5h" value="LanguageConceptSwitch" />
    <property role="1EXbeo" value="true" />
    <node concept="312cEg" id="fo" role="jymVt">
      <property role="34CwA1" value="false" />
      <property role="eg7rD" value="false" />
      <property role="TrG5h" value="myIndex" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm6S6" id="fF" role="1B3o_S" />
      <node concept="3uibUv" id="fG" role="1tU5fm">
        <ref role="3uigEE" to="ksn4:~LanguageConceptIndex" resolve="LanguageConceptIndex" />
      </node>
    </node>
    <node concept="Wx3nA" id="fp" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Action" />
      <node concept="3Tm1VV" id="fH" role="1B3o_S" />
      <node concept="10Oyi0" id="fI" role="1tU5fm" />
      <node concept="3cmrfG" id="fJ" role="33vP2m">
        <property role="3cmrfH" value="0" />
      </node>
    </node>
    <node concept="Wx3nA" id="fq" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Actuator" />
      <node concept="3Tm1VV" id="fK" role="1B3o_S" />
      <node concept="10Oyi0" id="fL" role="1tU5fm" />
      <node concept="3cmrfG" id="fM" role="33vP2m">
        <property role="3cmrfH" value="1" />
      </node>
    </node>
    <node concept="Wx3nA" id="fr" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="App" />
      <node concept="3Tm1VV" id="fN" role="1B3o_S" />
      <node concept="10Oyi0" id="fO" role="1tU5fm" />
      <node concept="3cmrfG" id="fP" role="33vP2m">
        <property role="3cmrfH" value="2" />
      </node>
    </node>
    <node concept="Wx3nA" id="fs" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Brick" />
      <node concept="3Tm1VV" id="fQ" role="1B3o_S" />
      <node concept="10Oyi0" id="fR" role="1tU5fm" />
      <node concept="3cmrfG" id="fS" role="33vP2m">
        <property role="3cmrfH" value="3" />
      </node>
    </node>
    <node concept="Wx3nA" id="ft" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Condition" />
      <node concept="3Tm1VV" id="fT" role="1B3o_S" />
      <node concept="10Oyi0" id="fU" role="1tU5fm" />
      <node concept="3cmrfG" id="fV" role="33vP2m">
        <property role="3cmrfH" value="4" />
      </node>
    </node>
    <node concept="Wx3nA" id="fu" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="ConditionAnalogic" />
      <node concept="3Tm1VV" id="fW" role="1B3o_S" />
      <node concept="10Oyi0" id="fX" role="1tU5fm" />
      <node concept="3cmrfG" id="fY" role="33vP2m">
        <property role="3cmrfH" value="5" />
      </node>
    </node>
    <node concept="Wx3nA" id="fv" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="ICondition" />
      <node concept="3Tm1VV" id="fZ" role="1B3o_S" />
      <node concept="10Oyi0" id="g0" role="1tU5fm" />
      <node concept="3cmrfG" id="g1" role="33vP2m">
        <property role="3cmrfH" value="6" />
      </node>
    </node>
    <node concept="Wx3nA" id="fw" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="LogicOp" />
      <node concept="3Tm1VV" id="g2" role="1B3o_S" />
      <node concept="10Oyi0" id="g3" role="1tU5fm" />
      <node concept="3cmrfG" id="g4" role="33vP2m">
        <property role="3cmrfH" value="7" />
      </node>
    </node>
    <node concept="Wx3nA" id="fx" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Sensor" />
      <node concept="3Tm1VV" id="g5" role="1B3o_S" />
      <node concept="10Oyi0" id="g6" role="1tU5fm" />
      <node concept="3cmrfG" id="g7" role="33vP2m">
        <property role="3cmrfH" value="8" />
      </node>
    </node>
    <node concept="Wx3nA" id="fy" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="State" />
      <node concept="3Tm1VV" id="g8" role="1B3o_S" />
      <node concept="10Oyi0" id="g9" role="1tU5fm" />
      <node concept="3cmrfG" id="ga" role="33vP2m">
        <property role="3cmrfH" value="9" />
      </node>
    </node>
    <node concept="Wx3nA" id="fz" role="jymVt">
      <property role="2dlcS1" value="false" />
      <property role="2dld4O" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="Transition" />
      <node concept="3Tm1VV" id="gb" role="1B3o_S" />
      <node concept="10Oyi0" id="gc" role="1tU5fm" />
      <node concept="3cmrfG" id="gd" role="33vP2m">
        <property role="3cmrfH" value="10" />
      </node>
    </node>
    <node concept="2tJIrI" id="f$" role="jymVt" />
    <node concept="3clFbW" id="f_" role="jymVt">
      <node concept="3cqZAl" id="ge" role="3clF45" />
      <node concept="3Tm1VV" id="gf" role="1B3o_S" />
      <node concept="3clFbS" id="gg" role="3clF47">
        <node concept="3cpWs8" id="gh" role="3cqZAp">
          <node concept="3cpWsn" id="gu" role="3cpWs9">
            <property role="TrG5h" value="builder" />
            <node concept="3uibUv" id="gv" role="1tU5fm">
              <ref role="3uigEE" to="ksn4:~LanguageConceptIndexBuilder" resolve="LanguageConceptIndexBuilder" />
            </node>
            <node concept="2ShNRf" id="gw" role="33vP2m">
              <node concept="1pGfFk" id="gx" role="2ShVmc">
                <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.&lt;init&gt;(long,long)" resolve="LanguageConceptIndexBuilder" />
                <node concept="1adDum" id="gy" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="gz" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gi" role="3cqZAp">
          <node concept="2OqwBi" id="g$" role="3clFbG">
            <node concept="37vLTw" id="g_" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="gA" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="gB" role="37wK5m">
                <property role="1adDun" value="0x6598d354a57389a0L" />
              </node>
              <node concept="37vLTw" id="gC" role="37wK5m">
                <ref role="3cqZAo" node="fp" resolve="Action" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gj" role="3cqZAp">
          <node concept="2OqwBi" id="gD" role="3clFbG">
            <node concept="37vLTw" id="gE" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="gF" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="gG" role="37wK5m">
                <property role="1adDun" value="0x6598d354a573893dL" />
              </node>
              <node concept="37vLTw" id="gH" role="37wK5m">
                <ref role="3cqZAo" node="fq" resolve="Actuator" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gk" role="3cqZAp">
          <node concept="2OqwBi" id="gI" role="3clFbG">
            <node concept="37vLTw" id="gJ" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="gK" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="gL" role="37wK5m">
                <property role="1adDun" value="0x6598d354a573891eL" />
              </node>
              <node concept="37vLTw" id="gM" role="37wK5m">
                <ref role="3cqZAo" node="fr" resolve="App" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gl" role="3cqZAp">
          <node concept="2OqwBi" id="gN" role="3clFbG">
            <node concept="37vLTw" id="gO" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="gP" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="gQ" role="37wK5m">
                <property role="1adDun" value="0x6598d354a57431f2L" />
              </node>
              <node concept="37vLTw" id="gR" role="37wK5m">
                <ref role="3cqZAo" node="fs" resolve="Brick" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gm" role="3cqZAp">
          <node concept="2OqwBi" id="gS" role="3clFbG">
            <node concept="37vLTw" id="gT" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="gU" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="gV" role="37wK5m">
                <property role="1adDun" value="0x6598d354a574326fL" />
              </node>
              <node concept="37vLTw" id="gW" role="37wK5m">
                <ref role="3cqZAo" node="ft" resolve="Condition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gn" role="3cqZAp">
          <node concept="2OqwBi" id="gX" role="3clFbG">
            <node concept="37vLTw" id="gY" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="gZ" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="h0" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fcb44fe0L" />
              </node>
              <node concept="37vLTw" id="h1" role="37wK5m">
                <ref role="3cqZAo" node="fu" resolve="ConditionAnalogic" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="go" role="3cqZAp">
          <node concept="2OqwBi" id="h2" role="3clFbG">
            <node concept="37vLTw" id="h3" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="h4" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="h5" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fcb3e16fL" />
              </node>
              <node concept="37vLTw" id="h6" role="37wK5m">
                <ref role="3cqZAo" node="fv" resolve="ICondition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gp" role="3cqZAp">
          <node concept="2OqwBi" id="h7" role="3clFbG">
            <node concept="37vLTw" id="h8" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="h9" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="ha" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fc99ed09L" />
              </node>
              <node concept="37vLTw" id="hb" role="37wK5m">
                <ref role="3cqZAo" node="fw" resolve="LogicOp" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gq" role="3cqZAp">
          <node concept="2OqwBi" id="hc" role="3clFbG">
            <node concept="37vLTw" id="hd" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="he" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="hf" role="37wK5m">
                <property role="1adDun" value="0x6598d354a57431b3L" />
              </node>
              <node concept="37vLTw" id="hg" role="37wK5m">
                <ref role="3cqZAo" node="fx" resolve="Sensor" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gr" role="3cqZAp">
          <node concept="2OqwBi" id="hh" role="3clFbG">
            <node concept="37vLTw" id="hi" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="hj" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="hk" role="37wK5m">
                <property role="1adDun" value="0x6598d354a573895cL" />
              </node>
              <node concept="37vLTw" id="hl" role="37wK5m">
                <ref role="3cqZAo" node="fy" resolve="State" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gs" role="3cqZAp">
          <node concept="2OqwBi" id="hm" role="3clFbG">
            <node concept="37vLTw" id="hn" role="2Oq$k0">
              <ref role="3cqZAo" node="gu" resolve="builder" />
            </node>
            <node concept="liA8E" id="ho" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.put(long,int):jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder" resolve="put" />
              <node concept="1adDum" id="hp" role="37wK5m">
                <property role="1adDun" value="0x6598d354a574322dL" />
              </node>
              <node concept="37vLTw" id="hq" role="37wK5m">
                <ref role="3cqZAo" node="fz" resolve="Transition" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="gt" role="3cqZAp">
          <node concept="37vLTI" id="hr" role="3clFbG">
            <node concept="2OqwBi" id="hs" role="37vLTx">
              <node concept="37vLTw" id="hu" role="2Oq$k0">
                <ref role="3cqZAo" node="gu" resolve="builder" />
              </node>
              <node concept="liA8E" id="hv" role="2OqNvi">
                <ref role="37wK5l" to="ksn4:~LanguageConceptIndexBuilder.seal():jetbrains.mps.lang.smodel.LanguageConceptIndex" resolve="seal" />
              </node>
            </node>
            <node concept="37vLTw" id="ht" role="37vLTJ">
              <ref role="3cqZAo" node="fo" resolve="myIndex" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="fA" role="jymVt" />
    <node concept="3clFb_" id="fB" role="jymVt">
      <property role="TrG5h" value="index" />
      <node concept="10Oyi0" id="hw" role="3clF45" />
      <node concept="3clFbS" id="hx" role="3clF47">
        <node concept="3cpWs6" id="hz" role="3cqZAp">
          <node concept="2OqwBi" id="h$" role="3cqZAk">
            <node concept="37vLTw" id="h_" role="2Oq$k0">
              <ref role="3cqZAo" node="fo" resolve="myIndex" />
            </node>
            <node concept="liA8E" id="hA" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~LanguageConceptIndex.index(jetbrains.mps.smodel.adapter.ids.SConceptId):int" resolve="index" />
              <node concept="37vLTw" id="hB" role="37wK5m">
                <ref role="3cqZAo" node="hy" resolve="cid" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="hy" role="3clF46">
        <property role="TrG5h" value="cid" />
        <node concept="3uibUv" id="hC" role="1tU5fm">
          <ref role="3uigEE" to="e8bb:~SConceptId" resolve="SConceptId" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="fC" role="jymVt" />
    <node concept="3clFb_" id="fD" role="jymVt">
      <property role="TrG5h" value="index" />
      <node concept="10Oyi0" id="hD" role="3clF45" />
      <node concept="3Tm1VV" id="hE" role="1B3o_S" />
      <node concept="3clFbS" id="hF" role="3clF47">
        <node concept="3cpWs6" id="hH" role="3cqZAp">
          <node concept="2OqwBi" id="hI" role="3cqZAk">
            <node concept="37vLTw" id="hJ" role="2Oq$k0">
              <ref role="3cqZAo" node="fo" resolve="myIndex" />
            </node>
            <node concept="liA8E" id="hK" role="2OqNvi">
              <ref role="37wK5l" to="ksn4:~ConceptIndex.index(org.jetbrains.mps.openapi.language.SAbstractConcept):int" resolve="index" />
              <node concept="37vLTw" id="hL" role="37wK5m">
                <ref role="3cqZAo" node="hG" resolve="concept" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="hG" role="3clF46">
        <property role="TrG5h" value="concept" />
        <node concept="3uibUv" id="hM" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
        </node>
      </node>
    </node>
    <node concept="3Tm1VV" id="fE" role="1B3o_S" />
  </node>
  <node concept="Qs71p" id="hN">
    <property role="TrG5h" value="SIGNAL" />
    <node concept="QsSxf" id="hO" role="Qtgdg">
      <property role="TrG5h" value="high" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="i0" role="37wK5m">
        <property role="Xl_RC" value="high" />
      </node>
      <node concept="Xl_RD" id="i1" role="37wK5m">
        <property role="Xl_RC" value="HIGH" />
      </node>
    </node>
    <node concept="QsSxf" id="hP" role="Qtgdg">
      <property role="TrG5h" value="low" />
      <ref role="37wK5l" to="wyt6:~Object.&lt;init&gt;()" resolve="Object" />
      <node concept="Xl_RD" id="i2" role="37wK5m">
        <property role="Xl_RC" value="low" />
      </node>
      <node concept="Xl_RD" id="i3" role="37wK5m">
        <property role="Xl_RC" value="LOW" />
      </node>
    </node>
    <node concept="3Tm1VV" id="hQ" role="1B3o_S" />
    <node concept="312cEg" id="hR" role="jymVt">
      <property role="TrG5h" value="myName" />
      <property role="3TUv4t" value="true" />
      <node concept="17QB3L" id="i4" role="1tU5fm" />
      <node concept="3Tm6S6" id="i5" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="hS" role="jymVt">
      <property role="TrG5h" value="getName" />
      <node concept="3clFbS" id="i6" role="3clF47">
        <node concept="3cpWs6" id="i9" role="3cqZAp">
          <node concept="37vLTw" id="ia" role="3cqZAk">
            <ref role="3cqZAo" node="hR" resolve="myName" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="i7" role="3clF45" />
      <node concept="3Tm1VV" id="i8" role="1B3o_S" />
    </node>
    <node concept="312cEg" id="hT" role="jymVt">
      <property role="TrG5h" value="myValue" />
      <property role="3TUv4t" value="true" />
      <node concept="17QB3L" id="ib" role="1tU5fm" />
      <node concept="3Tm6S6" id="ic" role="1B3o_S" />
    </node>
    <node concept="3clFbW" id="hU" role="jymVt">
      <node concept="3clFbS" id="id" role="3clF47">
        <node concept="3clFbF" id="ih" role="3cqZAp">
          <node concept="37vLTI" id="ij" role="3clFbG">
            <node concept="37vLTw" id="ik" role="37vLTJ">
              <ref role="3cqZAo" node="hR" resolve="myName" />
            </node>
            <node concept="37vLTw" id="il" role="37vLTx">
              <ref role="3cqZAo" node="ie" resolve="name" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ii" role="3cqZAp">
          <node concept="37vLTI" id="im" role="3clFbG">
            <node concept="37vLTw" id="in" role="37vLTJ">
              <ref role="3cqZAo" node="hT" resolve="myValue" />
            </node>
            <node concept="37vLTw" id="io" role="37vLTx">
              <ref role="3cqZAo" node="if" resolve="value" />
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="ie" role="3clF46">
        <property role="TrG5h" value="name" />
        <node concept="17QB3L" id="ip" role="1tU5fm" />
      </node>
      <node concept="37vLTG" id="if" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="iq" role="1tU5fm" />
      </node>
      <node concept="3Tm6S6" id="ig" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="hV" role="jymVt">
      <property role="TrG5h" value="getValue" />
      <node concept="3clFbS" id="ir" role="3clF47">
        <node concept="3cpWs6" id="iu" role="3cqZAp">
          <node concept="37vLTw" id="iv" role="3cqZAk">
            <ref role="3cqZAo" node="hT" resolve="myValue" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="is" role="3clF45" />
      <node concept="3Tm1VV" id="it" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="hW" role="jymVt">
      <property role="TrG5h" value="getValueAsString" />
      <node concept="3clFbS" id="iw" role="3clF47">
        <node concept="3cpWs6" id="iz" role="3cqZAp">
          <node concept="37vLTw" id="i$" role="3cqZAk">
            <ref role="3cqZAo" node="hT" resolve="myValue" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="ix" role="3clF45" />
      <node concept="3Tm1VV" id="iy" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="hX" role="jymVt">
      <property role="TrG5h" value="getConstants" />
      <node concept="3clFbS" id="i_" role="3clF47">
        <node concept="3cpWs8" id="iC" role="3cqZAp">
          <node concept="3cpWsn" id="iG" role="3cpWs9">
            <property role="TrG5h" value="list" />
            <node concept="_YKpA" id="iH" role="1tU5fm">
              <node concept="3uibUv" id="iJ" role="_ZDj9">
                <ref role="3uigEE" node="hN" resolve="SIGNAL" />
              </node>
            </node>
            <node concept="2ShNRf" id="iI" role="33vP2m">
              <node concept="2Jqq0_" id="iK" role="2ShVmc">
                <node concept="3uibUv" id="iL" role="HW$YZ">
                  <ref role="3uigEE" node="hN" resolve="SIGNAL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="iD" role="3cqZAp">
          <node concept="2OqwBi" id="iM" role="3clFbG">
            <node concept="37vLTw" id="iN" role="2Oq$k0">
              <ref role="3cqZAo" node="iG" resolve="list" />
            </node>
            <node concept="TSZUe" id="iO" role="2OqNvi">
              <node concept="Rm8GO" id="iP" role="25WWJ7">
                <ref role="Rm8GQ" node="hO" resolve="high" />
                <ref role="1Px2BO" node="hN" resolve="SIGNAL" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="iE" role="3cqZAp">
          <node concept="2OqwBi" id="iQ" role="3clFbG">
            <node concept="37vLTw" id="iR" role="2Oq$k0">
              <ref role="3cqZAo" node="iG" resolve="list" />
            </node>
            <node concept="TSZUe" id="iS" role="2OqNvi">
              <node concept="Rm8GO" id="iT" role="25WWJ7">
                <ref role="Rm8GQ" node="hP" resolve="low" />
                <ref role="1Px2BO" node="hN" resolve="SIGNAL" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="iF" role="3cqZAp">
          <node concept="37vLTw" id="iU" role="3cqZAk">
            <ref role="3cqZAo" node="iG" resolve="list" />
          </node>
        </node>
      </node>
      <node concept="_YKpA" id="iA" role="3clF45">
        <node concept="3uibUv" id="iV" role="_ZDj9">
          <ref role="3uigEE" node="hN" resolve="SIGNAL" />
        </node>
      </node>
      <node concept="3Tm1VV" id="iB" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="hY" role="jymVt">
      <property role="TrG5h" value="getDefault" />
      <node concept="3clFbS" id="iW" role="3clF47">
        <node concept="3cpWs6" id="iZ" role="3cqZAp">
          <node concept="10Nm6u" id="j0" role="3cqZAk" />
        </node>
      </node>
      <node concept="3uibUv" id="iX" role="3clF45">
        <ref role="3uigEE" node="hN" resolve="SIGNAL" />
      </node>
      <node concept="3Tm1VV" id="iY" role="1B3o_S" />
    </node>
    <node concept="2YIFZL" id="hZ" role="jymVt">
      <property role="TrG5h" value="parseValue" />
      <node concept="3clFbS" id="j1" role="3clF47">
        <node concept="3clFbJ" id="j5" role="3cqZAp">
          <node concept="3clFbS" id="j9" role="3clFbx">
            <node concept="3cpWs6" id="jb" role="3cqZAp">
              <node concept="2YIFZM" id="jc" role="3cqZAk">
                <ref role="37wK5l" node="hY" resolve="getDefault" />
                <ref role="1Pybhc" node="hN" resolve="SIGNAL" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="ja" role="3clFbw">
            <node concept="10Nm6u" id="jd" role="3uHU7w" />
            <node concept="37vLTw" id="je" role="3uHU7B">
              <ref role="3cqZAo" node="j3" resolve="value" />
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="j6" role="3cqZAp">
          <node concept="3clFbS" id="jf" role="3clFbx">
            <node concept="3cpWs6" id="jh" role="3cqZAp">
              <node concept="Rm8GO" id="ji" role="3cqZAk">
                <ref role="Rm8GQ" node="hO" resolve="high" />
                <ref role="1Px2BO" node="hN" resolve="SIGNAL" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="jg" role="3clFbw">
            <node concept="37vLTw" id="jj" role="2Oq$k0">
              <ref role="3cqZAo" node="j3" resolve="value" />
            </node>
            <node concept="liA8E" id="jk" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="jl" role="37wK5m">
                <node concept="Rm8GO" id="jm" role="2Oq$k0">
                  <ref role="Rm8GQ" node="hO" resolve="high" />
                  <ref role="1Px2BO" node="hN" resolve="SIGNAL" />
                </node>
                <node concept="liA8E" id="jn" role="2OqNvi">
                  <ref role="37wK5l" node="hW" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="j7" role="3cqZAp">
          <node concept="3clFbS" id="jo" role="3clFbx">
            <node concept="3cpWs6" id="jq" role="3cqZAp">
              <node concept="Rm8GO" id="jr" role="3cqZAk">
                <ref role="Rm8GQ" node="hP" resolve="low" />
                <ref role="1Px2BO" node="hN" resolve="SIGNAL" />
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="jp" role="3clFbw">
            <node concept="37vLTw" id="js" role="2Oq$k0">
              <ref role="3cqZAo" node="j3" resolve="value" />
            </node>
            <node concept="liA8E" id="jt" role="2OqNvi">
              <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
              <node concept="2OqwBi" id="ju" role="37wK5m">
                <node concept="Rm8GO" id="jv" role="2Oq$k0">
                  <ref role="Rm8GQ" node="hP" resolve="low" />
                  <ref role="1Px2BO" node="hN" resolve="SIGNAL" />
                </node>
                <node concept="liA8E" id="jw" role="2OqNvi">
                  <ref role="37wK5l" node="hW" resolve="getValueAsString" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="j8" role="3cqZAp">
          <node concept="2YIFZM" id="jx" role="3cqZAk">
            <ref role="37wK5l" node="hY" resolve="getDefault" />
            <ref role="1Pybhc" node="hN" resolve="SIGNAL" />
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="j2" role="3clF45">
        <ref role="3uigEE" node="hN" resolve="SIGNAL" />
      </node>
      <node concept="37vLTG" id="j3" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="jy" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="j4" role="1B3o_S" />
    </node>
  </node>
  <node concept="312cEu" id="jz">
    <property role="TrG5h" value="SIGNAL_PropertySupport" />
    <node concept="3uibUv" id="j$" role="1zkMxy">
      <ref role="3uigEE" to="w1kc:~PropertySupport" resolve="PropertySupport" />
    </node>
    <node concept="3Tm1VV" id="j_" role="1B3o_S" />
    <node concept="3clFb_" id="jA" role="jymVt">
      <property role="TrG5h" value="canSetValue" />
      <node concept="3clFbS" id="jD" role="3clF47">
        <node concept="3clFbJ" id="jH" role="3cqZAp">
          <node concept="3clFbS" id="jL" role="3clFbx">
            <node concept="3cpWs6" id="jN" role="3cqZAp">
              <node concept="3clFbT" id="jO" role="3cqZAk">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
          <node concept="3clFbC" id="jM" role="3clFbw">
            <node concept="37vLTw" id="jP" role="3uHU7B">
              <ref role="3cqZAo" node="jF" resolve="value" />
            </node>
            <node concept="10Nm6u" id="jQ" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs8" id="jI" role="3cqZAp">
          <node concept="3cpWsn" id="jR" role="3cpWs9">
            <property role="TrG5h" value="constants" />
            <node concept="uOF1S" id="jS" role="1tU5fm">
              <node concept="3uibUv" id="jU" role="uOL27">
                <ref role="3uigEE" node="hN" resolve="SIGNAL" />
              </node>
            </node>
            <node concept="2OqwBi" id="jT" role="33vP2m">
              <node concept="2YIFZM" id="jV" role="2Oq$k0">
                <ref role="37wK5l" node="hX" resolve="getConstants" />
                <ref role="1Pybhc" node="hN" resolve="SIGNAL" />
              </node>
              <node concept="uNJiE" id="jW" role="2OqNvi" />
            </node>
          </node>
        </node>
        <node concept="2$JKZl" id="jJ" role="3cqZAp">
          <node concept="3clFbS" id="jX" role="2LFqv$">
            <node concept="3cpWs8" id="jZ" role="3cqZAp">
              <node concept="3cpWsn" id="k1" role="3cpWs9">
                <property role="TrG5h" value="constant" />
                <node concept="3uibUv" id="k2" role="1tU5fm">
                  <ref role="3uigEE" node="hN" resolve="SIGNAL" />
                </node>
                <node concept="2OqwBi" id="k3" role="33vP2m">
                  <node concept="37vLTw" id="k4" role="2Oq$k0">
                    <ref role="3cqZAo" node="jR" resolve="constants" />
                  </node>
                  <node concept="v1n4t" id="k5" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="k0" role="3cqZAp">
              <node concept="3clFbS" id="k6" role="3clFbx">
                <node concept="3cpWs6" id="k8" role="3cqZAp">
                  <node concept="3clFbT" id="k9" role="3cqZAk">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="k7" role="3clFbw">
                <node concept="37vLTw" id="ka" role="2Oq$k0">
                  <ref role="3cqZAo" node="jF" resolve="value" />
                </node>
                <node concept="liA8E" id="kb" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                  <node concept="2OqwBi" id="kc" role="37wK5m">
                    <node concept="37vLTw" id="kd" role="2Oq$k0">
                      <ref role="3cqZAo" node="k1" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="ke" role="2OqNvi">
                      <ref role="37wK5l" node="hS" resolve="getName" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="jY" role="2$JKZa">
            <node concept="37vLTw" id="kf" role="2Oq$k0">
              <ref role="3cqZAo" node="jR" resolve="constants" />
            </node>
            <node concept="v0PNk" id="kg" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="jK" role="3cqZAp">
          <node concept="3clFbT" id="kh" role="3cqZAk">
            <property role="3clFbU" value="false" />
          </node>
        </node>
      </node>
      <node concept="10P_77" id="jE" role="3clF45" />
      <node concept="37vLTG" id="jF" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="ki" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="jG" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="jB" role="jymVt">
      <property role="TrG5h" value="toInternalValue" />
      <node concept="3clFbS" id="kj" role="3clF47">
        <node concept="3clFbJ" id="kn" role="3cqZAp">
          <node concept="3clFbS" id="kr" role="3clFbx">
            <node concept="3cpWs6" id="kt" role="3cqZAp">
              <node concept="10Nm6u" id="ku" role="3cqZAk" />
            </node>
          </node>
          <node concept="3clFbC" id="ks" role="3clFbw">
            <node concept="37vLTw" id="kv" role="3uHU7B">
              <ref role="3cqZAo" node="kl" resolve="value" />
            </node>
            <node concept="10Nm6u" id="kw" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs8" id="ko" role="3cqZAp">
          <node concept="3cpWsn" id="kx" role="3cpWs9">
            <property role="TrG5h" value="constants" />
            <node concept="2OqwBi" id="ky" role="33vP2m">
              <node concept="2YIFZM" id="k$" role="2Oq$k0">
                <ref role="37wK5l" node="hX" resolve="getConstants" />
                <ref role="1Pybhc" node="hN" resolve="SIGNAL" />
              </node>
              <node concept="uNJiE" id="k_" role="2OqNvi" />
            </node>
            <node concept="uOF1S" id="kz" role="1tU5fm">
              <node concept="3uibUv" id="kA" role="uOL27">
                <ref role="3uigEE" node="hN" resolve="SIGNAL" />
              </node>
            </node>
          </node>
        </node>
        <node concept="2$JKZl" id="kp" role="3cqZAp">
          <node concept="3clFbS" id="kB" role="2LFqv$">
            <node concept="3cpWs8" id="kD" role="3cqZAp">
              <node concept="3cpWsn" id="kF" role="3cpWs9">
                <property role="TrG5h" value="constant" />
                <node concept="3uibUv" id="kG" role="1tU5fm">
                  <ref role="3uigEE" node="hN" resolve="SIGNAL" />
                </node>
                <node concept="2OqwBi" id="kH" role="33vP2m">
                  <node concept="37vLTw" id="kI" role="2Oq$k0">
                    <ref role="3cqZAo" node="kx" resolve="constants" />
                  </node>
                  <node concept="v1n4t" id="kJ" role="2OqNvi" />
                </node>
              </node>
            </node>
            <node concept="3clFbJ" id="kE" role="3cqZAp">
              <node concept="3clFbS" id="kK" role="3clFbx">
                <node concept="3cpWs6" id="kM" role="3cqZAp">
                  <node concept="2OqwBi" id="kN" role="3cqZAk">
                    <node concept="37vLTw" id="kO" role="2Oq$k0">
                      <ref role="3cqZAo" node="kF" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="kP" role="2OqNvi">
                      <ref role="37wK5l" node="hW" resolve="getValueAsString" />
                    </node>
                  </node>
                </node>
              </node>
              <node concept="2OqwBi" id="kL" role="3clFbw">
                <node concept="37vLTw" id="kQ" role="2Oq$k0">
                  <ref role="3cqZAo" node="kl" resolve="value" />
                </node>
                <node concept="liA8E" id="kR" role="2OqNvi">
                  <ref role="37wK5l" to="wyt6:~String.equals(java.lang.Object):boolean" resolve="equals" />
                  <node concept="2OqwBi" id="kS" role="37wK5m">
                    <node concept="37vLTw" id="kT" role="2Oq$k0">
                      <ref role="3cqZAo" node="kF" resolve="constant" />
                    </node>
                    <node concept="liA8E" id="kU" role="2OqNvi">
                      <ref role="37wK5l" node="hS" resolve="getName" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="kC" role="2$JKZa">
            <node concept="37vLTw" id="kV" role="2Oq$k0">
              <ref role="3cqZAo" node="kx" resolve="constants" />
            </node>
            <node concept="v0PNk" id="kW" role="2OqNvi" />
          </node>
        </node>
        <node concept="3cpWs6" id="kq" role="3cqZAp">
          <node concept="10Nm6u" id="kX" role="3cqZAk" />
        </node>
      </node>
      <node concept="17QB3L" id="kk" role="3clF45" />
      <node concept="37vLTG" id="kl" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="kY" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="km" role="1B3o_S" />
    </node>
    <node concept="3clFb_" id="jC" role="jymVt">
      <property role="TrG5h" value="fromInternalValue" />
      <node concept="3clFbS" id="kZ" role="3clF47">
        <node concept="3cpWs8" id="l3" role="3cqZAp">
          <node concept="3cpWsn" id="l6" role="3cpWs9">
            <property role="TrG5h" value="constant" />
            <node concept="3uibUv" id="l7" role="1tU5fm">
              <ref role="3uigEE" node="hN" resolve="SIGNAL" />
            </node>
            <node concept="2YIFZM" id="l8" role="33vP2m">
              <ref role="37wK5l" node="hZ" resolve="parseValue" />
              <ref role="1Pybhc" node="hN" resolve="SIGNAL" />
              <node concept="37vLTw" id="l9" role="37wK5m">
                <ref role="3cqZAo" node="l1" resolve="value" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="l4" role="3cqZAp">
          <node concept="3clFbS" id="la" role="3clFbx">
            <node concept="3cpWs6" id="lc" role="3cqZAp">
              <node concept="2OqwBi" id="ld" role="3cqZAk">
                <node concept="37vLTw" id="le" role="2Oq$k0">
                  <ref role="3cqZAo" node="l6" resolve="constant" />
                </node>
                <node concept="liA8E" id="lf" role="2OqNvi">
                  <ref role="37wK5l" node="hS" resolve="getName" />
                </node>
              </node>
            </node>
          </node>
          <node concept="3y3z36" id="lb" role="3clFbw">
            <node concept="37vLTw" id="lg" role="3uHU7B">
              <ref role="3cqZAo" node="l6" resolve="constant" />
            </node>
            <node concept="10Nm6u" id="lh" role="3uHU7w" />
          </node>
        </node>
        <node concept="3cpWs6" id="l5" role="3cqZAp">
          <node concept="Xl_RD" id="li" role="3cqZAk">
            <property role="Xl_RC" value="" />
          </node>
        </node>
      </node>
      <node concept="17QB3L" id="l0" role="3clF45" />
      <node concept="37vLTG" id="l1" role="3clF46">
        <property role="TrG5h" value="value" />
        <node concept="17QB3L" id="lj" role="1tU5fm" />
      </node>
      <node concept="3Tm1VV" id="l2" role="1B3o_S" />
    </node>
  </node>
  <node concept="312cEu" id="lk">
    <property role="TrG5h" value="StructureAspectDescriptor" />
    <node concept="3uibUv" id="ll" role="1zkMxy">
      <ref role="3uigEE" to="ze1i:~BaseStructureAspectDescriptor" resolve="BaseStructureAspectDescriptor" />
    </node>
    <node concept="312cEg" id="lm" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptAction" />
      <node concept="3uibUv" id="lR" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="lS" role="33vP2m">
        <ref role="37wK5l" node="lG" resolve="createDescriptorForAction" />
      </node>
    </node>
    <node concept="312cEg" id="ln" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptActuator" />
      <node concept="3uibUv" id="lT" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="lU" role="33vP2m">
        <ref role="37wK5l" node="lH" resolve="createDescriptorForActuator" />
      </node>
    </node>
    <node concept="312cEg" id="lo" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptApp" />
      <node concept="3uibUv" id="lV" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="lW" role="33vP2m">
        <ref role="37wK5l" node="lI" resolve="createDescriptorForApp" />
      </node>
    </node>
    <node concept="312cEg" id="lp" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptBrick" />
      <node concept="3uibUv" id="lX" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="lY" role="33vP2m">
        <ref role="37wK5l" node="lJ" resolve="createDescriptorForBrick" />
      </node>
    </node>
    <node concept="312cEg" id="lq" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptCondition" />
      <node concept="3uibUv" id="lZ" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="m0" role="33vP2m">
        <ref role="37wK5l" node="lK" resolve="createDescriptorForCondition" />
      </node>
    </node>
    <node concept="312cEg" id="lr" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptConditionAnalogic" />
      <node concept="3uibUv" id="m1" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="m2" role="33vP2m">
        <ref role="37wK5l" node="lL" resolve="createDescriptorForConditionAnalogic" />
      </node>
    </node>
    <node concept="312cEg" id="ls" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptICondition" />
      <node concept="3uibUv" id="m3" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="m4" role="33vP2m">
        <ref role="37wK5l" node="lM" resolve="createDescriptorForICondition" />
      </node>
    </node>
    <node concept="312cEg" id="lt" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptLogicOp" />
      <node concept="3uibUv" id="m5" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="m6" role="33vP2m">
        <ref role="37wK5l" node="lN" resolve="createDescriptorForLogicOp" />
      </node>
    </node>
    <node concept="312cEg" id="lu" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptSensor" />
      <node concept="3uibUv" id="m7" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="m8" role="33vP2m">
        <ref role="37wK5l" node="lO" resolve="createDescriptorForSensor" />
      </node>
    </node>
    <node concept="312cEg" id="lv" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptState" />
      <node concept="3uibUv" id="m9" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="ma" role="33vP2m">
        <ref role="37wK5l" node="lP" resolve="createDescriptorForState" />
      </node>
    </node>
    <node concept="312cEg" id="lw" role="jymVt">
      <property role="eg7rD" value="false" />
      <property role="34CwA1" value="false" />
      <property role="3TUv4t" value="true" />
      <property role="TrG5h" value="myConceptTransition" />
      <node concept="3uibUv" id="mb" role="1tU5fm">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="1rXfSq" id="mc" role="33vP2m">
        <ref role="37wK5l" node="lQ" resolve="createDescriptorForTransition" />
      </node>
    </node>
    <node concept="312cEg" id="lx" role="jymVt">
      <property role="TrG5h" value="myConceptIndex" />
      <property role="3TUv4t" value="true" />
      <node concept="3Tm6S6" id="md" role="1B3o_S" />
      <node concept="3uibUv" id="me" role="1tU5fm">
        <ref role="3uigEE" node="fn" resolve="LanguageConceptSwitch" />
      </node>
    </node>
    <node concept="3Tm1VV" id="ly" role="1B3o_S" />
    <node concept="2tJIrI" id="lz" role="jymVt" />
    <node concept="3clFbW" id="l$" role="jymVt">
      <node concept="3cqZAl" id="mf" role="3clF45" />
      <node concept="3Tm1VV" id="mg" role="1B3o_S" />
      <node concept="3clFbS" id="mh" role="3clF47">
        <node concept="3clFbF" id="mi" role="3cqZAp">
          <node concept="37vLTI" id="mj" role="3clFbG">
            <node concept="2ShNRf" id="mk" role="37vLTx">
              <node concept="1pGfFk" id="mm" role="2ShVmc">
                <ref role="37wK5l" node="f_" resolve="LanguageConceptSwitch" />
              </node>
            </node>
            <node concept="37vLTw" id="ml" role="37vLTJ">
              <ref role="3cqZAo" node="lx" resolve="myConceptIndex" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="l_" role="jymVt" />
    <node concept="3clFb_" id="lA" role="jymVt">
      <property role="1EzhhJ" value="false" />
      <property role="od$2w" value="false" />
      <property role="TrG5h" value="getDescriptors" />
      <property role="DiZV1" value="false" />
      <node concept="3clFbS" id="mn" role="3clF47">
        <node concept="3cpWs6" id="mr" role="3cqZAp">
          <node concept="2YIFZM" id="ms" role="3cqZAk">
            <ref role="1Pybhc" to="33ny:~Arrays" resolve="Arrays" />
            <ref role="37wK5l" to="33ny:~Arrays.asList(java.lang.Object...):java.util.List" resolve="asList" />
            <node concept="37vLTw" id="mt" role="37wK5m">
              <ref role="3cqZAo" node="lm" resolve="myConceptAction" />
            </node>
            <node concept="37vLTw" id="mu" role="37wK5m">
              <ref role="3cqZAo" node="ln" resolve="myConceptActuator" />
            </node>
            <node concept="37vLTw" id="mv" role="37wK5m">
              <ref role="3cqZAo" node="lo" resolve="myConceptApp" />
            </node>
            <node concept="37vLTw" id="mw" role="37wK5m">
              <ref role="3cqZAo" node="lp" resolve="myConceptBrick" />
            </node>
            <node concept="37vLTw" id="mx" role="37wK5m">
              <ref role="3cqZAo" node="lq" resolve="myConceptCondition" />
            </node>
            <node concept="37vLTw" id="my" role="37wK5m">
              <ref role="3cqZAo" node="lr" resolve="myConceptConditionAnalogic" />
            </node>
            <node concept="37vLTw" id="mz" role="37wK5m">
              <ref role="3cqZAo" node="ls" resolve="myConceptICondition" />
            </node>
            <node concept="37vLTw" id="m$" role="37wK5m">
              <ref role="3cqZAo" node="lt" resolve="myConceptLogicOp" />
            </node>
            <node concept="37vLTw" id="m_" role="37wK5m">
              <ref role="3cqZAo" node="lu" resolve="myConceptSensor" />
            </node>
            <node concept="37vLTw" id="mA" role="37wK5m">
              <ref role="3cqZAo" node="lv" resolve="myConceptState" />
            </node>
            <node concept="37vLTw" id="mB" role="37wK5m">
              <ref role="3cqZAo" node="lw" resolve="myConceptTransition" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm1VV" id="mo" role="1B3o_S" />
      <node concept="3uibUv" id="mp" role="3clF45">
        <ref role="3uigEE" to="33ny:~Collection" resolve="Collection" />
        <node concept="3uibUv" id="mC" role="11_B2D">
          <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
        </node>
      </node>
      <node concept="2AHcQZ" id="mq" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
    </node>
    <node concept="2tJIrI" id="lB" role="jymVt" />
    <node concept="3clFb_" id="lC" role="jymVt">
      <property role="IEkAT" value="false" />
      <property role="1EzhhJ" value="false" />
      <property role="TrG5h" value="getDescriptor" />
      <property role="DiZV1" value="false" />
      <node concept="3Tm1VV" id="mD" role="1B3o_S" />
      <node concept="37vLTG" id="mE" role="3clF46">
        <property role="TrG5h" value="id" />
        <node concept="3uibUv" id="mJ" role="1tU5fm">
          <ref role="3uigEE" to="e8bb:~SConceptId" resolve="SConceptId" />
        </node>
      </node>
      <node concept="3clFbS" id="mF" role="3clF47">
        <node concept="3KaCP$" id="mK" role="3cqZAp">
          <node concept="3KbdKl" id="mL" role="3KbHQx">
            <node concept="3clFbS" id="mY" role="3Kbo56">
              <node concept="3cpWs6" id="n0" role="3cqZAp">
                <node concept="37vLTw" id="n1" role="3cqZAk">
                  <ref role="3cqZAo" node="lm" resolve="myConceptAction" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="mZ" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fp" resolve="Action" />
            </node>
          </node>
          <node concept="3KbdKl" id="mM" role="3KbHQx">
            <node concept="3clFbS" id="n2" role="3Kbo56">
              <node concept="3cpWs6" id="n4" role="3cqZAp">
                <node concept="37vLTw" id="n5" role="3cqZAk">
                  <ref role="3cqZAo" node="ln" resolve="myConceptActuator" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="n3" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fq" resolve="Actuator" />
            </node>
          </node>
          <node concept="3KbdKl" id="mN" role="3KbHQx">
            <node concept="3clFbS" id="n6" role="3Kbo56">
              <node concept="3cpWs6" id="n8" role="3cqZAp">
                <node concept="37vLTw" id="n9" role="3cqZAk">
                  <ref role="3cqZAo" node="lo" resolve="myConceptApp" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="n7" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fr" resolve="App" />
            </node>
          </node>
          <node concept="3KbdKl" id="mO" role="3KbHQx">
            <node concept="3clFbS" id="na" role="3Kbo56">
              <node concept="3cpWs6" id="nc" role="3cqZAp">
                <node concept="37vLTw" id="nd" role="3cqZAk">
                  <ref role="3cqZAo" node="lp" resolve="myConceptBrick" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nb" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fs" resolve="Brick" />
            </node>
          </node>
          <node concept="3KbdKl" id="mP" role="3KbHQx">
            <node concept="3clFbS" id="ne" role="3Kbo56">
              <node concept="3cpWs6" id="ng" role="3cqZAp">
                <node concept="37vLTw" id="nh" role="3cqZAk">
                  <ref role="3cqZAo" node="lq" resolve="myConceptCondition" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nf" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="ft" resolve="Condition" />
            </node>
          </node>
          <node concept="3KbdKl" id="mQ" role="3KbHQx">
            <node concept="3clFbS" id="ni" role="3Kbo56">
              <node concept="3cpWs6" id="nk" role="3cqZAp">
                <node concept="37vLTw" id="nl" role="3cqZAk">
                  <ref role="3cqZAo" node="lr" resolve="myConceptConditionAnalogic" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nj" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fu" resolve="ConditionAnalogic" />
            </node>
          </node>
          <node concept="3KbdKl" id="mR" role="3KbHQx">
            <node concept="3clFbS" id="nm" role="3Kbo56">
              <node concept="3cpWs6" id="no" role="3cqZAp">
                <node concept="37vLTw" id="np" role="3cqZAk">
                  <ref role="3cqZAo" node="ls" resolve="myConceptICondition" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nn" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fv" resolve="ICondition" />
            </node>
          </node>
          <node concept="3KbdKl" id="mS" role="3KbHQx">
            <node concept="3clFbS" id="nq" role="3Kbo56">
              <node concept="3cpWs6" id="ns" role="3cqZAp">
                <node concept="37vLTw" id="nt" role="3cqZAk">
                  <ref role="3cqZAo" node="lt" resolve="myConceptLogicOp" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nr" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fw" resolve="LogicOp" />
            </node>
          </node>
          <node concept="3KbdKl" id="mT" role="3KbHQx">
            <node concept="3clFbS" id="nu" role="3Kbo56">
              <node concept="3cpWs6" id="nw" role="3cqZAp">
                <node concept="37vLTw" id="nx" role="3cqZAk">
                  <ref role="3cqZAo" node="lu" resolve="myConceptSensor" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nv" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fx" resolve="Sensor" />
            </node>
          </node>
          <node concept="3KbdKl" id="mU" role="3KbHQx">
            <node concept="3clFbS" id="ny" role="3Kbo56">
              <node concept="3cpWs6" id="n$" role="3cqZAp">
                <node concept="37vLTw" id="n_" role="3cqZAk">
                  <ref role="3cqZAo" node="lv" resolve="myConceptState" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nz" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fy" resolve="State" />
            </node>
          </node>
          <node concept="3KbdKl" id="mV" role="3KbHQx">
            <node concept="3clFbS" id="nA" role="3Kbo56">
              <node concept="3cpWs6" id="nC" role="3cqZAp">
                <node concept="37vLTw" id="nD" role="3cqZAk">
                  <ref role="3cqZAo" node="lw" resolve="myConceptTransition" />
                </node>
              </node>
            </node>
            <node concept="10M0yZ" id="nB" role="3Kbmr1">
              <ref role="1PxDUh" node="fn" resolve="LanguageConceptSwitch" />
              <ref role="3cqZAo" node="fz" resolve="Transition" />
            </node>
          </node>
          <node concept="2OqwBi" id="mW" role="3KbGdf">
            <node concept="37vLTw" id="nE" role="2Oq$k0">
              <ref role="3cqZAo" node="lx" resolve="myConceptIndex" />
            </node>
            <node concept="liA8E" id="nF" role="2OqNvi">
              <ref role="37wK5l" node="fB" resolve="index" />
              <node concept="37vLTw" id="nG" role="37wK5m">
                <ref role="3cqZAo" node="mE" resolve="id" />
              </node>
            </node>
          </node>
          <node concept="3clFbS" id="mX" role="3Kb1Dw">
            <node concept="3cpWs6" id="nH" role="3cqZAp">
              <node concept="10Nm6u" id="nI" role="3cqZAk" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3uibUv" id="mG" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
      <node concept="2AHcQZ" id="mH" role="2AJF6D">
        <ref role="2AI5Lk" to="wyt6:~Override" resolve="Override" />
      </node>
      <node concept="2AHcQZ" id="mI" role="2AJF6D">
        <ref role="2AI5Lk" to="mhfm:~Nullable" resolve="Nullable" />
      </node>
    </node>
    <node concept="2tJIrI" id="lD" role="jymVt" />
    <node concept="3clFb_" id="lE" role="jymVt">
      <property role="TrG5h" value="internalIndex" />
      <node concept="10Oyi0" id="nJ" role="3clF45" />
      <node concept="3clFbS" id="nK" role="3clF47">
        <node concept="3cpWs6" id="nM" role="3cqZAp">
          <node concept="2OqwBi" id="nN" role="3cqZAk">
            <node concept="37vLTw" id="nO" role="2Oq$k0">
              <ref role="3cqZAo" node="lx" resolve="myConceptIndex" />
            </node>
            <node concept="liA8E" id="nP" role="2OqNvi">
              <ref role="37wK5l" node="fD" resolve="index" />
              <node concept="37vLTw" id="nQ" role="37wK5m">
                <ref role="3cqZAo" node="nL" resolve="c" />
              </node>
            </node>
          </node>
        </node>
      </node>
      <node concept="37vLTG" id="nL" role="3clF46">
        <property role="TrG5h" value="c" />
        <node concept="3uibUv" id="nR" role="1tU5fm">
          <ref role="3uigEE" to="c17a:~SAbstractConcept" resolve="SAbstractConcept" />
        </node>
      </node>
    </node>
    <node concept="2tJIrI" id="lF" role="jymVt" />
    <node concept="2YIFZL" id="lG" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForAction" />
      <node concept="3clFbS" id="nS" role="3clF47">
        <node concept="3cpWs8" id="nV" role="3cqZAp">
          <node concept="3cpWsn" id="o1" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="o2" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="o3" role="33vP2m">
              <node concept="1pGfFk" id="o4" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="o5" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="o6" role="37wK5m">
                  <property role="Xl_RC" value="Action" />
                </node>
                <node concept="1adDum" id="o7" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="o8" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="o9" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a57389a0L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nW" role="3cqZAp">
          <node concept="2OqwBi" id="oa" role="3clFbG">
            <node concept="37vLTw" id="ob" role="2Oq$k0">
              <ref role="3cqZAo" node="o1" resolve="b" />
            </node>
            <node concept="liA8E" id="oc" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="od" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="oe" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="of" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nX" role="3cqZAp">
          <node concept="2OqwBi" id="og" role="3clFbG">
            <node concept="37vLTw" id="oh" role="2Oq$k0">
              <ref role="3cqZAo" node="o1" resolve="b" />
            </node>
            <node concept="liA8E" id="oi" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="oj" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797267360" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nY" role="3cqZAp">
          <node concept="2OqwBi" id="ok" role="3clFbG">
            <node concept="37vLTw" id="ol" role="2Oq$k0">
              <ref role="3cqZAo" node="o1" resolve="b" />
            </node>
            <node concept="liA8E" id="om" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="on" role="37wK5m">
                <property role="Xl_RC" value="signal" />
              </node>
              <node concept="1adDum" id="oo" role="37wK5m">
                <property role="1adDun" value="0x6598d354a57389bcL" />
              </node>
              <node concept="Xl_RD" id="op" role="37wK5m">
                <property role="Xl_RC" value="7320833554797267388" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="nZ" role="3cqZAp">
          <node concept="2OqwBi" id="oq" role="3clFbG">
            <node concept="2OqwBi" id="or" role="2Oq$k0">
              <node concept="2OqwBi" id="ot" role="2Oq$k0">
                <node concept="2OqwBi" id="ov" role="2Oq$k0">
                  <node concept="2OqwBi" id="ox" role="2Oq$k0">
                    <node concept="37vLTw" id="oz" role="2Oq$k0">
                      <ref role="3cqZAo" node="o1" resolve="b" />
                    </node>
                    <node concept="liA8E" id="o$" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="associate" />
                      <node concept="Xl_RD" id="o_" role="37wK5m">
                        <property role="Xl_RC" value="target" />
                      </node>
                      <node concept="1adDum" id="oA" role="37wK5m">
                        <property role="1adDun" value="0x6598d354a57427daL" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="oy" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="target" />
                    <node concept="1adDum" id="oB" role="37wK5m">
                      <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                    </node>
                    <node concept="1adDum" id="oC" role="37wK5m">
                      <property role="1adDun" value="0x9882c5f0dd3df659L" />
                    </node>
                    <node concept="1adDum" id="oD" role="37wK5m">
                      <property role="1adDun" value="0x6598d354a573893dL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="ow" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="optional" />
                  <node concept="3clFbT" id="oE" role="37wK5m">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="ou" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="oF" role="37wK5m">
                  <property role="Xl_RC" value="7320833554797307866" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="os" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="o0" role="3cqZAp">
          <node concept="2OqwBi" id="oG" role="3cqZAk">
            <node concept="37vLTw" id="oH" role="2Oq$k0">
              <ref role="3cqZAo" node="o1" resolve="b" />
            </node>
            <node concept="liA8E" id="oI" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="nT" role="1B3o_S" />
      <node concept="3uibUv" id="nU" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lH" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForActuator" />
      <node concept="3clFbS" id="oJ" role="3clF47">
        <node concept="3cpWs8" id="oM" role="3cqZAp">
          <node concept="3cpWsn" id="oT" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="oU" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="oV" role="33vP2m">
              <node concept="1pGfFk" id="oW" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="oX" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="oY" role="37wK5m">
                  <property role="Xl_RC" value="Actuator" />
                </node>
                <node concept="1adDum" id="oZ" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="p0" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="p1" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a573893dL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="oN" role="3cqZAp">
          <node concept="2OqwBi" id="p2" role="3clFbG">
            <node concept="37vLTw" id="p3" role="2Oq$k0">
              <ref role="3cqZAo" node="oT" resolve="b" />
            </node>
            <node concept="liA8E" id="p4" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="p5" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="p6" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="p7" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="oO" role="3cqZAp">
          <node concept="2OqwBi" id="p8" role="3clFbG">
            <node concept="37vLTw" id="p9" role="2Oq$k0">
              <ref role="3cqZAo" node="oT" resolve="b" />
            </node>
            <node concept="liA8E" id="pa" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="super_" />
              <node concept="Xl_RD" id="pb" role="37wK5m">
                <property role="Xl_RC" value="ArduinoML.structure.Brick" />
              </node>
              <node concept="1adDum" id="pc" role="37wK5m">
                <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
              </node>
              <node concept="1adDum" id="pd" role="37wK5m">
                <property role="1adDun" value="0x9882c5f0dd3df659L" />
              </node>
              <node concept="1adDum" id="pe" role="37wK5m">
                <property role="1adDun" value="0x6598d354a57431f2L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="oP" role="3cqZAp">
          <node concept="2OqwBi" id="pf" role="3clFbG">
            <node concept="37vLTw" id="pg" role="2Oq$k0">
              <ref role="3cqZAo" node="oT" resolve="b" />
            </node>
            <node concept="liA8E" id="ph" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="pi" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797267261" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="oQ" role="3cqZAp">
          <node concept="2OqwBi" id="pj" role="3clFbG">
            <node concept="37vLTw" id="pk" role="2Oq$k0">
              <ref role="3cqZAo" node="oT" resolve="b" />
            </node>
            <node concept="liA8E" id="pl" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="pm" role="37wK5m">
                <property role="Xl_RC" value="analog" />
              </node>
              <node concept="1adDum" id="pn" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fcb8bf68L" />
              </node>
              <node concept="Xl_RD" id="po" role="37wK5m">
                <property role="Xl_RC" value="3199544644538515304" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="oR" role="3cqZAp">
          <node concept="2OqwBi" id="pp" role="3clFbG">
            <node concept="37vLTw" id="pq" role="2Oq$k0">
              <ref role="3cqZAo" node="oT" resolve="b" />
            </node>
            <node concept="liA8E" id="pr" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.alias(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="alias" />
              <node concept="Xl_RD" id="ps" role="37wK5m">
                <property role="Xl_RC" value="actuator" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="oS" role="3cqZAp">
          <node concept="2OqwBi" id="pt" role="3cqZAk">
            <node concept="37vLTw" id="pu" role="2Oq$k0">
              <ref role="3cqZAo" node="oT" resolve="b" />
            </node>
            <node concept="liA8E" id="pv" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="oK" role="1B3o_S" />
      <node concept="3uibUv" id="oL" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lI" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForApp" />
      <node concept="3clFbS" id="pw" role="3clF47">
        <node concept="3cpWs8" id="pz" role="3cqZAp">
          <node concept="3cpWsn" id="pF" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="pG" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="pH" role="33vP2m">
              <node concept="1pGfFk" id="pI" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="pJ" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="pK" role="37wK5m">
                  <property role="Xl_RC" value="App" />
                </node>
                <node concept="1adDum" id="pL" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="pM" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="pN" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a573891eL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="p$" role="3cqZAp">
          <node concept="2OqwBi" id="pO" role="3clFbG">
            <node concept="37vLTw" id="pP" role="2Oq$k0">
              <ref role="3cqZAo" node="pF" resolve="b" />
            </node>
            <node concept="liA8E" id="pQ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="pR" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="pS" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="pT" role="37wK5m">
                <property role="3clFbU" value="true" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="p_" role="3cqZAp">
          <node concept="2OqwBi" id="pU" role="3clFbG">
            <node concept="37vLTw" id="pV" role="2Oq$k0">
              <ref role="3cqZAo" node="pF" resolve="b" />
            </node>
            <node concept="liA8E" id="pW" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.parent(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="parent" />
              <node concept="1adDum" id="pX" role="37wK5m">
                <property role="1adDun" value="0xceab519525ea4f22L" />
              </node>
              <node concept="1adDum" id="pY" role="37wK5m">
                <property role="1adDun" value="0x9b92103b95ca8c0cL" />
              </node>
              <node concept="1adDum" id="pZ" role="37wK5m">
                <property role="1adDun" value="0x110396eaaa4L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="pA" role="3cqZAp">
          <node concept="2OqwBi" id="q0" role="3clFbG">
            <node concept="37vLTw" id="q1" role="2Oq$k0">
              <ref role="3cqZAo" node="pF" resolve="b" />
            </node>
            <node concept="liA8E" id="q2" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="q3" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797267230" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="pB" role="3cqZAp">
          <node concept="2OqwBi" id="q4" role="3clFbG">
            <node concept="2OqwBi" id="q5" role="2Oq$k0">
              <node concept="2OqwBi" id="q7" role="2Oq$k0">
                <node concept="2OqwBi" id="q9" role="2Oq$k0">
                  <node concept="2OqwBi" id="qb" role="2Oq$k0">
                    <node concept="2OqwBi" id="qd" role="2Oq$k0">
                      <node concept="2OqwBi" id="qf" role="2Oq$k0">
                        <node concept="37vLTw" id="qh" role="2Oq$k0">
                          <ref role="3cqZAo" node="pF" resolve="b" />
                        </node>
                        <node concept="liA8E" id="qi" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="qj" role="37wK5m">
                            <property role="Xl_RC" value="actuators" />
                          </node>
                          <node concept="1adDum" id="qk" role="37wK5m">
                            <property role="1adDun" value="0x6598d354a5741fd7L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="qg" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="ql" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="qm" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="qn" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a573893dL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="qe" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="qo" role="37wK5m">
                        <property role="3clFbU" value="false" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="qc" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="qp" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="qa" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="qq" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="q8" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="qr" role="37wK5m">
                  <property role="Xl_RC" value="7320833554797305815" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="q6" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="pC" role="3cqZAp">
          <node concept="2OqwBi" id="qs" role="3clFbG">
            <node concept="2OqwBi" id="qt" role="2Oq$k0">
              <node concept="2OqwBi" id="qv" role="2Oq$k0">
                <node concept="2OqwBi" id="qx" role="2Oq$k0">
                  <node concept="2OqwBi" id="qz" role="2Oq$k0">
                    <node concept="2OqwBi" id="q_" role="2Oq$k0">
                      <node concept="2OqwBi" id="qB" role="2Oq$k0">
                        <node concept="37vLTw" id="qD" role="2Oq$k0">
                          <ref role="3cqZAo" node="pF" resolve="b" />
                        </node>
                        <node concept="liA8E" id="qE" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="qF" role="37wK5m">
                            <property role="Xl_RC" value="sensors" />
                          </node>
                          <node concept="1adDum" id="qG" role="37wK5m">
                            <property role="1adDun" value="0x6598d354a57431eeL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="qC" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="qH" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="qI" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="qJ" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a57431b3L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="qA" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="qK" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="q$" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="qL" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="qy" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="qM" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="qw" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="qN" role="37wK5m">
                  <property role="Xl_RC" value="7320833554797310446" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="qu" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="pD" role="3cqZAp">
          <node concept="2OqwBi" id="qO" role="3clFbG">
            <node concept="2OqwBi" id="qP" role="2Oq$k0">
              <node concept="2OqwBi" id="qR" role="2Oq$k0">
                <node concept="2OqwBi" id="qT" role="2Oq$k0">
                  <node concept="2OqwBi" id="qV" role="2Oq$k0">
                    <node concept="2OqwBi" id="qX" role="2Oq$k0">
                      <node concept="2OqwBi" id="qZ" role="2Oq$k0">
                        <node concept="37vLTw" id="r1" role="2Oq$k0">
                          <ref role="3cqZAo" node="pF" resolve="b" />
                        </node>
                        <node concept="liA8E" id="r2" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="r3" role="37wK5m">
                            <property role="Xl_RC" value="states" />
                          </node>
                          <node concept="1adDum" id="r4" role="37wK5m">
                            <property role="1adDun" value="0x6598d354a5741fd4L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="r0" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="r5" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="r6" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="r7" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a573895cL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="qY" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="r8" role="37wK5m">
                        <property role="3clFbU" value="false" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="qW" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="r9" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="qU" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="ra" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="qS" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="rb" role="37wK5m">
                  <property role="Xl_RC" value="7320833554797305812" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="qQ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="pE" role="3cqZAp">
          <node concept="2OqwBi" id="rc" role="3cqZAk">
            <node concept="37vLTw" id="rd" role="2Oq$k0">
              <ref role="3cqZAo" node="pF" resolve="b" />
            </node>
            <node concept="liA8E" id="re" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="px" role="1B3o_S" />
      <node concept="3uibUv" id="py" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lJ" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForBrick" />
      <node concept="3clFbS" id="rf" role="3clF47">
        <node concept="3cpWs8" id="ri" role="3cqZAp">
          <node concept="3cpWsn" id="ro" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="rp" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="rq" role="33vP2m">
              <node concept="1pGfFk" id="rr" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="rs" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="rt" role="37wK5m">
                  <property role="Xl_RC" value="Brick" />
                </node>
                <node concept="1adDum" id="ru" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="rv" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="rw" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a57431f2L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rj" role="3cqZAp">
          <node concept="2OqwBi" id="rx" role="3clFbG">
            <node concept="37vLTw" id="ry" role="2Oq$k0">
              <ref role="3cqZAo" node="ro" resolve="b" />
            </node>
            <node concept="liA8E" id="rz" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="r$" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="r_" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="rA" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rk" role="3cqZAp">
          <node concept="2OqwBi" id="rB" role="3clFbG">
            <node concept="37vLTw" id="rC" role="2Oq$k0">
              <ref role="3cqZAo" node="ro" resolve="b" />
            </node>
            <node concept="liA8E" id="rD" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.parent(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="parent" />
              <node concept="1adDum" id="rE" role="37wK5m">
                <property role="1adDun" value="0xceab519525ea4f22L" />
              </node>
              <node concept="1adDum" id="rF" role="37wK5m">
                <property role="1adDun" value="0x9b92103b95ca8c0cL" />
              </node>
              <node concept="1adDum" id="rG" role="37wK5m">
                <property role="1adDun" value="0x110396eaaa4L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rl" role="3cqZAp">
          <node concept="2OqwBi" id="rH" role="3clFbG">
            <node concept="37vLTw" id="rI" role="2Oq$k0">
              <ref role="3cqZAo" node="ro" resolve="b" />
            </node>
            <node concept="liA8E" id="rJ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="rK" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797310450" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rm" role="3cqZAp">
          <node concept="2OqwBi" id="rL" role="3clFbG">
            <node concept="37vLTw" id="rM" role="2Oq$k0">
              <ref role="3cqZAo" node="ro" resolve="b" />
            </node>
            <node concept="liA8E" id="rN" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="rO" role="37wK5m">
                <property role="Xl_RC" value="pin" />
              </node>
              <node concept="1adDum" id="rP" role="37wK5m">
                <property role="1adDun" value="0x6598d354a574322bL" />
              </node>
              <node concept="Xl_RD" id="rQ" role="37wK5m">
                <property role="Xl_RC" value="7320833554797310507" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="rn" role="3cqZAp">
          <node concept="2OqwBi" id="rR" role="3cqZAk">
            <node concept="37vLTw" id="rS" role="2Oq$k0">
              <ref role="3cqZAo" node="ro" resolve="b" />
            </node>
            <node concept="liA8E" id="rT" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="rg" role="1B3o_S" />
      <node concept="3uibUv" id="rh" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lK" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForCondition" />
      <node concept="3clFbS" id="rU" role="3clF47">
        <node concept="3cpWs8" id="rX" role="3cqZAp">
          <node concept="3cpWsn" id="s5" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="s6" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="s7" role="33vP2m">
              <node concept="1pGfFk" id="s8" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="s9" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="sa" role="37wK5m">
                  <property role="Xl_RC" value="Condition" />
                </node>
                <node concept="1adDum" id="sb" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="sc" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="sd" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a574326fL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rY" role="3cqZAp">
          <node concept="2OqwBi" id="se" role="3clFbG">
            <node concept="37vLTw" id="sf" role="2Oq$k0">
              <ref role="3cqZAo" node="s5" resolve="b" />
            </node>
            <node concept="liA8E" id="sg" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="sh" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="si" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="sj" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="rZ" role="3cqZAp">
          <node concept="2OqwBi" id="sk" role="3clFbG">
            <node concept="37vLTw" id="sl" role="2Oq$k0">
              <ref role="3cqZAo" node="uQ" resolve="b" />
            </node>
            <node concept="liA8E" id="sm" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.parent(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="parent" />
              <node concept="1adDum" id="sn" role="37wK5m">
                <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
              </node>
              <node concept="1adDum" id="so" role="37wK5m">
                <property role="1adDun" value="0x9882c5f0dd3df659L" />
              </node>
              <node concept="1adDum" id="sp" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fcb3e16fL" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="s0" role="3cqZAp">
          <node concept="2OqwBi" id="sq" role="3clFbG">
            <node concept="37vLTw" id="sr" role="2Oq$k0">
              <ref role="3cqZAo" node="s5" resolve="b" />
            </node>
            <node concept="liA8E" id="ss" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="st" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797310575" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="s1" role="3cqZAp">
          <node concept="2OqwBi" id="su" role="3clFbG">
            <node concept="37vLTw" id="sv" role="2Oq$k0">
              <ref role="3cqZAo" node="s5" resolve="b" />
            </node>
            <node concept="liA8E" id="sw" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="sx" role="37wK5m">
                <property role="Xl_RC" value="signal" />
              </node>
              <node concept="1adDum" id="sy" role="37wK5m">
                <property role="1adDun" value="0x6c655ac4f1eb8d17L" />
              </node>
              <node concept="Xl_RD" id="sz" role="37wK5m">
                <property role="Xl_RC" value="7810748930662829335" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="s2" role="3cqZAp">
          <node concept="2OqwBi" id="s$" role="3clFbG">
            <node concept="2OqwBi" id="s_" role="2Oq$k0">
              <node concept="2OqwBi" id="sB" role="2Oq$k0">
                <node concept="2OqwBi" id="sD" role="2Oq$k0">
                  <node concept="2OqwBi" id="sF" role="2Oq$k0">
                    <node concept="37vLTw" id="sH" role="2Oq$k0">
                      <ref role="3cqZAo" node="s5" resolve="b" />
                    </node>
                    <node concept="liA8E" id="sI" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="associate" />
                      <node concept="Xl_RD" id="sJ" role="37wK5m">
                        <property role="Xl_RC" value="sensor" />
                      </node>
                      <node concept="1adDum" id="sK" role="37wK5m">
                        <property role="1adDun" value="0x6c655ac4f1eb8d19L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="sG" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="target" />
                    <node concept="1adDum" id="sL" role="37wK5m">
                      <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                    </node>
                    <node concept="1adDum" id="sM" role="37wK5m">
                      <property role="1adDun" value="0x9882c5f0dd3df659L" />
                    </node>
                    <node concept="1adDum" id="sN" role="37wK5m">
                      <property role="1adDun" value="0x6598d354a57431b3L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="sE" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="optional" />
                  <node concept="3clFbT" id="sO" role="37wK5m">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="sC" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="sP" role="37wK5m">
                  <property role="Xl_RC" value="7810748930662829337" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="sA" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="s3" role="3cqZAp">
          <node concept="2OqwBi" id="sQ" role="3clFbG">
            <node concept="2OqwBi" id="sR" role="2Oq$k0">
              <node concept="2OqwBi" id="sT" role="2Oq$k0">
                <node concept="2OqwBi" id="sV" role="2Oq$k0">
                  <node concept="2OqwBi" id="sX" role="2Oq$k0">
                    <node concept="2OqwBi" id="sZ" role="2Oq$k0">
                      <node concept="2OqwBi" id="t1" role="2Oq$k0">
                        <node concept="37vLTw" id="t3" role="2Oq$k0">
                          <ref role="3cqZAo" node="s5" resolve="b" />
                        </node>
                        <node concept="liA8E" id="t4" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="t5" role="37wK5m">
                            <property role="Xl_RC" value="logicOp" />
                          </node>
                          <node concept="1adDum" id="t6" role="37wK5m">
                            <property role="1adDun" value="0x6c655ac4f1eb8d1bL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="t2" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="t7" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="t8" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="t9" role="37wK5m">
                          <property role="1adDun" value="0x2c6710e6fc99ed09L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="t0" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="ta" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="sY" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="tb" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="sW" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="tc" role="37wK5m">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="sU" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="td" role="37wK5m">
                  <property role="Xl_RC" value="7810748930662829339" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="sS" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="s4" role="3cqZAp">
          <node concept="2OqwBi" id="te" role="3cqZAk">
            <node concept="37vLTw" id="tf" role="2Oq$k0">
              <ref role="3cqZAo" node="s5" resolve="b" />
            </node>
            <node concept="liA8E" id="tg" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="rV" role="1B3o_S" />
      <node concept="3uibUv" id="rW" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lL" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForConditionAnalogic" />
      <node concept="3clFbS" id="th" role="3clF47">
        <node concept="3cpWs8" id="tk" role="3cqZAp">
          <node concept="3cpWsn" id="tt" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="tu" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="tv" role="33vP2m">
              <node concept="1pGfFk" id="tw" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="tx" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="ty" role="37wK5m">
                  <property role="Xl_RC" value="ConditionAnalogic" />
                </node>
                <node concept="1adDum" id="tz" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="t$" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="t_" role="37wK5m">
                  <property role="1adDun" value="0x2c6710e6fcb44fe0L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tl" role="3cqZAp">
          <node concept="2OqwBi" id="tA" role="3clFbG">
            <node concept="37vLTw" id="tB" role="2Oq$k0">
              <ref role="3cqZAo" node="tt" resolve="b" />
            </node>
            <node concept="liA8E" id="tC" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="tD" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="tE" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="tF" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tm" role="3cqZAp">
          <node concept="2OqwBi" id="tG" role="3clFbG">
            <node concept="37vLTw" id="tH" role="2Oq$k0">
              <ref role="3cqZAo" node="uQ" resolve="b" />
            </node>
            <node concept="liA8E" id="tI" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.parent(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="parent" />
              <node concept="1adDum" id="tJ" role="37wK5m">
                <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
              </node>
              <node concept="1adDum" id="tK" role="37wK5m">
                <property role="1adDun" value="0x9882c5f0dd3df659L" />
              </node>
              <node concept="1adDum" id="tL" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fcb3e16fL" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tn" role="3cqZAp">
          <node concept="2OqwBi" id="tM" role="3clFbG">
            <node concept="37vLTw" id="tN" role="2Oq$k0">
              <ref role="3cqZAo" node="tt" resolve="b" />
            </node>
            <node concept="liA8E" id="tO" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="tP" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/3199544644538224608" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="to" role="3cqZAp">
          <node concept="2OqwBi" id="tQ" role="3clFbG">
            <node concept="37vLTw" id="tR" role="2Oq$k0">
              <ref role="3cqZAo" node="tt" resolve="b" />
            </node>
            <node concept="liA8E" id="tS" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="tT" role="37wK5m">
                <property role="Xl_RC" value="num" />
              </node>
              <node concept="1adDum" id="tU" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fcb44fe3L" />
              </node>
              <node concept="Xl_RD" id="tV" role="37wK5m">
                <property role="Xl_RC" value="3199544644538224611" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tp" role="3cqZAp">
          <node concept="2OqwBi" id="tW" role="3clFbG">
            <node concept="37vLTw" id="tX" role="2Oq$k0">
              <ref role="3cqZAo" node="tt" resolve="b" />
            </node>
            <node concept="liA8E" id="tY" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="tZ" role="37wK5m">
                <property role="Xl_RC" value="comp" />
              </node>
              <node concept="1adDum" id="u0" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fcb44fe5L" />
              </node>
              <node concept="Xl_RD" id="u1" role="37wK5m">
                <property role="Xl_RC" value="3199544644538224613" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tq" role="3cqZAp">
          <node concept="2OqwBi" id="u2" role="3clFbG">
            <node concept="2OqwBi" id="u3" role="2Oq$k0">
              <node concept="2OqwBi" id="u5" role="2Oq$k0">
                <node concept="2OqwBi" id="u7" role="2Oq$k0">
                  <node concept="2OqwBi" id="u9" role="2Oq$k0">
                    <node concept="37vLTw" id="ub" role="2Oq$k0">
                      <ref role="3cqZAo" node="tt" resolve="b" />
                    </node>
                    <node concept="liA8E" id="uc" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="associate" />
                      <node concept="Xl_RD" id="ud" role="37wK5m">
                        <property role="Xl_RC" value="sensor" />
                      </node>
                      <node concept="1adDum" id="ue" role="37wK5m">
                        <property role="1adDun" value="0x2c6710e6fcb44feaL" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="ua" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="target" />
                    <node concept="1adDum" id="uf" role="37wK5m">
                      <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                    </node>
                    <node concept="1adDum" id="ug" role="37wK5m">
                      <property role="1adDun" value="0x9882c5f0dd3df659L" />
                    </node>
                    <node concept="1adDum" id="uh" role="37wK5m">
                      <property role="1adDun" value="0x6598d354a57431b3L" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="u8" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="optional" />
                  <node concept="3clFbT" id="ui" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="u6" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="uj" role="37wK5m">
                  <property role="Xl_RC" value="3199544644538224618" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="u4" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="tr" role="3cqZAp">
          <node concept="2OqwBi" id="uk" role="3clFbG">
            <node concept="2OqwBi" id="ul" role="2Oq$k0">
              <node concept="2OqwBi" id="un" role="2Oq$k0">
                <node concept="2OqwBi" id="up" role="2Oq$k0">
                  <node concept="2OqwBi" id="ur" role="2Oq$k0">
                    <node concept="2OqwBi" id="ut" role="2Oq$k0">
                      <node concept="2OqwBi" id="uv" role="2Oq$k0">
                        <node concept="37vLTw" id="ux" role="2Oq$k0">
                          <ref role="3cqZAo" node="tt" resolve="b" />
                        </node>
                        <node concept="liA8E" id="uy" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="uz" role="37wK5m">
                            <property role="Xl_RC" value="logicOp" />
                          </node>
                          <node concept="1adDum" id="u$" role="37wK5m">
                            <property role="1adDun" value="0x2c6710e6fcb44fe8L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="uw" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="u_" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="uA" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="uB" role="37wK5m">
                          <property role="1adDun" value="0x2c6710e6fc99ed09L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="uu" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="uC" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="us" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="uD" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="uq" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="uE" role="37wK5m">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="uo" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="uF" role="37wK5m">
                  <property role="Xl_RC" value="3199544644538224616" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="um" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="ts" role="3cqZAp">
          <node concept="2OqwBi" id="uG" role="3cqZAk">
            <node concept="37vLTw" id="uH" role="2Oq$k0">
              <ref role="3cqZAo" node="tt" resolve="b" />
            </node>
            <node concept="liA8E" id="uI" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="ti" role="1B3o_S" />
      <node concept="3uibUv" id="tj" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lM" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForICondition" />
      <node concept="3clFbS" id="uJ" role="3clF47">
        <node concept="3cpWs8" id="uM" role="3cqZAp">
          <node concept="3cpWsn" id="uQ" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="uR" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="uS" role="33vP2m">
              <node concept="1pGfFk" id="uT" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="uU" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="uV" role="37wK5m">
                  <property role="Xl_RC" value="ICondition" />
                </node>
                <node concept="1adDum" id="uW" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="uX" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="uY" role="37wK5m">
                  <property role="1adDun" value="0x2c6710e6fcb3e16fL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uN" role="3cqZAp">
          <node concept="2OqwBi" id="uZ" role="3clFbG">
            <node concept="37vLTw" id="v0" role="2Oq$k0">
              <ref role="3cqZAo" node="uQ" resolve="b" />
            </node>
            <node concept="liA8E" id="v1" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.interface_():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="interface_" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="uO" role="3cqZAp">
          <node concept="2OqwBi" id="v2" role="3clFbG">
            <node concept="37vLTw" id="v3" role="2Oq$k0">
              <ref role="3cqZAo" node="uQ" resolve="b" />
            </node>
            <node concept="liA8E" id="v4" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="v5" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/3199544644538196335" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="uP" role="3cqZAp">
          <node concept="2OqwBi" id="v6" role="3cqZAk">
            <node concept="37vLTw" id="v7" role="2Oq$k0">
              <ref role="3cqZAo" node="uQ" resolve="b" />
            </node>
            <node concept="liA8E" id="v8" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="uK" role="1B3o_S" />
      <node concept="3uibUv" id="uL" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lN" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForLogicOp" />
      <node concept="3clFbS" id="v9" role="3clF47">
        <node concept="3cpWs8" id="vc" role="3cqZAp">
          <node concept="3cpWsn" id="vi" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="vj" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="vk" role="33vP2m">
              <node concept="1pGfFk" id="vl" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="vm" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="vn" role="37wK5m">
                  <property role="Xl_RC" value="LogicOp" />
                </node>
                <node concept="1adDum" id="vo" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="vp" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="vq" role="37wK5m">
                  <property role="1adDun" value="0x2c6710e6fc99ed09L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vd" role="3cqZAp">
          <node concept="2OqwBi" id="vr" role="3clFbG">
            <node concept="37vLTw" id="vs" role="2Oq$k0">
              <ref role="3cqZAo" node="vi" resolve="b" />
            </node>
            <node concept="liA8E" id="vt" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="vu" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="vv" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="vw" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ve" role="3cqZAp">
          <node concept="2OqwBi" id="vx" role="3clFbG">
            <node concept="37vLTw" id="vy" role="2Oq$k0">
              <ref role="3cqZAo" node="vi" resolve="b" />
            </node>
            <node concept="liA8E" id="vz" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="v$" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/3199544644536495369" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vf" role="3cqZAp">
          <node concept="2OqwBi" id="v_" role="3clFbG">
            <node concept="37vLTw" id="vA" role="2Oq$k0">
              <ref role="3cqZAo" node="vi" resolve="b" />
            </node>
            <node concept="liA8E" id="vB" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="vC" role="37wK5m">
                <property role="Xl_RC" value="operator" />
              </node>
              <node concept="1adDum" id="vD" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fc9c9dc2L" />
              </node>
              <node concept="Xl_RD" id="vE" role="37wK5m">
                <property role="Xl_RC" value="3199544644536671682" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="vg" role="3cqZAp">
          <node concept="2OqwBi" id="vF" role="3clFbG">
            <node concept="2OqwBi" id="vG" role="2Oq$k0">
              <node concept="2OqwBi" id="vI" role="2Oq$k0">
                <node concept="2OqwBi" id="vK" role="2Oq$k0">
                  <node concept="2OqwBi" id="vM" role="2Oq$k0">
                    <node concept="2OqwBi" id="vO" role="2Oq$k0">
                      <node concept="2OqwBi" id="vQ" role="2Oq$k0">
                        <node concept="37vLTw" id="vS" role="2Oq$k0">
                          <ref role="3cqZAo" node="vi" resolve="b" />
                        </node>
                        <node concept="liA8E" id="vT" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="vU" role="37wK5m">
                            <property role="Xl_RC" value="when" />
                          </node>
                          <node concept="1adDum" id="vV" role="37wK5m">
                            <property role="1adDun" value="0x2c6710e6fc99ed0aL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="vR" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="vW" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="vX" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="vY" role="37wK5m">
                          <property role="1adDun" value="0x2c6710e6fcb3e16fL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="vP" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="vZ" role="37wK5m">
                        <property role="3clFbU" value="false" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="vN" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="w0" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="vL" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="w1" role="37wK5m">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="vJ" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="w2" role="37wK5m">
                  <property role="Xl_RC" value="3199544644536495370" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="vH" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="vh" role="3cqZAp">
          <node concept="2OqwBi" id="w3" role="3cqZAk">
            <node concept="37vLTw" id="w4" role="2Oq$k0">
              <ref role="3cqZAo" node="vi" resolve="b" />
            </node>
            <node concept="liA8E" id="w5" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="va" role="1B3o_S" />
      <node concept="3uibUv" id="vb" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lO" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForSensor" />
      <node concept="3clFbS" id="w6" role="3clF47">
        <node concept="3cpWs8" id="w9" role="3cqZAp">
          <node concept="3cpWsn" id="wf" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="wg" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="wh" role="33vP2m">
              <node concept="1pGfFk" id="wi" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="wj" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="wk" role="37wK5m">
                  <property role="Xl_RC" value="Sensor" />
                </node>
                <node concept="1adDum" id="wl" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="wm" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="wn" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a57431b3L" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wa" role="3cqZAp">
          <node concept="2OqwBi" id="wo" role="3clFbG">
            <node concept="37vLTw" id="wp" role="2Oq$k0">
              <ref role="3cqZAo" node="wf" resolve="b" />
            </node>
            <node concept="liA8E" id="wq" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="wr" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="ws" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="wt" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wb" role="3cqZAp">
          <node concept="2OqwBi" id="wu" role="3clFbG">
            <node concept="37vLTw" id="wv" role="2Oq$k0">
              <ref role="3cqZAo" node="wf" resolve="b" />
            </node>
            <node concept="liA8E" id="ww" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.super_(java.lang.String,long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="super_" />
              <node concept="Xl_RD" id="wx" role="37wK5m">
                <property role="Xl_RC" value="ArduinoML.structure.Brick" />
              </node>
              <node concept="1adDum" id="wy" role="37wK5m">
                <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
              </node>
              <node concept="1adDum" id="wz" role="37wK5m">
                <property role="1adDun" value="0x9882c5f0dd3df659L" />
              </node>
              <node concept="1adDum" id="w$" role="37wK5m">
                <property role="1adDun" value="0x6598d354a57431f2L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wc" role="3cqZAp">
          <node concept="2OqwBi" id="w_" role="3clFbG">
            <node concept="37vLTw" id="wA" role="2Oq$k0">
              <ref role="3cqZAo" node="wf" resolve="b" />
            </node>
            <node concept="liA8E" id="wB" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="wC" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797310387" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wd" role="3cqZAp">
          <node concept="2OqwBi" id="wD" role="3clFbG">
            <node concept="37vLTw" id="wE" role="2Oq$k0">
              <ref role="3cqZAo" node="wf" resolve="b" />
            </node>
            <node concept="liA8E" id="wF" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="wG" role="37wK5m">
                <property role="Xl_RC" value="analog" />
              </node>
              <node concept="1adDum" id="wH" role="37wK5m">
                <property role="1adDun" value="0x2c6710e6fca9dcf7L" />
              </node>
              <node concept="Xl_RD" id="wI" role="37wK5m">
                <property role="Xl_RC" value="3199544644537539831" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="we" role="3cqZAp">
          <node concept="2OqwBi" id="wJ" role="3cqZAk">
            <node concept="37vLTw" id="wK" role="2Oq$k0">
              <ref role="3cqZAo" node="wf" resolve="b" />
            </node>
            <node concept="liA8E" id="wL" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="w7" role="1B3o_S" />
      <node concept="3uibUv" id="w8" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lP" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForState" />
      <node concept="3clFbS" id="wM" role="3clF47">
        <node concept="3cpWs8" id="wP" role="3cqZAp">
          <node concept="3cpWsn" id="wX" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="wY" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="wZ" role="33vP2m">
              <node concept="1pGfFk" id="x0" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="x1" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="x2" role="37wK5m">
                  <property role="Xl_RC" value="State" />
                </node>
                <node concept="1adDum" id="x3" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="x4" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="x5" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a573895cL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wQ" role="3cqZAp">
          <node concept="2OqwBi" id="x6" role="3clFbG">
            <node concept="37vLTw" id="x7" role="2Oq$k0">
              <ref role="3cqZAo" node="wX" resolve="b" />
            </node>
            <node concept="liA8E" id="x8" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="x9" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="xa" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="xb" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wR" role="3cqZAp">
          <node concept="2OqwBi" id="xc" role="3clFbG">
            <node concept="37vLTw" id="xd" role="2Oq$k0">
              <ref role="3cqZAo" node="wX" resolve="b" />
            </node>
            <node concept="liA8E" id="xe" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.parent(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="parent" />
              <node concept="1adDum" id="xf" role="37wK5m">
                <property role="1adDun" value="0xceab519525ea4f22L" />
              </node>
              <node concept="1adDum" id="xg" role="37wK5m">
                <property role="1adDun" value="0x9b92103b95ca8c0cL" />
              </node>
              <node concept="1adDum" id="xh" role="37wK5m">
                <property role="1adDun" value="0x110396eaaa4L" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wS" role="3cqZAp">
          <node concept="2OqwBi" id="xi" role="3clFbG">
            <node concept="37vLTw" id="xj" role="2Oq$k0">
              <ref role="3cqZAo" node="wX" resolve="b" />
            </node>
            <node concept="liA8E" id="xk" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="xl" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797267292" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wT" role="3cqZAp">
          <node concept="2OqwBi" id="xm" role="3clFbG">
            <node concept="37vLTw" id="xn" role="2Oq$k0">
              <ref role="3cqZAo" node="wX" resolve="b" />
            </node>
            <node concept="liA8E" id="xo" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.prop(java.lang.String,long,java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="prop" />
              <node concept="Xl_RD" id="xp" role="37wK5m">
                <property role="Xl_RC" value="isInitial" />
              </node>
              <node concept="1adDum" id="xq" role="37wK5m">
                <property role="1adDun" value="0x6598d354a5741fc6L" />
              </node>
              <node concept="Xl_RD" id="xr" role="37wK5m">
                <property role="Xl_RC" value="7320833554797305798" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wU" role="3cqZAp">
          <node concept="2OqwBi" id="xs" role="3clFbG">
            <node concept="2OqwBi" id="xt" role="2Oq$k0">
              <node concept="2OqwBi" id="xv" role="2Oq$k0">
                <node concept="2OqwBi" id="xx" role="2Oq$k0">
                  <node concept="2OqwBi" id="xz" role="2Oq$k0">
                    <node concept="2OqwBi" id="x_" role="2Oq$k0">
                      <node concept="2OqwBi" id="xB" role="2Oq$k0">
                        <node concept="37vLTw" id="xD" role="2Oq$k0">
                          <ref role="3cqZAo" node="wX" resolve="b" />
                        </node>
                        <node concept="liA8E" id="xE" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="xF" role="37wK5m">
                            <property role="Xl_RC" value="actions" />
                          </node>
                          <node concept="1adDum" id="xG" role="37wK5m">
                            <property role="1adDun" value="0x6598d354a5741fc9L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="xC" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="xH" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="xI" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="xJ" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a57389a0L" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="xA" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="xK" role="37wK5m">
                        <property role="3clFbU" value="false" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="x$" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="xL" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="xy" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="xM" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="xw" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="xN" role="37wK5m">
                  <property role="Xl_RC" value="7320833554797305801" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="xu" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="wV" role="3cqZAp">
          <node concept="2OqwBi" id="xO" role="3clFbG">
            <node concept="2OqwBi" id="xP" role="2Oq$k0">
              <node concept="2OqwBi" id="xR" role="2Oq$k0">
                <node concept="2OqwBi" id="xT" role="2Oq$k0">
                  <node concept="2OqwBi" id="xV" role="2Oq$k0">
                    <node concept="2OqwBi" id="xX" role="2Oq$k0">
                      <node concept="2OqwBi" id="xZ" role="2Oq$k0">
                        <node concept="37vLTw" id="y1" role="2Oq$k0">
                          <ref role="3cqZAo" node="wX" resolve="b" />
                        </node>
                        <node concept="liA8E" id="y2" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="y3" role="37wK5m">
                            <property role="Xl_RC" value="transitions" />
                          </node>
                          <node concept="1adDum" id="y4" role="37wK5m">
                            <property role="1adDun" value="0x7a50f50182385313L" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="y0" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="y5" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="y6" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="y7" role="37wK5m">
                          <property role="1adDun" value="0x6598d354a574322dL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="xY" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="y8" role="37wK5m">
                        <property role="3clFbU" value="true" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="xW" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="y9" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="xU" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="ya" role="37wK5m">
                    <property role="3clFbU" value="true" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="xS" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="yb" role="37wK5m">
                  <property role="Xl_RC" value="8813813857592562451" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="xQ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="wW" role="3cqZAp">
          <node concept="2OqwBi" id="yc" role="3cqZAk">
            <node concept="37vLTw" id="yd" role="2Oq$k0">
              <ref role="3cqZAo" node="wX" resolve="b" />
            </node>
            <node concept="liA8E" id="ye" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="wN" role="1B3o_S" />
      <node concept="3uibUv" id="wO" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
    <node concept="2YIFZL" id="lQ" role="jymVt">
      <property role="od$2w" value="false" />
      <property role="DiZV1" value="false" />
      <property role="2aFKle" value="false" />
      <property role="TrG5h" value="createDescriptorForTransition" />
      <node concept="3clFbS" id="yf" role="3clF47">
        <node concept="3cpWs8" id="yi" role="3cqZAp">
          <node concept="3cpWsn" id="yo" role="3cpWs9">
            <property role="TrG5h" value="b" />
            <node concept="3uibUv" id="yp" role="1tU5fm">
              <ref role="3uigEE" to="bzg8:~ConceptDescriptorBuilder2" resolve="ConceptDescriptorBuilder2" />
            </node>
            <node concept="2ShNRf" id="yq" role="33vP2m">
              <node concept="1pGfFk" id="yr" role="2ShVmc">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.&lt;init&gt;(java.lang.String,java.lang.String,long,long,long)" resolve="ConceptDescriptorBuilder2" />
                <node concept="Xl_RD" id="ys" role="37wK5m">
                  <property role="Xl_RC" value="ArduinoML" />
                </node>
                <node concept="Xl_RD" id="yt" role="37wK5m">
                  <property role="Xl_RC" value="Transition" />
                </node>
                <node concept="1adDum" id="yu" role="37wK5m">
                  <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                </node>
                <node concept="1adDum" id="yv" role="37wK5m">
                  <property role="1adDun" value="0x9882c5f0dd3df659L" />
                </node>
                <node concept="1adDum" id="yw" role="37wK5m">
                  <property role="1adDun" value="0x6598d354a574322dL" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="yj" role="3cqZAp">
          <node concept="2OqwBi" id="yx" role="3clFbG">
            <node concept="37vLTw" id="yy" role="2Oq$k0">
              <ref role="3cqZAo" node="yo" resolve="b" />
            </node>
            <node concept="liA8E" id="yz" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.class_(boolean,boolean,boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="class_" />
              <node concept="3clFbT" id="y$" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="y_" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
              <node concept="3clFbT" id="yA" role="37wK5m">
                <property role="3clFbU" value="false" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="yk" role="3cqZAp">
          <node concept="2OqwBi" id="yB" role="3clFbG">
            <node concept="37vLTw" id="yC" role="2Oq$k0">
              <ref role="3cqZAo" node="yo" resolve="b" />
            </node>
            <node concept="liA8E" id="yD" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="origin" />
              <node concept="Xl_RD" id="yE" role="37wK5m">
                <property role="Xl_RC" value="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)/7320833554797310509" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="yl" role="3cqZAp">
          <node concept="2OqwBi" id="yF" role="3clFbG">
            <node concept="2OqwBi" id="yG" role="2Oq$k0">
              <node concept="2OqwBi" id="yI" role="2Oq$k0">
                <node concept="2OqwBi" id="yK" role="2Oq$k0">
                  <node concept="2OqwBi" id="yM" role="2Oq$k0">
                    <node concept="37vLTw" id="yO" role="2Oq$k0">
                      <ref role="3cqZAo" node="yo" resolve="b" />
                    </node>
                    <node concept="liA8E" id="yP" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.associate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="associate" />
                      <node concept="Xl_RD" id="yQ" role="37wK5m">
                        <property role="Xl_RC" value="to" />
                      </node>
                      <node concept="1adDum" id="yR" role="37wK5m">
                        <property role="1adDun" value="0x6598d354a5743269L" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="yN" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="target" />
                    <node concept="1adDum" id="yS" role="37wK5m">
                      <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                    </node>
                    <node concept="1adDum" id="yT" role="37wK5m">
                      <property role="1adDun" value="0x9882c5f0dd3df659L" />
                    </node>
                    <node concept="1adDum" id="yU" role="37wK5m">
                      <property role="1adDun" value="0x6598d354a573895cL" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="yL" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="optional" />
                  <node concept="3clFbT" id="yV" role="37wK5m">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="yJ" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AssociationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="yW" role="37wK5m">
                  <property role="Xl_RC" value="7320833554797310569" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="yH" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AssociationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3clFbF" id="ym" role="3cqZAp">
          <node concept="2OqwBi" id="yX" role="3clFbG">
            <node concept="2OqwBi" id="yY" role="2Oq$k0">
              <node concept="2OqwBi" id="z0" role="2Oq$k0">
                <node concept="2OqwBi" id="z2" role="2Oq$k0">
                  <node concept="2OqwBi" id="z4" role="2Oq$k0">
                    <node concept="2OqwBi" id="z6" role="2Oq$k0">
                      <node concept="2OqwBi" id="z8" role="2Oq$k0">
                        <node concept="37vLTw" id="za" role="2Oq$k0">
                          <ref role="3cqZAo" node="yo" resolve="b" />
                        </node>
                        <node concept="liA8E" id="zb" role="2OqNvi">
                          <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.aggregate(java.lang.String,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="aggregate" />
                          <node concept="Xl_RD" id="zc" role="37wK5m">
                            <property role="Xl_RC" value="when" />
                          </node>
                          <node concept="1adDum" id="zd" role="37wK5m">
                            <property role="1adDun" value="0x6c655ac4f1eb8d2aL" />
                          </node>
                        </node>
                      </node>
                      <node concept="liA8E" id="z9" role="2OqNvi">
                        <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.target(long,long,long):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="target" />
                        <node concept="1adDum" id="ze" role="37wK5m">
                          <property role="1adDun" value="0x4ec5b2f08f3a4941L" />
                        </node>
                        <node concept="1adDum" id="zf" role="37wK5m">
                          <property role="1adDun" value="0x9882c5f0dd3df659L" />
                        </node>
                        <node concept="1adDum" id="zg" role="37wK5m">
                          <property role="1adDun" value="0x2c6710e6fcb3e16fL" />
                        </node>
                      </node>
                    </node>
                    <node concept="liA8E" id="z7" role="2OqNvi">
                      <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.optional(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="optional" />
                      <node concept="3clFbT" id="zh" role="37wK5m">
                        <property role="3clFbU" value="false" />
                      </node>
                    </node>
                  </node>
                  <node concept="liA8E" id="z5" role="2OqNvi">
                    <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.ordered(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="ordered" />
                    <node concept="3clFbT" id="zi" role="37wK5m">
                      <property role="3clFbU" value="true" />
                    </node>
                  </node>
                </node>
                <node concept="liA8E" id="z3" role="2OqNvi">
                  <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.multiple(boolean):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="multiple" />
                  <node concept="3clFbT" id="zj" role="37wK5m">
                    <property role="3clFbU" value="false" />
                  </node>
                </node>
              </node>
              <node concept="liA8E" id="z1" role="2OqNvi">
                <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.origin(java.lang.String):jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2$AggregationLinkBuilder" resolve="origin" />
                <node concept="Xl_RD" id="zk" role="37wK5m">
                  <property role="Xl_RC" value="7810748930662829354" />
                </node>
              </node>
            </node>
            <node concept="liA8E" id="yZ" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2$AggregationLinkBuilder.done():jetbrains.mps.smodel.runtime.impl.ConceptDescriptorBuilder2" resolve="done" />
            </node>
          </node>
        </node>
        <node concept="3cpWs6" id="yn" role="3cqZAp">
          <node concept="2OqwBi" id="zl" role="3cqZAk">
            <node concept="37vLTw" id="zm" role="2Oq$k0">
              <ref role="3cqZAo" node="yo" resolve="b" />
            </node>
            <node concept="liA8E" id="zn" role="2OqNvi">
              <ref role="37wK5l" to="bzg8:~ConceptDescriptorBuilder2.create():jetbrains.mps.smodel.runtime.ConceptDescriptor" resolve="create" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3Tm6S6" id="yg" role="1B3o_S" />
      <node concept="3uibUv" id="yh" role="3clF45">
        <ref role="3uigEE" to="ze1i:~ConceptDescriptor" resolve="ConceptDescriptor" />
      </node>
    </node>
  </node>
</model>

