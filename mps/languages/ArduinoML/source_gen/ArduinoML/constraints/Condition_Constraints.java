package ArduinoML.constraints;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.base.BaseConstraintsDescriptor;
import jetbrains.mps.smodel.adapter.structure.MetaAdapterFactory;
import java.util.Map;
import org.jetbrains.mps.openapi.language.SProperty;
import jetbrains.mps.smodel.runtime.PropertyConstraintsDescriptor;
import java.util.HashMap;
import jetbrains.mps.smodel.runtime.base.BasePropertyConstraintsDescriptor;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;
import org.jetbrains.mps.openapi.model.SNode;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SPropertyOperations;
import jetbrains.mps.lang.smodel.generator.smodelAdapter.SLinkOperations;

public class Condition_Constraints extends BaseConstraintsDescriptor {
  public Condition_Constraints() {
    super(MetaAdapterFactory.getConcept(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574326fL, "ArduinoML.structure.Condition"));
  }

  @Override
  protected Map<SProperty, PropertyConstraintsDescriptor> getSpecifiedProperties() {
    Map<SProperty, PropertyConstraintsDescriptor> properties = new HashMap<SProperty, PropertyConstraintsDescriptor>();
    properties.put(MetaAdapterFactory.getProperty(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574326fL, 0x6c655ac4f1eb8d17L, "signal"), new BasePropertyConstraintsDescriptor(MetaIdFactory.propId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574326fL, 0x6c655ac4f1eb8d17L), this) {
      @Override
      public boolean hasOwnValidator() {
        return true;
      }
      @Override
      public boolean validateValue(SNode node, String propertyValue) {
        String propertyName = "signal";
        return !((SPropertyOperations.getBoolean(SLinkOperations.getTarget(node, MetaAdapterFactory.getReferenceLink(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574326fL, 0x6c655ac4f1eb8d19L, "sensor")), MetaAdapterFactory.getProperty(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a57431b3L, 0x2c6710e6fca9dcf7L, "analog"))));
      }
    });
    return properties;
  }
}
