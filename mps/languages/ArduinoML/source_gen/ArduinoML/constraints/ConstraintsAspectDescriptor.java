package ArduinoML.constraints;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.BaseConstraintsAspectDescriptor;
import jetbrains.mps.smodel.runtime.ConstraintsDescriptor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.base.BaseConstraintsDescriptor;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public class ConstraintsAspectDescriptor extends BaseConstraintsAspectDescriptor {
  public ConstraintsAspectDescriptor() {
  }

  @Override
  public ConstraintsDescriptor getConstraints(SAbstractConcept concept) {
    SAbstractConcept cncpt_a0c = concept;
    switch (index_2qnle6_a0c.index(cncpt_a0c)) {
      case 0:
        return new Brick_Constraints();
      case 1:
        return new Condition_Constraints();
      case 2:
        return new ConditionAnalogic_Constraints();
      default:
    }
    return new BaseConstraintsDescriptor(concept);
  }
  private static final ConceptSwitchIndex index_2qnle6_a0c = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a57431f2L), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574326fL), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x2c6710e6fcb44fe0L)).seal();
}
