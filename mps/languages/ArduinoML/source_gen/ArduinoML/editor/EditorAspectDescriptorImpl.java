package ArduinoML.editor;

/*Generated by MPS */

import jetbrains.mps.nodeEditor.EditorAspectDescriptorBase;
import org.jetbrains.annotations.NotNull;
import java.util.Collection;
import jetbrains.mps.openapi.editor.descriptor.ConceptEditor;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import java.util.Collections;
import jetbrains.mps.openapi.editor.descriptor.SubstituteMenu;
import jetbrains.mps.lang.smodel.ConceptSwitchIndex;
import jetbrains.mps.lang.smodel.ConceptSwitchIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.MetaIdFactory;

public class EditorAspectDescriptorImpl extends EditorAspectDescriptorBase {
  @NotNull
  public Collection<ConceptEditor> getDeclaredEditors(SAbstractConcept concept) {
    SAbstractConcept cncpt_a0a = ((SAbstractConcept) concept);
    switch (index_xbvbvu_a0a.index(cncpt_a0a)) {
      case 0:
        return Collections.<ConceptEditor>singletonList(new Action_Editor());
      case 1:
        return Collections.<ConceptEditor>singletonList(new Actuator_Editor());
      case 2:
        return Collections.<ConceptEditor>singletonList(new App_Editor());
      case 3:
        return Collections.<ConceptEditor>singletonList(new Condition_Editor());
      case 4:
        return Collections.<ConceptEditor>singletonList(new ConditionAnalogic_Editor());
      case 5:
        return Collections.<ConceptEditor>singletonList(new LogicOp_Editor());
      case 6:
        return Collections.<ConceptEditor>singletonList(new Sensor_Editor());
      case 7:
        return Collections.<ConceptEditor>singletonList(new State_Editor());
      case 8:
        return Collections.<ConceptEditor>singletonList(new Transition_Editor());
      default:
    }
    return Collections.<ConceptEditor>emptyList();
  }


  @NotNull
  @Override
  public Collection<SubstituteMenu> getDeclaredDefaultSubstituteMenus(SAbstractConcept concept) {
    SAbstractConcept cncpt_a0d = concept;
    switch (index_xbvbvu_a0d.index(cncpt_a0d)) {
      case 0:
        return Collections.<SubstituteMenu>singletonList(new Action_SubstituteMenu());
      case 1:
        return Collections.<SubstituteMenu>singletonList(new Condition_SubstituteMenu());
      case 2:
        return Collections.<SubstituteMenu>singletonList(new Transition_SubstituteMenu());
      default:
    }
    return Collections.<SubstituteMenu>emptyList();
  }

  private static final ConceptSwitchIndex index_xbvbvu_a0a = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a57389a0L), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a573893dL), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a573891eL), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574326fL), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x2c6710e6fcb44fe0L), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x2c6710e6fc99ed09L), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a57431b3L), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a573895cL), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574322dL)).seal();
  private static final ConceptSwitchIndex index_xbvbvu_a0d = new ConceptSwitchIndexBuilder().put(MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a57389a0L), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574326fL), MetaIdFactory.conceptId(0x4ec5b2f08f3a4941L, 0x9882c5f0dd3df659L, 0x6598d354a574322dL)).seal();
}
