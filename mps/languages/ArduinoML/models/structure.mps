<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="6" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1082978164219" name="jetbrains.mps.lang.structure.structure.EnumerationDataTypeDeclaration" flags="ng" index="AxPO7">
        <property id="1212080844762" name="hasNoDefaultMember" index="PDuV0" />
        <reference id="1083171729157" name="memberDataType" index="M4eZT" />
        <child id="1083172003582" name="member" index="M5hS2" />
      </concept>
      <concept id="1083171877298" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ig" index="M4N5e">
        <property id="1083923523172" name="externalValue" index="1uS6qo" />
        <property id="1083923523171" name="internalValue" index="1uS6qv" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ" />
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="6moOPi_sS$u">
    <property role="EcuMT" value="7320833554797267230" />
    <property role="TrG5h" value="App" />
    <property role="19KtqR" value="true" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6moOPi_sS$U" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="6moOPi_t1Zn" role="1TKVEi">
      <property role="IQ2ns" value="7320833554797305815" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="actuators" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="6moOPi_sS$X" resolve="Actuator" />
    </node>
    <node concept="1TJgyj" id="6moOPi_t37I" role="1TKVEi">
      <property role="IQ2ns" value="7320833554797310446" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="sensors" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6moOPi_t36N" resolve="Sensor" />
    </node>
    <node concept="1TJgyj" id="6moOPi_t1Zk" role="1TKVEi">
      <property role="IQ2ns" value="7320833554797305812" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="states" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="6moOPi_sS_s" resolve="State" />
    </node>
  </node>
  <node concept="1TIwiD" id="6moOPi_sS$X">
    <property role="EcuMT" value="7320833554797267261" />
    <property role="TrG5h" value="Actuator" />
    <property role="34LRSv" value="actuator" />
    <ref role="1TJDcQ" node="6moOPi_t37M" resolve="Brick" />
    <node concept="1TJgyi" id="2LB4erWIbXC" role="1TKVEl">
      <property role="IQ2nx" value="3199544644538515304" />
      <property role="TrG5h" value="analog" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="6moOPi_sS_s">
    <property role="EcuMT" value="7320833554797267292" />
    <property role="TrG5h" value="State" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6moOPi_sS_S" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="6moOPi_t1Z6" role="1TKVEl">
      <property role="IQ2nx" value="7320833554797305798" />
      <property role="TrG5h" value="isInitial" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
    <node concept="1TJgyj" id="6moOPi_t1Z9" role="1TKVEi">
      <property role="IQ2ns" value="7320833554797305801" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="actions" />
      <property role="20lbJX" value="1..n" />
      <ref role="20lvS9" node="6moOPi_sSAw" resolve="Action" />
    </node>
    <node concept="1TJgyj" id="7DgXg62e5cj" role="1TKVEi">
      <property role="IQ2ns" value="8813813857592562451" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="transitions" />
      <property role="20lbJX" value="0..n" />
      <ref role="20lvS9" node="6moOPi_t38H" resolve="Transition" />
    </node>
  </node>
  <node concept="AxPO7" id="6moOPi_sS_V">
    <property role="TrG5h" value="SIGNAL" />
    <property role="PDuV0" value="true" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <node concept="M4N5e" id="6moOPi_sS_W" role="M5hS2">
      <property role="1uS6qv" value="HIGH" />
      <property role="1uS6qo" value="high" />
    </node>
    <node concept="M4N5e" id="6moOPi_sSAo" role="M5hS2">
      <property role="1uS6qo" value="low" />
      <property role="1uS6qv" value="LOW" />
    </node>
  </node>
  <node concept="1TIwiD" id="6moOPi_sSAw">
    <property role="EcuMT" value="7320833554797267360" />
    <property role="TrG5h" value="Action" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="6moOPi_sSAW" role="1TKVEl">
      <property role="IQ2nx" value="7320833554797267388" />
      <property role="TrG5h" value="signal" />
      <ref role="AX2Wp" node="6moOPi_sS_V" resolve="SIGNAL" />
    </node>
    <node concept="1TJgyj" id="6moOPi_t2vq" role="1TKVEi">
      <property role="IQ2ns" value="7320833554797307866" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="target" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6moOPi_sS$X" resolve="Actuator" />
    </node>
  </node>
  <node concept="1TIwiD" id="6moOPi_t36N">
    <property role="EcuMT" value="7320833554797310387" />
    <property role="TrG5h" value="Sensor" />
    <ref role="1TJDcQ" node="6moOPi_t37M" resolve="Brick" />
    <node concept="1TJgyi" id="2LB4erWEtNR" role="1TKVEl">
      <property role="IQ2nx" value="3199544644537539831" />
      <property role="TrG5h" value="analog" />
      <ref role="AX2Wp" to="tpck:fKAQMTB" resolve="boolean" />
    </node>
  </node>
  <node concept="1TIwiD" id="6moOPi_t37M">
    <property role="EcuMT" value="7320833554797310450" />
    <property role="TrG5h" value="Brick" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="6moOPi_t38D" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="6moOPi_t38F" role="1TKVEl">
      <property role="IQ2nx" value="7320833554797310507" />
      <property role="TrG5h" value="pin" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="1TIwiD" id="6moOPi_t38H">
    <property role="EcuMT" value="7320833554797310509" />
    <property role="TrG5h" value="Transition" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="6moOPi_t39D" role="1TKVEi">
      <property role="IQ2ns" value="7320833554797310569" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="to" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6moOPi_sS_s" resolve="State" />
    </node>
    <node concept="1TJgyj" id="6L_mGjLUSOE" role="1TKVEi">
      <property role="IQ2ns" value="7810748930662829354" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="when" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2LB4erWGY5J" resolve="ICondition" />
    </node>
  </node>
  <node concept="1TIwiD" id="6moOPi_t39J">
    <property role="EcuMT" value="7320833554797310575" />
    <property role="TrG5h" value="Condition" />
    <node concept="1TJgyi" id="6L_mGjLUSOn" role="1TKVEl">
      <property role="IQ2nx" value="7810748930662829335" />
      <property role="TrG5h" value="signal" />
      <ref role="AX2Wp" node="6moOPi_sS_V" resolve="SIGNAL" />
    </node>
    <node concept="1TJgyj" id="6L_mGjLUSOp" role="1TKVEi">
      <property role="IQ2ns" value="7810748930662829337" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="sensor" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="6moOPi_t36N" resolve="Sensor" />
    </node>
    <node concept="1TJgyj" id="6L_mGjLUSOr" role="1TKVEi">
      <property role="IQ2ns" value="7810748930662829339" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="logicOp" />
      <ref role="20lvS9" node="2LB4erWAuO9" resolve="LogicOp" />
    </node>
    <node concept="PrWs8" id="2LB4erWGY5K" role="PzmwI">
      <ref role="PrY4T" node="2LB4erWGY5J" resolve="ICondition" />
    </node>
  </node>
  <node concept="1TIwiD" id="2LB4erWAuO9">
    <property role="EcuMT" value="3199544644536495369" />
    <property role="TrG5h" value="LogicOp" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="2LB4erWAuOa" role="1TKVEi">
      <property role="IQ2ns" value="3199544644536495370" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="when" />
      <property role="20lbJX" value="1" />
      <ref role="20lvS9" node="2LB4erWGY5J" resolve="ICondition" />
    </node>
    <node concept="1TJgyi" id="2LB4erWB9R2" role="1TKVEl">
      <property role="IQ2nx" value="3199544644536671682" />
      <property role="TrG5h" value="operator" />
      <ref role="AX2Wp" node="2LB4erWB9QV" resolve="COND" />
    </node>
  </node>
  <node concept="AxPO7" id="2LB4erWB9QV">
    <property role="TrG5h" value="COND" />
    <property role="PDuV0" value="true" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <node concept="M4N5e" id="2LB4erWB9QW" role="M5hS2">
      <property role="1uS6qo" value="and" />
      <property role="1uS6qv" value="&amp;&amp;" />
    </node>
    <node concept="M4N5e" id="2LB4erWB9QX" role="M5hS2">
      <property role="1uS6qo" value="or" />
      <property role="1uS6qv" value="||" />
    </node>
  </node>
  <node concept="AxPO7" id="2LB4erWEtNz">
    <property role="TrG5h" value="COMP" />
    <property role="PDuV0" value="true" />
    <ref role="M4eZT" to="tpck:fKAOsGN" resolve="string" />
    <node concept="M4N5e" id="2LB4erWEtN$" role="M5hS2">
      <property role="1uS6qv" value="&lt;" />
      <property role="1uS6qo" value="lt" />
    </node>
    <node concept="M4N5e" id="2LB4erWEtN_" role="M5hS2">
      <property role="1uS6qv" value="&gt;" />
      <property role="1uS6qo" value="gt" />
    </node>
    <node concept="M4N5e" id="2LB4erWEtNC" role="M5hS2">
      <property role="1uS6qv" value="&lt;=" />
      <property role="1uS6qo" value="le" />
    </node>
    <node concept="M4N5e" id="2LB4erWEtNG" role="M5hS2">
      <property role="1uS6qv" value="&gt;=" />
      <property role="1uS6qo" value="ge" />
    </node>
    <node concept="M4N5e" id="2LB4erWEtNL" role="M5hS2">
      <property role="1uS6qv" value="==" />
      <property role="1uS6qo" value="eq" />
    </node>
    <node concept="M4N5e" id="2LB4erWFiG0" role="M5hS2">
      <property role="1uS6qo" value="ne" />
      <property role="1uS6qv" value="!=" />
    </node>
  </node>
  <node concept="PlHQZ" id="2LB4erWGY5J">
    <property role="EcuMT" value="3199544644538196335" />
    <property role="TrG5h" value="ICondition" />
  </node>
  <node concept="1TIwiD" id="2LB4erWH4Zw">
    <property role="EcuMT" value="3199544644538224608" />
    <property role="TrG5h" value="ConditionAnalogic" />
    <node concept="PrWs8" id="2LB4erWH4Zx" role="PzmwI">
      <ref role="PrY4T" node="2LB4erWGY5J" resolve="ICondition" />
    </node>
    <node concept="1TJgyi" id="2LB4erWH4Zz" role="1TKVEl">
      <property role="IQ2nx" value="3199544644538224611" />
      <property role="TrG5h" value="num" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="2LB4erWH4Z_" role="1TKVEl">
      <property role="IQ2nx" value="3199544644538224613" />
      <property role="TrG5h" value="comp" />
      <ref role="AX2Wp" node="2LB4erWEtNz" resolve="COMP" />
    </node>
    <node concept="1TJgyj" id="2LB4erWH4ZC" role="1TKVEi">
      <property role="IQ2ns" value="3199544644538224616" />
      <property role="20lmBu" value="aggregation" />
      <property role="20kJfa" value="logicOp" />
      <ref role="20lvS9" node="2LB4erWAuO9" resolve="LogicOp" />
    </node>
    <node concept="1TJgyj" id="2LB4erWH4ZE" role="1TKVEi">
      <property role="IQ2ns" value="3199544644538224618" />
      <property role="20lmBu" value="reference" />
      <property role="20kJfa" value="sensor" />
      <ref role="20lvS9" node="6moOPi_t36N" resolve="Sensor" />
    </node>
  </node>
</model>

