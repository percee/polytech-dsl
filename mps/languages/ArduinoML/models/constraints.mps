<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:bb5dc07f-17d1-41da-822b-c537b11dbe75(ArduinoML.constraints)">
  <persistence version="9" />
  <languages>
    <use id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints" version="4" />
    <devkit ref="00000000-0000-4000-0000-5604ebd4f22c(jetbrains.mps.devkit.aspect.constraints)" />
  </languages>
  <imports>
    <import index="m31e" ref="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068581242878" name="jetbrains.mps.baseLanguage.structure.ReturnStatement" flags="nn" index="3cpWs6">
        <child id="1068581517676" name="expression" index="3cqZAk" />
      </concept>
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
    </language>
    <language id="3f4bc5f5-c6c1-4a28-8b10-c83066ffa4a1" name="jetbrains.mps.lang.constraints">
      <concept id="1147467115080" name="jetbrains.mps.lang.constraints.structure.NodePropertyConstraint" flags="ng" index="EnEH3">
        <reference id="1147467295099" name="applicableProperty" index="EomxK" />
        <child id="1212097481299" name="propertyValidator" index="QCWH9" />
      </concept>
      <concept id="1147468365020" name="jetbrains.mps.lang.constraints.structure.ConstraintsFunctionParameter_node" flags="nn" index="EsrRn" />
      <concept id="1212096972063" name="jetbrains.mps.lang.constraints.structure.ConstraintFunction_PropertyValidator" flags="in" index="QB0g5" />
      <concept id="1213093968558" name="jetbrains.mps.lang.constraints.structure.ConceptConstraints" flags="ng" index="1M2fIO">
        <reference id="1213093996982" name="concept" index="1M2myG" />
        <child id="1213098023997" name="property" index="1MhHOB" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
    </language>
  </registry>
  <node concept="1M2fIO" id="7DgXg62cE88">
    <ref role="1M2myG" to="m31e:6moOPi_t37M" resolve="Brick" />
  </node>
  <node concept="1M2fIO" id="2LB4erWHF0p">
    <ref role="1M2myG" to="m31e:2LB4erWH4Zw" resolve="ConditionAnalogic" />
    <node concept="EnEH3" id="2LB4erWHF0s" role="1MhHOB">
      <ref role="EomxK" to="m31e:2LB4erWH4Z_" resolve="comp" />
      <node concept="QB0g5" id="2LB4erWHF0u" role="QCWH9">
        <node concept="3clFbS" id="2LB4erWHF0v" role="2VODD2">
          <node concept="3cpWs6" id="2LB4erWHHMT" role="3cqZAp">
            <node concept="2OqwBi" id="2LB4erWHJ5C" role="3cqZAk">
              <node concept="2OqwBi" id="2LB4erWHI$X" role="2Oq$k0">
                <node concept="EsrRn" id="2LB4erWHInE" role="2Oq$k0" />
                <node concept="3TrEf2" id="2LB4erWHIKh" role="2OqNvi">
                  <ref role="3Tt5mk" to="m31e:2LB4erWH4ZE" resolve="sensor" />
                </node>
              </node>
              <node concept="3TrcHB" id="2LB4erWHJjW" role="2OqNvi">
                <ref role="3TsBF5" to="m31e:2LB4erWEtNR" resolve="analog" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="1M2fIO" id="2LB4erWHRu0">
    <ref role="1M2myG" to="m31e:6moOPi_t39J" resolve="Condition" />
    <node concept="EnEH3" id="2LB4erWHRu3" role="1MhHOB">
      <ref role="EomxK" to="m31e:6L_mGjLUSOn" resolve="signal" />
      <node concept="QB0g5" id="2LB4erWHRu5" role="QCWH9">
        <node concept="3clFbS" id="2LB4erWHRu6" role="2VODD2">
          <node concept="3cpWs6" id="2LB4erWHR_e" role="3cqZAp">
            <node concept="3fqX7Q" id="2LB4erWI2b2" role="3cqZAk">
              <node concept="1eOMI4" id="2LB4erWI2b4" role="3fr31v">
                <node concept="2OqwBi" id="2LB4erWI3gs" role="1eOMHV">
                  <node concept="2OqwBi" id="2LB4erWI2vF" role="2Oq$k0">
                    <node concept="EsrRn" id="2LB4erWI2iy" role="2Oq$k0" />
                    <node concept="3TrEf2" id="2LB4erWI2Ix" role="2OqNvi">
                      <ref role="3Tt5mk" to="m31e:6L_mGjLUSOp" resolve="sensor" />
                    </node>
                  </node>
                  <node concept="3TrcHB" id="2LB4erWI3$g" role="2OqNvi">
                    <ref role="3TsBF5" to="m31e:2LB4erWEtNR" resolve="analog" />
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

