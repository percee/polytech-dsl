<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:c810b9e6-5ae7-474d-8653-75385bafc114(ArduinoML.textGen)">
  <persistence version="9" />
  <languages>
    <use id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen" version="1" />
    <devkit ref="fa73d85a-ac7f-447b-846c-fcdc41caa600(jetbrains.mps.devkit.aspect.textgen)" />
  </languages>
  <imports>
    <import index="m31e" ref="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="wyt6" ref="6354ebe7-c22a-4a0f-ac54-50b52ab9b065/java:java.lang(JDK/)" implicit="true" />
  </imports>
  <registry>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1137021947720" name="jetbrains.mps.baseLanguage.structure.ConceptFunction" flags="in" index="2VMwT0">
        <child id="1137022507850" name="body" index="2VODD2" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1081236700937" name="jetbrains.mps.baseLanguage.structure.StaticMethodCall" flags="nn" index="2YIFZM">
        <reference id="1144433194310" name="classConcept" index="1Pybhc" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
        <child id="1068499141038" name="actualArgument" index="37wK5m" />
      </concept>
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="b83431fe-5c8f-40bc-8a36-65e25f4dd253" name="jetbrains.mps.lang.textGen">
      <concept id="45307784116571022" name="jetbrains.mps.lang.textGen.structure.FilenameFunction" flags="ig" index="29tfMY" />
      <concept id="8931911391946696733" name="jetbrains.mps.lang.textGen.structure.ExtensionDeclaration" flags="in" index="9MYSb" />
      <concept id="1237305208784" name="jetbrains.mps.lang.textGen.structure.NewLineAppendPart" flags="ng" index="l8MVK" />
      <concept id="1237305334312" name="jetbrains.mps.lang.textGen.structure.NodeAppendPart" flags="ng" index="l9hG8">
        <child id="1237305790512" name="value" index="lb14g" />
      </concept>
      <concept id="1237305491868" name="jetbrains.mps.lang.textGen.structure.CollectionAppendPart" flags="ng" index="l9S2W">
        <property id="1237306003719" name="separator" index="lbP0B" />
        <property id="1237983969951" name="withSeparator" index="XA4eZ" />
        <child id="1237305945551" name="list" index="lbANJ" />
      </concept>
      <concept id="1237305557638" name="jetbrains.mps.lang.textGen.structure.ConstantStringAppendPart" flags="ng" index="la8eA">
        <property id="1237305576108" name="value" index="lacIc" />
      </concept>
      <concept id="1237306079178" name="jetbrains.mps.lang.textGen.structure.AppendOperation" flags="nn" index="lc7rE">
        <child id="1237306115446" name="part" index="lcghm" />
      </concept>
      <concept id="1233670071145" name="jetbrains.mps.lang.textGen.structure.ConceptTextGenDeclaration" flags="ig" index="WtQ9Q">
        <reference id="1233670257997" name="conceptDeclaration" index="WuzLi" />
        <child id="45307784116711884" name="filename" index="29tGrW" />
        <child id="1233749296504" name="textGenBlock" index="11c4hB" />
        <child id="7991274449437422201" name="extension" index="33IsuW" />
      </concept>
      <concept id="1233748055915" name="jetbrains.mps.lang.textGen.structure.NodeParameter" flags="nn" index="117lpO" />
      <concept id="1233749247888" name="jetbrains.mps.lang.textGen.structure.GenerateTextDeclaration" flags="in" index="11bSqf" />
      <concept id="1233920501193" name="jetbrains.mps.lang.textGen.structure.IndentBufferOperation" flags="nn" index="1bpajm" />
      <concept id="1236188139846" name="jetbrains.mps.lang.textGen.structure.WithIndentOperation" flags="nn" index="3izx1p">
        <child id="1236188238861" name="list" index="3izTki" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1172008320231" name="jetbrains.mps.lang.smodel.structure.Node_IsNotNullOperation" flags="nn" index="3x8VRR" />
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1204980550705" name="jetbrains.mps.baseLanguage.collections.structure.VisitAllOperation" flags="nn" index="2es0OD" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
    </language>
  </registry>
  <node concept="WtQ9Q" id="7DgXg62c_Yx">
    <ref role="WuzLi" to="m31e:6moOPi_sS$u" resolve="App" />
    <node concept="29tfMY" id="7DgXg62cA3e" role="29tGrW">
      <node concept="3clFbS" id="7DgXg62cA3f" role="2VODD2">
        <node concept="3clFbF" id="7DgXg62cAbE" role="3cqZAp">
          <node concept="Xl_RD" id="7DgXg62cAbD" role="3clFbG">
            <property role="Xl_RC" value="main" />
          </node>
        </node>
      </node>
    </node>
    <node concept="9MYSb" id="7DgXg62cAki" role="33IsuW">
      <node concept="3clFbS" id="7DgXg62cAkj" role="2VODD2">
        <node concept="3clFbF" id="7DgXg62cAsO" role="3cqZAp">
          <node concept="Xl_RD" id="7DgXg62cAsN" role="3clFbG">
            <property role="Xl_RC" value="c" />
          </node>
        </node>
      </node>
    </node>
    <node concept="11bSqf" id="7DgXg62cADv" role="11c4hB">
      <node concept="3clFbS" id="7DgXg62cADw" role="2VODD2">
        <node concept="lc7rE" id="7DgXg62cAMp" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cIuY" role="lcghm">
            <property role="lacIc" value="#include &lt;avr/io.h&gt;" />
          </node>
          <node concept="l8MVK" id="7DgXg62cIyg" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cI_M" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cIAk" role="lcghm">
            <property role="lacIc" value="#include &lt;util/delay.h&gt;" />
          </node>
          <node concept="l8MVK" id="7DgXg62cIEj" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cIFb" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cIFN" role="lcghm">
            <property role="lacIc" value="#include &lt;Arduino.h&gt;" />
          </node>
          <node concept="l8MVK" id="7DgXg62cIIB" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cIJ_" role="3cqZAp">
          <node concept="l8MVK" id="7DgXg62cIKj" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cILj" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cIM5" role="lcghm">
            <property role="lacIc" value="/** Generating code for application " />
          </node>
          <node concept="l9hG8" id="7DgXg62cIPP" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62cIZ4" role="lb14g">
              <node concept="117lpO" id="7DgXg62cIQL" role="2Oq$k0" />
              <node concept="3TrcHB" id="7DgXg62cJ7d" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62cJcC" role="lcghm">
            <property role="lacIc" value=" **/" />
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62cJjj" role="3cqZAp">
          <node concept="l8MVK" id="7DgXg62cJmA" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62f_d6" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62f_yn" role="lcghm">
            <property role="lacIc" value="void (*currentState)(void);" />
          </node>
          <node concept="l8MVK" id="7DgXg62f_zR" role="lcghm" />
          <node concept="l8MVK" id="7DgXg62f__0" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cJqb" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cJty" role="lcghm">
            <property role="lacIc" value="// Declaring states function headers " />
          </node>
          <node concept="l8MVK" id="7DgXg62cJx3" role="lcghm" />
        </node>
        <node concept="3clFbF" id="7DgXg62cJ$M" role="3cqZAp">
          <node concept="2OqwBi" id="7DgXg62cPHI" role="3clFbG">
            <node concept="2OqwBi" id="7DgXg62cJGN" role="2Oq$k0">
              <node concept="117lpO" id="7DgXg62cJ$K" role="2Oq$k0" />
              <node concept="3Tsc0h" id="7DgXg62cJRg" role="2OqNvi">
                <ref role="3TtcxE" to="m31e:6moOPi_t1Zk" resolve="states" />
              </node>
            </node>
            <node concept="2es0OD" id="7DgXg62cSha" role="2OqNvi">
              <node concept="1bVj0M" id="7DgXg62cShc" role="23t8la">
                <node concept="3clFbS" id="7DgXg62cShd" role="1bW5cS">
                  <node concept="lc7rE" id="7DgXg62cSn6" role="3cqZAp">
                    <node concept="la8eA" id="7DgXg62cSrB" role="lcghm">
                      <property role="lacIc" value="void state_" />
                    </node>
                    <node concept="l9hG8" id="7DgXg62cSUu" role="lcghm">
                      <node concept="2OqwBi" id="7DgXg62cTcc" role="lb14g">
                        <node concept="37vLTw" id="7DgXg62cSZz" role="2Oq$k0">
                          <ref role="3cqZAo" node="7DgXg62cShe" resolve="it" />
                        </node>
                        <node concept="3TrcHB" id="7DgXg62cTp1" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                    </node>
                    <node concept="la8eA" id="7DgXg62cTAG" role="lcghm">
                      <property role="lacIc" value="();" />
                    </node>
                    <node concept="l8MVK" id="7DgXg62cU1R" role="lcghm" />
                  </node>
                </node>
                <node concept="Rh6nW" id="7DgXg62cShe" role="1bW2Oz">
                  <property role="TrG5h" value="it" />
                  <node concept="2jxLKc" id="7DgXg62cShf" role="1tU5fm" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62cUjO" role="3cqZAp">
          <node concept="l8MVK" id="7DgXg62cUsi" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cU_2" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cUH$" role="lcghm">
            <property role="lacIc" value="// Declaring available actuators " />
          </node>
          <node concept="l8MVK" id="7DgXg62cUKQ" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cUTI" role="3cqZAp">
          <node concept="l9S2W" id="7DgXg62cV2m" role="lcghm">
            <property role="XA4eZ" value="true" />
            <property role="lbP0B" value="\n" />
            <node concept="2OqwBi" id="7DgXg62cV8L" role="lbANJ">
              <node concept="117lpO" id="7DgXg62cV2K" role="2Oq$k0" />
              <node concept="3Tsc0h" id="7DgXg62cVgm" role="2OqNvi">
                <ref role="3TtcxE" to="m31e:6moOPi_t1Zn" resolve="actuators" />
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62dLpC" role="3cqZAp">
          <node concept="l8MVK" id="7DgXg62dLLu" role="lcghm" />
          <node concept="l8MVK" id="7DgXg62dLMb" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62dMan" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62dMyj" role="lcghm">
            <property role="lacIc" value="// Declaring available sensors " />
          </node>
          <node concept="l8MVK" id="7DgXg62dM$S" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62dMXQ" role="3cqZAp">
          <node concept="l9S2W" id="7DgXg62dNlS" role="lcghm">
            <property role="XA4eZ" value="true" />
            <property role="lbP0B" value="\n" />
            <node concept="2OqwBi" id="7DgXg62dNsj" role="lbANJ">
              <node concept="117lpO" id="7DgXg62dNmi" role="2Oq$k0" />
              <node concept="3Tsc0h" id="7DgXg62dNEN" role="2OqNvi">
                <ref role="3TtcxE" to="m31e:6moOPi_t37I" resolve="sensors" />
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62cYmY" role="3cqZAp">
          <node concept="l8MVK" id="7DgXg62cYvR" role="lcghm" />
          <node concept="l8MVK" id="7DgXg62cYw$" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cYDN" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cYMM" role="lcghm">
            <property role="lacIc" value="// Declaring states" />
          </node>
          <node concept="l8MVK" id="7DgXg62g5j$" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62cYXG" role="3cqZAp">
          <node concept="l9S2W" id="7DgXg62cZ6J" role="lcghm">
            <property role="XA4eZ" value="true" />
            <property role="lbP0B" value=" \n \n" />
            <node concept="2OqwBi" id="7DgXg62cZda" role="lbANJ">
              <node concept="117lpO" id="7DgXg62cZ79" role="2Oq$k0" />
              <node concept="3Tsc0h" id="7DgXg62cZkJ" role="2OqNvi">
                <ref role="3TtcxE" to="m31e:6moOPi_t1Zk" resolve="states" />
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62d24Y" role="3cqZAp">
          <node concept="l8MVK" id="7DgXg62d2ei" role="lcghm" />
          <node concept="l8MVK" id="7DgXg62d2eZ" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62d2oD" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62d2y3" role="lcghm">
            <property role="lacIc" value="void setup()" />
          </node>
          <node concept="l8MVK" id="7DgXg62d2$R" role="lcghm" />
          <node concept="la8eA" id="7DgXg62d2_C" role="lcghm">
            <property role="lacIc" value="{" />
          </node>
          <node concept="l8MVK" id="7DgXg62d2AG" role="lcghm" />
        </node>
        <node concept="3izx1p" id="7DgXg62d2K$" role="3cqZAp">
          <node concept="3clFbS" id="7DgXg62d2KA" role="3izTki">
            <node concept="3clFbF" id="7DgXg62d2U6" role="3cqZAp">
              <node concept="2OqwBi" id="7DgXg62d4UJ" role="3clFbG">
                <node concept="2OqwBi" id="7DgXg62d326" role="2Oq$k0">
                  <node concept="117lpO" id="7DgXg62d2U5" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="7DgXg62d3gA" role="2OqNvi">
                    <ref role="3TtcxE" to="m31e:6moOPi_t1Zn" resolve="actuators" />
                  </node>
                </node>
                <node concept="2es0OD" id="7DgXg62d6pa" role="2OqNvi">
                  <node concept="1bVj0M" id="7DgXg62d6pc" role="23t8la">
                    <node concept="3clFbS" id="7DgXg62d6pd" role="1bW5cS">
                      <node concept="1bpajm" id="7DgXg62d6$D" role="3cqZAp" />
                      <node concept="lc7rE" id="7DgXg62d6Ll" role="3cqZAp">
                        <node concept="la8eA" id="7DgXg62d6PO" role="lcghm">
                          <property role="lacIc" value="pinMode(" />
                        </node>
                        <node concept="l9hG8" id="7DgXg62d7bt" role="lcghm">
                          <node concept="2OqwBi" id="7DgXg62d7tE" role="lb14g">
                            <node concept="37vLTw" id="7DgXg62d7gs" role="2Oq$k0">
                              <ref role="3cqZAo" node="7DgXg62d6pe" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="7DgXg62d7F_" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                        <node concept="la8eA" id="7DgXg62d7Tm" role="lcghm">
                          <property role="lacIc" value=", " />
                        </node>
                        <node concept="la8eA" id="7DgXg62d8kH" role="lcghm">
                          <property role="lacIc" value="OUTPUT);" />
                        </node>
                        <node concept="l8MVK" id="7DgXg62d9hD" role="lcghm" />
                      </node>
                    </node>
                    <node concept="Rh6nW" id="7DgXg62d6pe" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="7DgXg62d6pf" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="3clFbF" id="7DgXg62dCnX" role="3cqZAp">
              <node concept="2OqwBi" id="7DgXg62dENt" role="3clFbG">
                <node concept="2OqwBi" id="7DgXg62dCvG" role="2Oq$k0">
                  <node concept="117lpO" id="7DgXg62dCnV" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="7DgXg62dCND" role="2OqNvi">
                    <ref role="3TtcxE" to="m31e:6moOPi_t37I" resolve="sensors" />
                  </node>
                </node>
                <node concept="2es0OD" id="7DgXg62dHFs" role="2OqNvi">
                  <node concept="1bVj0M" id="7DgXg62dHFu" role="23t8la">
                    <node concept="3clFbS" id="7DgXg62dHFv" role="1bW5cS">
                      <node concept="1bpajm" id="7DgXg62dHLi" role="3cqZAp" />
                      <node concept="lc7rE" id="7DgXg62dHWD" role="3cqZAp">
                        <node concept="la8eA" id="7DgXg62dI18" role="lcghm">
                          <property role="lacIc" value="pinMode(" />
                        </node>
                        <node concept="l9hG8" id="7DgXg62dItF" role="lcghm">
                          <node concept="2OqwBi" id="7DgXg62dIJS" role="lb14g">
                            <node concept="37vLTw" id="7DgXg62dIyE" role="2Oq$k0">
                              <ref role="3cqZAo" node="7DgXg62dHFw" resolve="it" />
                            </node>
                            <node concept="3TrcHB" id="7DgXg62dIXN" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                        <node concept="la8eA" id="7DgXg62dJb$" role="lcghm">
                          <property role="lacIc" value=", " />
                        </node>
                        <node concept="la8eA" id="7DgXg62dJwe" role="lcghm">
                          <property role="lacIc" value="INPUT);" />
                        </node>
                        <node concept="l8MVK" id="7DgXg62dKmt" role="lcghm" />
                      </node>
                    </node>
                    <node concept="Rh6nW" id="7DgXg62dHFw" role="1bW2Oz">
                      <property role="TrG5h" value="it" />
                      <node concept="2jxLKc" id="7DgXg62dHFx" role="1tU5fm" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62d9CD" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62d9Rr" role="lcghm">
            <property role="lacIc" value="}" />
          </node>
          <node concept="l8MVK" id="7DgXg62d9Sn" role="lcghm" />
          <node concept="l8MVK" id="7DgXg62d9T8" role="lcghm" />
        </node>
        <node concept="lc7rE" id="7DgXg62dWeR" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62dWzm" role="lcghm">
            <property role="lacIc" value="void loop(){" />
          </node>
          <node concept="l8MVK" id="7DgXg62dW$Z" role="lcghm" />
        </node>
        <node concept="3izx1p" id="7DgXg62dXJq" role="3cqZAp">
          <node concept="3clFbS" id="7DgXg62dXJs" role="3izTki">
            <node concept="1bpajm" id="7DgXg62fHQU" role="3cqZAp" />
            <node concept="lc7rE" id="7DgXg62fHPO" role="3cqZAp">
              <node concept="la8eA" id="7DgXg62fHRw" role="lcghm">
                <property role="lacIc" value="(*currentState)();" />
              </node>
              <node concept="l8MVK" id="7DgXg62fPy1" role="lcghm" />
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62dWTX" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62dXqk" role="lcghm">
            <property role="lacIc" value="}" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7DgXg62cDRI">
    <ref role="WuzLi" to="m31e:6moOPi_t37M" resolve="Brick" />
    <node concept="11bSqf" id="7DgXg62cDRJ" role="11c4hB">
      <node concept="3clFbS" id="7DgXg62cDRK" role="2VODD2">
        <node concept="lc7rE" id="7DgXg62cDSu" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62cGX4" role="lcghm">
            <property role="lacIc" value="int " />
          </node>
          <node concept="l9hG8" id="7DgXg62cGY0" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62cH7f" role="lb14g">
              <node concept="117lpO" id="7DgXg62cGYW" role="2Oq$k0" />
              <node concept="3TrcHB" id="7DgXg62cHfo" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62cHkN" role="lcghm">
            <property role="lacIc" value=" = " />
          </node>
          <node concept="l9hG8" id="7DgXg62cHqS" role="lcghm">
            <node concept="2YIFZM" id="7DgXg62cHuP" role="lb14g">
              <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
              <ref role="37wK5l" to="wyt6:~String.valueOf(int):java.lang.String" resolve="valueOf" />
              <node concept="2OqwBi" id="7DgXg62cHCr" role="37wK5m">
                <node concept="117lpO" id="7DgXg62cHwg" role="2Oq$k0" />
                <node concept="3TrcHB" id="7DgXg62cHLj" role="2OqNvi">
                  <ref role="3TsBF5" to="m31e:6moOPi_t38F" resolve="pin" />
                </node>
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62cI2Z" role="lcghm">
            <property role="lacIc" value=";" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7DgXg62dY4p">
    <ref role="WuzLi" to="m31e:6moOPi_sS_s" resolve="State" />
    <node concept="11bSqf" id="7DgXg62dY4q" role="11c4hB">
      <node concept="3clFbS" id="7DgXg62dY4r" role="2VODD2">
        <node concept="lc7rE" id="7DgXg62dZcN" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62dZfz" role="lcghm">
            <property role="lacIc" value="void state_" />
          </node>
          <node concept="l9hG8" id="7DgXg62dZhr" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62dZqE" role="lb14g">
              <node concept="117lpO" id="7DgXg62dZin" role="2Oq$k0" />
              <node concept="3TrcHB" id="7DgXg62dZyN" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62dZCe" role="lcghm">
            <property role="lacIc" value="(){" />
          </node>
          <node concept="l8MVK" id="7DgXg62dZIj" role="lcghm" />
        </node>
        <node concept="3izx1p" id="7DgXg62dZOn" role="3cqZAp">
          <node concept="3clFbS" id="7DgXg62dZOp" role="3izTki">
            <node concept="1bpajm" id="7DgXg62dZRF" role="3cqZAp" />
            <node concept="lc7rE" id="7DgXg62e01i" role="3cqZAp">
              <node concept="l9S2W" id="7DgXg62e01I" role="lcghm">
                <property role="XA4eZ" value="true" />
                <property role="lbP0B" value="\n" />
                <node concept="2OqwBi" id="7DgXg62e089" role="lbANJ">
                  <node concept="117lpO" id="7DgXg62e028" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="7DgXg62e0mD" role="2OqNvi">
                    <ref role="3TtcxE" to="m31e:6moOPi_t1Z9" resolve="actions" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="7DgXg62faDv" role="lcghm" />
            </node>
            <node concept="1bpajm" id="7DgXg62fPBT" role="3cqZAp" />
            <node concept="lc7rE" id="7DgXg62faGf" role="3cqZAp">
              <node concept="l9S2W" id="7DgXg62faKb" role="lcghm">
                <property role="XA4eZ" value="true" />
                <property role="lbP0B" value=" \n" />
                <node concept="2OqwBi" id="7DgXg62faQM" role="lbANJ">
                  <node concept="117lpO" id="7DgXg62faKL" role="2Oq$k0" />
                  <node concept="3Tsc0h" id="7DgXg62fb5u" role="2OqNvi">
                    <ref role="3TtcxE" to="m31e:7DgXg62e5cj" resolve="transitions" />
                  </node>
                </node>
              </node>
              <node concept="l8MVK" id="7DgXg62fdqY" role="lcghm" />
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62gdeC" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62gdjq" role="lcghm">
            <property role="lacIc" value=")" />
          </node>
          <node concept="l8MVK" id="7DgXg62gG0u" role="lcghm" />
        </node>
        <node concept="3clFbJ" id="7DgXg62dY5c" role="3cqZAp">
          <node concept="2OqwBi" id="7DgXg62dYH0" role="3clFbw">
            <node concept="117lpO" id="7DgXg62dY5H" role="2Oq$k0" />
            <node concept="3TrcHB" id="7DgXg62dYVx" role="2OqNvi">
              <ref role="3TsBF5" to="m31e:6moOPi_t1Z6" resolve="isInitial" />
            </node>
          </node>
          <node concept="3clFbS" id="7DgXg62dY5e" role="3clFbx">
            <node concept="1bpajm" id="7DgXg62gGkN" role="3cqZAp" />
            <node concept="lc7rE" id="7DgXg62dYXX" role="3cqZAp">
              <node concept="la8eA" id="7DgXg62dYYl" role="lcghm">
                <property role="lacIc" value="currentState = &amp;state_" />
              </node>
              <node concept="l9hG8" id="7DgXg62f_K5" role="lcghm">
                <node concept="2OqwBi" id="7DgXg62f_Ty" role="lb14g">
                  <node concept="117lpO" id="7DgXg62f_Lf" role="2Oq$k0" />
                  <node concept="3TrcHB" id="7DgXg62fA1R" role="2OqNvi">
                    <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                  </node>
                </node>
              </node>
              <node concept="la8eA" id="7DgXg62fA9E" role="lcghm">
                <property role="lacIc" value=";" />
              </node>
              <node concept="l8MVK" id="7DgXg62dZ6t" role="lcghm" />
            </node>
          </node>
        </node>
        <node concept="3clFbH" id="7DgXg62g$zx" role="3cqZAp" />
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7DgXg62e2Sw">
    <ref role="WuzLi" to="m31e:6moOPi_sSAw" resolve="Action" />
    <node concept="11bSqf" id="7DgXg62e2Sx" role="11c4hB">
      <node concept="3clFbS" id="7DgXg62e2Sy" role="2VODD2">
        <node concept="lc7rE" id="7DgXg62e32n" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62e32J" role="lcghm">
            <property role="lacIc" value="digitalWrite(" />
          </node>
          <node concept="l9hG8" id="7DgXg62e355" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62e3EZ" role="lb14g">
              <node concept="2OqwBi" id="7DgXg62e3dy" role="2Oq$k0">
                <node concept="117lpO" id="7DgXg62e361" role="2Oq$k0" />
                <node concept="3TrEf2" id="7DgXg62e3qe" role="2OqNvi">
                  <ref role="3Tt5mk" to="m31e:6moOPi_t2vq" resolve="target" />
                </node>
              </node>
              <node concept="3TrcHB" id="7DgXg62e41x" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62e4aS" role="lcghm">
            <property role="lacIc" value=", " />
          </node>
          <node concept="l9hG8" id="7DgXg62e4kX" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62e4xR" role="lb14g">
              <node concept="117lpO" id="7DgXg62e4qm" role="2Oq$k0" />
              <node concept="3TrcHB" id="7DgXg62e4CO" role="2OqNvi">
                <ref role="3TsBF5" to="m31e:6moOPi_sSAW" resolve="signal" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62e4TC" role="lcghm">
            <property role="lacIc" value=");" />
          </node>
          <node concept="l8MVK" id="7DgXg62e54J" role="lcghm" />
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7DgXg62fdu4">
    <ref role="WuzLi" to="m31e:6moOPi_t38H" resolve="Transition" />
    <node concept="11bSqf" id="7DgXg62fdu5" role="11c4hB">
      <node concept="3clFbS" id="7DgXg62fdu6" role="2VODD2">
        <node concept="lc7rE" id="7DgXg62fk80" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62fkeR" role="lcghm">
            <property role="lacIc" value="if(" />
          </node>
        </node>
        <node concept="lc7rE" id="2LB4erWE2QN" role="3cqZAp">
          <node concept="l9hG8" id="2LB4erWE2V_" role="lcghm">
            <node concept="2OqwBi" id="2LB4erWE33Y" role="lb14g">
              <node concept="117lpO" id="2LB4erWE2Wt" role="2Oq$k0" />
              <node concept="3TrEf2" id="2LB4erWE3aQ" role="2OqNvi">
                <ref role="3Tt5mk" to="m31e:6L_mGjLUSOE" resolve="when" />
              </node>
            </node>
          </node>
        </node>
        <node concept="lc7rE" id="7DgXg62fdv9" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62fdvx" role="lcghm">
            <property role="lacIc" value="){" />
          </node>
          <node concept="l8MVK" id="7DgXg62fq1K" role="lcghm" />
        </node>
        <node concept="1bpajm" id="7DgXg62fozb" role="3cqZAp" />
        <node concept="lc7rE" id="7DgXg62foMd" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62foTP" role="lcghm">
            <property role="lacIc" value="currentState = &amp;state_" />
          </node>
          <node concept="l9hG8" id="7DgXg62foVH" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62fpso" role="lb14g">
              <node concept="2OqwBi" id="7DgXg62fp5l" role="2Oq$k0">
                <node concept="117lpO" id="7DgXg62foXO" role="2Oq$k0" />
                <node concept="3TrEf2" id="7DgXg62fpci" role="2OqNvi">
                  <ref role="3Tt5mk" to="m31e:6moOPi_t39D" resolve="to" />
                </node>
              </node>
              <node concept="3TrcHB" id="7DgXg62fpC2" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62gd2_" role="lcghm">
            <property role="lacIc" value=";" />
          </node>
          <node concept="l8MVK" id="7DgXg62fq6L" role="lcghm" />
        </node>
        <node concept="1bpajm" id="7DgXg62gsWe" role="3cqZAp" />
        <node concept="lc7rE" id="7DgXg62fpQ_" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62fq0O" role="lcghm">
            <property role="lacIc" value="}" />
          </node>
          <node concept="l8MVK" id="7DgXg62fqbM" role="lcghm" />
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="7DgXg62fqcz">
    <ref role="WuzLi" to="m31e:6moOPi_t39J" resolve="Condition" />
    <node concept="11bSqf" id="7DgXg62fqc$" role="11c4hB">
      <node concept="3clFbS" id="7DgXg62fqc_" role="2VODD2">
        <node concept="lc7rE" id="7DgXg62fqdj" role="3cqZAp">
          <node concept="la8eA" id="7DgXg62fqdR" role="lcghm">
            <property role="lacIc" value="digitalRead(" />
          </node>
          <node concept="l9hG8" id="7DgXg62fqhi" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62fXh8" role="lb14g">
              <node concept="2OqwBi" id="7DgXg62fqpV" role="2Oq$k0">
                <node concept="117lpO" id="7DgXg62fqiq" role="2Oq$k0" />
                <node concept="3TrEf2" id="7DgXg62fqx4" role="2OqNvi">
                  <ref role="3Tt5mk" to="m31e:6L_mGjLUSOp" resolve="sensor" />
                </node>
              </node>
              <node concept="3TrcHB" id="7DgXg62fXvY" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="7DgXg62fqGE" role="lcghm">
            <property role="lacIc" value=") == " />
          </node>
          <node concept="l9hG8" id="7DgXg62fqSn" role="lcghm">
            <node concept="2OqwBi" id="7DgXg62fr5u" role="lb14g">
              <node concept="117lpO" id="7DgXg62fqXX" role="2Oq$k0" />
              <node concept="3TrcHB" id="7DgXg62frcB" role="2OqNvi">
                <ref role="3TsBF5" to="m31e:6L_mGjLUSOn" resolve="signal" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="7DgXg62frmw" role="3cqZAp">
          <node concept="3clFbS" id="7DgXg62frmy" role="3clFbx">
            <node concept="lc7rE" id="7DgXg62ft0X" role="3cqZAp">
              <node concept="l9hG8" id="7DgXg62ft3B" role="lcghm">
                <node concept="2OqwBi" id="7DgXg62ftcg" role="lb14g">
                  <node concept="117lpO" id="7DgXg62ft4J" role="2Oq$k0" />
                  <node concept="3TrEf2" id="7DgXg62ftjp" role="2OqNvi">
                    <ref role="3Tt5mk" to="m31e:6L_mGjLUSOr" resolve="logicOp" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="7DgXg62fsDH" role="3clFbw">
            <node concept="2OqwBi" id="7DgXg62frzk" role="2Oq$k0">
              <node concept="117lpO" id="7DgXg62frsf" role="2Oq$k0" />
              <node concept="3TrEf2" id="7DgXg62frJD" role="2OqNvi">
                <ref role="3Tt5mk" to="m31e:6L_mGjLUSOr" resolve="logicOp" />
              </node>
            </node>
            <node concept="3x8VRR" id="7DgXg62fsWH" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="2LB4erWBAkS">
    <ref role="WuzLi" to="m31e:2LB4erWAuO9" resolve="LogicOp" />
    <node concept="11bSqf" id="2LB4erWBAkT" role="11c4hB">
      <node concept="3clFbS" id="2LB4erWBAkU" role="2VODD2">
        <node concept="lc7rE" id="2LB4erWBDc5" role="3cqZAp">
          <node concept="la8eA" id="2LB4erWDh7c" role="lcghm">
            <property role="lacIc" value=" " />
          </node>
          <node concept="l9hG8" id="2LB4erWBDcr" role="lcghm">
            <node concept="2OqwBi" id="2LB4erWBDkO" role="lb14g">
              <node concept="117lpO" id="2LB4erWBDdj" role="2Oq$k0" />
              <node concept="3TrcHB" id="2LB4erWBDrw" role="2OqNvi">
                <ref role="3TsBF5" to="m31e:2LB4erWB9R2" resolve="operator" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="2LB4erWDoTa" role="lcghm">
            <property role="lacIc" value=" " />
          </node>
          <node concept="l9hG8" id="2LB4erWDhcn" role="lcghm">
            <node concept="2OqwBi" id="2LB4erWDhmV" role="lb14g">
              <node concept="117lpO" id="2LB4erWDhfq" role="2Oq$k0" />
              <node concept="3TrEf2" id="2LB4erWDhxt" role="2OqNvi">
                <ref role="3Tt5mk" to="m31e:2LB4erWAuOa" resolve="when" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
  </node>
  <node concept="WtQ9Q" id="2LB4erWHqGu">
    <ref role="WuzLi" to="m31e:2LB4erWH4Zw" resolve="ConditionAnalogic" />
    <node concept="11bSqf" id="2LB4erWHqGv" role="11c4hB">
      <node concept="3clFbS" id="2LB4erWHqGw" role="2VODD2">
        <node concept="lc7rE" id="2LB4erWHqQl" role="3cqZAp">
          <node concept="la8eA" id="2LB4erWHqQR" role="lcghm">
            <property role="lacIc" value="analogRead(" />
          </node>
          <node concept="l9hG8" id="2LB4erWHqT1" role="lcghm">
            <node concept="2OqwBi" id="2LB4erWHrq9" role="lb14g">
              <node concept="2OqwBi" id="2LB4erWHr2d" role="2Oq$k0">
                <node concept="117lpO" id="2LB4erWHqTU" role="2Oq$k0" />
                <node concept="3TrEf2" id="2LB4erWHrah" role="2OqNvi">
                  <ref role="3Tt5mk" to="m31e:2LB4erWH4ZE" resolve="sensor" />
                </node>
              </node>
              <node concept="3TrcHB" id="2LB4erWHr$W" role="2OqNvi">
                <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="2LB4erWHt6M" role="lcghm">
            <property role="lacIc" value=") " />
          </node>
          <node concept="l9hG8" id="2LB4erWHs2W" role="lcghm">
            <node concept="2OqwBi" id="2LB4erWHsgA" role="lb14g">
              <node concept="117lpO" id="2LB4erWHs8j" role="2Oq$k0" />
              <node concept="3TrcHB" id="2LB4erWHsou" role="2OqNvi">
                <ref role="3TsBF5" to="m31e:2LB4erWH4Z_" resolve="comp" />
              </node>
            </node>
          </node>
          <node concept="la8eA" id="2LB4erWIl1C" role="lcghm">
            <property role="lacIc" value=" " />
          </node>
          <node concept="l9hG8" id="2LB4erWHtnw" role="lcghm">
            <node concept="2YIFZM" id="2LB4erWHv6U" role="lb14g">
              <ref role="1Pybhc" to="wyt6:~String" resolve="String" />
              <ref role="37wK5l" to="wyt6:~String.valueOf(int):java.lang.String" resolve="valueOf" />
              <node concept="2OqwBi" id="2LB4erWHvw7" role="37wK5m">
                <node concept="117lpO" id="2LB4erWHv8j" role="2Oq$k0" />
                <node concept="3TrcHB" id="2LB4erWHvCM" role="2OqNvi">
                  <ref role="3TsBF5" to="m31e:2LB4erWH4Zz" resolve="num" />
                </node>
              </node>
            </node>
          </node>
        </node>
        <node concept="3clFbJ" id="2LB4erWHwcT" role="3cqZAp">
          <node concept="3clFbS" id="2LB4erWHwcV" role="3clFbx">
            <node concept="lc7rE" id="2LB4erWHycf" role="3cqZAp">
              <node concept="l9hG8" id="2LB4erWHyx8" role="lcghm">
                <node concept="2OqwBi" id="2LB4erWHyEj" role="lb14g">
                  <node concept="117lpO" id="2LB4erWHyy0" role="2Oq$k0" />
                  <node concept="3TrEf2" id="2LB4erWHz6S" role="2OqNvi">
                    <ref role="3Tt5mk" to="m31e:2LB4erWH4ZC" resolve="logicOp" />
                  </node>
                </node>
              </node>
            </node>
          </node>
          <node concept="2OqwBi" id="2LB4erWHx$B" role="3clFbw">
            <node concept="2OqwBi" id="2LB4erWHwNd" role="2Oq$k0">
              <node concept="117lpO" id="2LB4erWHwC7" role="2Oq$k0" />
              <node concept="3TrEf2" id="2LB4erWHwXS" role="2OqNvi">
                <ref role="3Tt5mk" to="m31e:2LB4erWH4ZC" resolve="logicOp" />
              </node>
            </node>
            <node concept="3x8VRR" id="2LB4erWHxJL" role="2OqNvi" />
          </node>
        </node>
      </node>
    </node>
  </node>
</model>

