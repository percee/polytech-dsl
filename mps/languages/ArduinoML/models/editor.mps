<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ad1b4d9a-5b3e-4d70-9fe5-3f8e4d1d1053(ArduinoML.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="11" />
    <devkit ref="2677cb18-f558-4e33-bc38-a5139cee06dc(jetbrains.mps.devkit.language-design)" />
  </languages>
  <imports>
    <import index="m31e" ref="r:091040f0-d676-4d4f-bbf6-36934e0c3ace(ArduinoML.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1237307900041" name="jetbrains.mps.lang.editor.structure.IndentLayoutIndentStyleClassItem" flags="ln" index="lj46D" />
      <concept id="1237308012275" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineStyleClassItem" flags="ln" index="ljvvj" />
      <concept id="1237375020029" name="jetbrains.mps.lang.editor.structure.IndentLayoutNewLineChildrenStyleClassItem" flags="ln" index="pj6Ft" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414536763" name="jetbrains.mps.lang.editor.structure.BooleanStyleSheetItem" flags="ln" index="VOi$J">
        <property id="1186414551515" name="flag" index="VOm3f" />
      </concept>
      <concept id="1233758997495" name="jetbrains.mps.lang.editor.structure.PunctuationLeftStyleClassItem" flags="ln" index="11L4FC" />
      <concept id="1088013125922" name="jetbrains.mps.lang.editor.structure.CellModel_RefCell" flags="sg" stub="730538219795941030" index="1iCGBv">
        <child id="1088186146602" name="editorComponent" index="1sWHZn" />
      </concept>
      <concept id="1236262245656" name="jetbrains.mps.lang.editor.structure.MatchingLabelStyleClassItem" flags="ln" index="3mYdg7">
        <property id="1238091709220" name="labelName" index="1413C4" />
      </concept>
      <concept id="1088185857835" name="jetbrains.mps.lang.editor.structure.InlineEditorComponent" flags="ig" index="1sVBvm" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <property id="1140017977771" name="readOnly" index="1Intyy" />
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="6L_mGjLVcgv">
    <ref role="1XX52x" to="m31e:6moOPi_sS$u" resolve="App" />
    <node concept="3EZMnI" id="6L_mGjLVcnK" role="2wV5jI">
      <node concept="l2Vlx" id="6L_mGjLVcnL" role="2iSdaV" />
      <node concept="3F0ifn" id="6L_mGjLVcnM" role="3EZMnx">
        <property role="3F0ifm" value="app" />
      </node>
      <node concept="3F0A7n" id="6L_mGjLVcnN" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6L_mGjLVcnO" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="3mYdg7" id="6L_mGjLVcnP" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVcnQ" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6L_mGjLVcnY" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_t1Zn" resolve="actuators" />
        <node concept="l2Vlx" id="6L_mGjLVcnZ" role="2czzBx" />
        <node concept="pj6Ft" id="6L_mGjLVco0" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6L_mGjLVco1" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVco2" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6L_mGjLVuzc" role="3EZMnx">
        <node concept="lj46D" id="6L_mGjLVu_5" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVuBm" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6L_mGjLVco9" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_t37I" resolve="sensors" />
        <node concept="l2Vlx" id="6L_mGjLVcoa" role="2czzBx" />
        <node concept="pj6Ft" id="6L_mGjLVcob" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6L_mGjLVcoc" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVcod" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6L_mGjLVuBY" role="3EZMnx">
        <node concept="lj46D" id="6L_mGjLVuCx" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVuCz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F2HdR" id="6L_mGjLVcok" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_t1Zk" resolve="states" />
        <node concept="l2Vlx" id="6L_mGjLVcol" role="2czzBx" />
        <node concept="pj6Ft" id="6L_mGjLVcom" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="lj46D" id="6L_mGjLVcon" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVcoo" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6L_mGjLVco$" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="3mYdg7" id="6L_mGjLVco_" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6L_mGjLVhyV">
    <ref role="1XX52x" to="m31e:6moOPi_sS$X" resolve="Actuator" />
    <node concept="3EZMnI" id="6L_mGjLVhyX" role="2wV5jI">
      <node concept="l2Vlx" id="6L_mGjLVhyY" role="2iSdaV" />
      <node concept="3F0ifn" id="6L_mGjLVhyZ" role="3EZMnx">
        <property role="3F0ifm" value="actuator" />
      </node>
      <node concept="3F0A7n" id="6L_mGjLVhz0" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6L_mGjLVhz6" role="3EZMnx">
        <property role="3F0ifm" value=":" />
        <node concept="11L4FC" id="6L_mGjLVhz7" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6L_mGjLVhz8" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_t38F" resolve="pin" />
      </node>
      <node concept="3F0ifn" id="2LB4erWIbXL" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F0ifn" id="2LB4erWIbY1" role="3EZMnx">
        <property role="3F0ifm" value="analog:" />
      </node>
      <node concept="3F0A7n" id="2LB4erWIbYj" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWIbXC" resolve="analog" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6L_mGjLVjGJ">
    <ref role="1XX52x" to="m31e:6moOPi_t36N" resolve="Sensor" />
    <node concept="3EZMnI" id="6L_mGjLVjHk" role="2wV5jI">
      <node concept="l2Vlx" id="6L_mGjLVjHl" role="2iSdaV" />
      <node concept="3F0ifn" id="6L_mGjLVjHm" role="3EZMnx">
        <property role="3F0ifm" value="sensor" />
      </node>
      <node concept="3F0A7n" id="6L_mGjLVjHn" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6L_mGjLVjHt" role="3EZMnx">
        <property role="3F0ifm" value=":" />
        <node concept="11L4FC" id="6L_mGjLVjHu" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0A7n" id="6L_mGjLVjHv" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_t38F" resolve="pin" />
      </node>
      <node concept="3F0ifn" id="2LB4erWEtO0" role="3EZMnx">
        <property role="3F0ifm" value="," />
      </node>
      <node concept="3F0ifn" id="2LB4erWEtOD" role="3EZMnx">
        <property role="3F0ifm" value="analog:" />
      </node>
      <node concept="3F0A7n" id="2LB4erWEtOV" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWEtNR" resolve="analog" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6L_mGjLVkRb">
    <ref role="1XX52x" to="m31e:6moOPi_sS_s" resolve="State" />
    <node concept="3EZMnI" id="6L_mGjLVkSP" role="2wV5jI">
      <node concept="l2Vlx" id="6L_mGjLVkSQ" role="2iSdaV" />
      <node concept="3F0ifn" id="6L_mGjLVkSR" role="3EZMnx">
        <property role="3F0ifm" value="state" />
      </node>
      <node concept="3F0A7n" id="6L_mGjLVkSS" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="3F0ifn" id="6L_mGjLVkST" role="3EZMnx">
        <property role="3F0ifm" value="{" />
        <node concept="3mYdg7" id="6L_mGjLVkSU" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVkSV" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3EZMnI" id="6L_mGjLVkSW" role="3EZMnx">
        <node concept="l2Vlx" id="6L_mGjLVkSX" role="2iSdaV" />
        <node concept="lj46D" id="6L_mGjLVkSY" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
        <node concept="3F0ifn" id="6L_mGjLVkSZ" role="3EZMnx">
          <property role="3F0ifm" value="  is initial" />
        </node>
        <node concept="3F0ifn" id="6L_mGjLVkT0" role="3EZMnx">
          <property role="3F0ifm" value=":" />
          <node concept="11L4FC" id="6L_mGjLVkT1" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0A7n" id="6L_mGjLVkT2" role="3EZMnx">
          <ref role="1NtTu8" to="m31e:6moOPi_t1Z6" resolve="isInitial" />
          <node concept="ljvvj" id="6L_mGjLVkT3" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F2HdR" id="6L_mGjLVkTa" role="3EZMnx">
          <ref role="1NtTu8" to="m31e:6moOPi_t1Z9" resolve="actions" />
          <node concept="l2Vlx" id="6L_mGjLVkTb" role="2czzBx" />
          <node concept="pj6Ft" id="6L_mGjLVkTc" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="lj46D" id="6L_mGjLVkTd" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
          <node concept="ljvvj" id="6L_mGjLVkTe" role="3F10Kt">
            <property role="VOm3f" value="true" />
          </node>
        </node>
        <node concept="3F0ifn" id="7DgXg62e5jt" role="3EZMnx">
          <property role="3F0ifm" value=" " />
        </node>
        <node concept="3F2HdR" id="7DgXg62e5iA" role="3EZMnx">
          <ref role="1NtTu8" to="m31e:7DgXg62e5cj" resolve="transitions" />
          <node concept="l2Vlx" id="7DgXg62e5iD" role="2czzBx" />
        </node>
        <node concept="ljvvj" id="2LB4erWBvcz" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="6L_mGjLVkTf" role="3EZMnx">
        <property role="3F0ifm" value="}" />
        <node concept="3mYdg7" id="6L_mGjLVkTg" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
        <node concept="ljvvj" id="2LB4erWBo5L" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6L_mGjLVkU6">
    <ref role="1XX52x" to="m31e:6moOPi_sSAw" resolve="Action" />
    <node concept="3EZMnI" id="6L_mGjLVq3t" role="2wV5jI">
      <node concept="l2Vlx" id="6L_mGjLVq3u" role="2iSdaV" />
      <node concept="1iCGBv" id="6L_mGjLVq3x" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_t2vq" resolve="target" />
        <node concept="1sVBvm" id="6L_mGjLVq3$" role="1sWHZn">
          <node concept="3F0A7n" id="6L_mGjLVq3A" role="2wV5jI">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6L_mGjLVq3F" role="3EZMnx">
        <property role="3F0ifm" value="~&gt;" />
      </node>
      <node concept="3F0A7n" id="6L_mGjLVq3I" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_sSAW" resolve="signal" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6L_mGjLVo2Y">
    <ref role="1XX52x" to="m31e:6moOPi_t39J" resolve="Condition" />
    <node concept="3EZMnI" id="6L_mGjLVo30" role="2wV5jI">
      <node concept="l2Vlx" id="6L_mGjLVo31" role="2iSdaV" />
      <node concept="1iCGBv" id="6L_mGjLVo34" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6L_mGjLUSOp" resolve="sensor" />
        <node concept="1sVBvm" id="6L_mGjLVo37" role="1sWHZn">
          <node concept="3F0A7n" id="6L_mGjLVo39" role="2wV5jI">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="2LB4erWH4ZU" role="3EZMnx">
        <property role="3F0ifm" value="is:" />
      </node>
      <node concept="3F0A7n" id="6L_mGjLVo3h" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6L_mGjLUSOn" resolve="signal" />
      </node>
      <node concept="3F1sOY" id="2LB4erWCNbA" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6L_mGjLUSOr" resolve="logicOp" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="6L_mGjLVs2l">
    <ref role="1XX52x" to="m31e:6moOPi_t38H" resolve="Transition" />
    <node concept="3EZMnI" id="6L_mGjLVs2n" role="2wV5jI">
      <node concept="l2Vlx" id="6L_mGjLVs2o" role="2iSdaV" />
      <node concept="3F0ifn" id="6L_mGjLVs2p" role="3EZMnx">
        <property role="3F0ifm" value="transition" />
      </node>
      <node concept="3F0ifn" id="6L_mGjLVs2x" role="3EZMnx">
        <property role="3F0ifm" value="to" />
      </node>
      <node concept="1iCGBv" id="6L_mGjLVs2y" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:6moOPi_t39D" resolve="to" />
        <node concept="1sVBvm" id="6L_mGjLVs2_" role="1sWHZn">
          <node concept="3F0A7n" id="6L_mGjLVs2B" role="2wV5jI">
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0ifn" id="6L_mGjLVs2C" role="3EZMnx">
        <property role="3F0ifm" value="when" />
        <node concept="3mYdg7" id="6L_mGjLVs2D" role="3F10Kt">
          <property role="1413C4" value="body-brace" />
        </node>
        <node concept="ljvvj" id="6L_mGjLVs2E" role="3F10Kt">
          <property role="VOm3f" value="true" />
        </node>
      </node>
      <node concept="3F0ifn" id="7DgXg62eLrr" role="3EZMnx">
        <property role="3F0ifm" value="  " />
      </node>
      <node concept="3EZMnI" id="7DgXg62eXVA" role="3EZMnx">
        <node concept="2iRkQZ" id="7DgXg62eXVB" role="2iSdaV" />
        <node concept="3F1sOY" id="2LB4erWE2GJ" role="3EZMnx">
          <ref role="1NtTu8" to="m31e:6L_mGjLUSOE" resolve="when" />
        </node>
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="2LB4erWAuO_">
    <ref role="1XX52x" to="m31e:2LB4erWAuO9" resolve="LogicOp" />
    <node concept="3EZMnI" id="2LB4erWAuOB" role="2wV5jI">
      <node concept="3F0A7n" id="2LB4erWB9R4" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWB9R2" resolve="operator" />
      </node>
      <node concept="3F1sOY" id="2LB4erWCG6o" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWAuOa" resolve="when" />
      </node>
      <node concept="l2Vlx" id="2LB4erWAuOE" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="2LB4erWH50r">
    <ref role="1XX52x" to="m31e:2LB4erWH4Zw" resolve="ConditionAnalogic" />
    <node concept="3EZMnI" id="2LB4erWH50t" role="2wV5jI">
      <node concept="1iCGBv" id="2LB4erWH50$" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWH4ZE" resolve="sensor" />
        <node concept="1sVBvm" id="2LB4erWH50A" role="1sWHZn">
          <node concept="3F0A7n" id="2LB4erWH50H" role="2wV5jI">
            <property role="1Intyy" value="true" />
            <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
          </node>
        </node>
      </node>
      <node concept="3F0A7n" id="2LB4erWH50P" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWH4Z_" resolve="comp" />
      </node>
      <node concept="3F0A7n" id="2LB4erWH51d" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWH4Zz" resolve="num" />
      </node>
      <node concept="3F1sOY" id="2LB4erWH51r" role="3EZMnx">
        <ref role="1NtTu8" to="m31e:2LB4erWH4ZC" resolve="logicOp" />
      </node>
      <node concept="l2Vlx" id="2LB4erWH50w" role="2iSdaV" />
    </node>
  </node>
</model>

