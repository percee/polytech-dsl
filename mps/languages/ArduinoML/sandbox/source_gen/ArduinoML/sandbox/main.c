#include <avr/io.h>
#include <util/delay.h>
#include <Arduino.h>

/** Generating code for application Mono alarm **/
void (*currentState)(void);

// Declaring states function headers 
void state_released();
void state_pushed();

// Declaring available actuators 
int buzzer = 10;

// Declaring available sensors 
int button1 = 7;

// Declaring states
void state_released(){
  digitalWrite(buzzer, LOW);

  if(digitalRead(button1) == HIGH){
  currentState = &state_pushed;
  }

)
currentState = &state_released;
 
 
void state_pushed(){
  digitalWrite(buzzer, HIGH);

  if(digitalRead(button1) == LOW){
  currentState = &state_released;
  }

)


void setup()
{
  pinMode(buzzer, OUTPUT);
  pinMode(button1, INPUT);
}

void loop(){
  (*currentState)();
}