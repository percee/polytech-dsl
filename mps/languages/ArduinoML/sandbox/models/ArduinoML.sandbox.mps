<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:41f3e41c-df2f-4105-ad6c-998b35aead4e(ArduinoML.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="4ec5b2f0-8f3a-4941-9882-c5f0dd3df659" name="ArduinoML" version="0" />
    <use id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core" version="1" />
  </languages>
  <imports />
  <registry>
    <language id="4ec5b2f0-8f3a-4941-9882-c5f0dd3df659" name="ArduinoML">
      <concept id="7320833554797267292" name="ArduinoML.structure.State" flags="ng" index="30UPC8">
        <property id="7320833554797305798" name="isInitial" index="30VcMi" />
        <child id="7320833554797305801" name="actions" index="30VcMt" />
        <child id="8813813857592562451" name="transitions" index="3Sqaoi" />
      </concept>
      <concept id="7320833554797267230" name="ArduinoML.structure.App" flags="ng" index="30UPDa">
        <child id="7320833554797305812" name="states" index="30VcM0" />
        <child id="7320833554797305815" name="actuators" index="30VcM3" />
        <child id="7320833554797310446" name="sensors" index="30VeaU" />
      </concept>
      <concept id="7320833554797267261" name="ArduinoML.structure.Actuator" flags="ng" index="30UPDD" />
      <concept id="7320833554797267360" name="ArduinoML.structure.Action" flags="ng" index="30UPFO">
        <property id="7320833554797267388" name="signal" index="30UPFC" />
        <reference id="7320833554797307866" name="target" index="30Vfie" />
      </concept>
      <concept id="7320833554797310575" name="ArduinoML.structure.Condition" flags="ng" index="30Ve4V">
        <property id="7810748930662829335" name="signal" index="2tzHvj" />
        <reference id="7810748930662829337" name="sensor" index="2tzHvt" />
      </concept>
      <concept id="7320833554797310509" name="ArduinoML.structure.Transition" flags="ng" index="30Ve5T">
        <reference id="7320833554797310569" name="to" index="30Ve4X" />
        <child id="7810748930662829354" name="when" index="2tzHvI" />
      </concept>
      <concept id="7320833554797310450" name="ArduinoML.structure.Brick" flags="ng" index="30VeaA">
        <property id="7320833554797310507" name="pin" index="30Ve5Z" />
      </concept>
      <concept id="7320833554797310387" name="ArduinoML.structure.Sensor" flags="ng" index="30VebB">
        <property id="3199544644537539831" name="analog" index="3gNGKh" />
      </concept>
      <concept id="3199544644538224608" name="ArduinoML.structure.ConditionAnalogic" flags="ng" index="3gOPW6">
        <property id="3199544644538224613" name="comp" index="3gOPW3" />
        <property id="3199544644538224611" name="num" index="3gOPW5" />
        <reference id="3199544644538224618" name="sensor" index="3gOPWc" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="30UPDa" id="6L_mGjLV9oV">
    <property role="TrG5h" value="Dual-check alarm" />
    <node concept="30VebB" id="6L_mGjLV9Xq" role="30VeaU">
      <property role="TrG5h" value="button1" />
      <property role="30Ve5Z" value="9" />
      <property role="3gNGKh" value="true" />
    </node>
    <node concept="30VebB" id="6L_mGjLV9Xs" role="30VeaU">
      <property role="TrG5h" value="button2" />
      <property role="30Ve5Z" value="10" />
    </node>
    <node concept="30UPC8" id="6L_mGjLV9oW" role="30VcM0">
      <property role="TrG5h" value="released" />
      <property role="30VcMi" value="true" />
      <node concept="30UPFO" id="6L_mGjLV9oX" role="30VcMt">
        <property role="30UPFC" value="LOW" />
        <ref role="30Vfie" node="6L_mGjLV9oY" resolve="buzzer" />
      </node>
      <node concept="30Ve5T" id="2LB4erWFBU0" role="3Sqaoi">
        <ref role="30Ve4X" node="7DgXg62gcVe" resolve="pushed" />
        <node concept="3gOPW6" id="2LB4erWHjuU" role="2tzHvI">
          <property role="3gOPW3" value="==" />
          <property role="3gOPW5" value="100" />
          <ref role="3gOPWc" node="6L_mGjLV9Xq" resolve="button1" />
        </node>
      </node>
    </node>
    <node concept="30UPC8" id="6L_mGjLVay6" role="30VcM0">
      <property role="TrG5h" value="pushed" />
      <node concept="30UPFO" id="6L_mGjLVay7" role="30VcMt">
        <property role="30UPFC" value="HIGH" />
        <ref role="30Vfie" node="6L_mGjLV9oY" resolve="buzzer" />
      </node>
      <node concept="30Ve5T" id="7DgXg62ehYL" role="3Sqaoi">
        <ref role="30Ve4X" node="6L_mGjLV9oW" resolve="released" />
        <node concept="30Ve4V" id="2LB4erWHEZX" role="2tzHvI">
          <property role="2tzHvj" value="HIGH" />
          <ref role="2tzHvt" node="6L_mGjLV9Xs" resolve="button2" />
        </node>
      </node>
    </node>
    <node concept="30UPDD" id="6L_mGjLV9oY" role="30VcM3">
      <property role="TrG5h" value="buzzer" />
      <property role="30Ve5Z" value="12" />
    </node>
  </node>
  <node concept="30UPDa" id="7DgXg62gcV6">
    <property role="TrG5h" value="Mono alarm" />
    <node concept="30VebB" id="7DgXg62gcV7" role="30VeaU">
      <property role="TrG5h" value="button1" />
      <property role="30Ve5Z" value="7" />
    </node>
    <node concept="30UPC8" id="7DgXg62gcV9" role="30VcM0">
      <property role="TrG5h" value="released" />
      <property role="30VcMi" value="true" />
      <node concept="30Ve5T" id="7DgXg62gcVa" role="3Sqaoi">
        <ref role="30Ve4X" node="7DgXg62gcVe" resolve="pushed" />
        <node concept="30Ve4V" id="7DgXg62gcVb" role="2tzHvI">
          <property role="2tzHvj" value="HIGH" />
          <ref role="2tzHvt" node="7DgXg62gcV7" resolve="button1" />
        </node>
      </node>
      <node concept="30UPFO" id="7DgXg62gcVd" role="30VcMt">
        <property role="30UPFC" value="LOW" />
        <ref role="30Vfie" node="7DgXg62gcVj" resolve="buzzer" />
      </node>
    </node>
    <node concept="30UPC8" id="7DgXg62gcVe" role="30VcM0">
      <property role="TrG5h" value="pushed" />
      <node concept="30UPFO" id="7DgXg62gcVf" role="30VcMt">
        <property role="30UPFC" value="HIGH" />
        <ref role="30Vfie" node="7DgXg62gcVj" resolve="buzzer" />
      </node>
      <node concept="30Ve5T" id="7DgXg62gcVg" role="3Sqaoi">
        <ref role="30Ve4X" node="7DgXg62gcV9" resolve="released" />
        <node concept="30Ve4V" id="7DgXg62gcVh" role="2tzHvI">
          <property role="2tzHvj" value="LOW" />
          <ref role="2tzHvt" node="7DgXg62gcV7" resolve="button1" />
        </node>
      </node>
    </node>
    <node concept="30UPDD" id="7DgXg62gcVj" role="30VcM3">
      <property role="TrG5h" value="buzzer" />
      <property role="30Ve5Z" value="10" />
    </node>
  </node>
</model>

