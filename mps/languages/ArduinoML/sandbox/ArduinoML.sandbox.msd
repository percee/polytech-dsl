<?xml version="1.0" encoding="UTF-8"?>
<solution name="ArduinoML.sandbox" uuid="b63abed6-3ec4-4207-8736-3b146afa6b26" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <sourcePath />
  <languageVersions>
    <language slang="l:4ec5b2f0-8f3a-4941-9882-c5f0dd3df659:ArduinoML" version="0" />
    <language slang="l:ceab5195-25ea-4f22-9b92-103b95ca8c0c:jetbrains.mps.lang.core" version="1" />
  </languageVersions>
  <dependencyVersions>
    <module reference="b63abed6-3ec4-4207-8736-3b146afa6b26(ArduinoML.sandbox)" version="0" />
  </dependencyVersions>
</solution>

